﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_AttackRadius : Enemy
{
    private CircleCollider2D coll;

    public float attackCooldownTime;
    private float attackTime;
    float lastAttackTime;

    float originalRadius;

    void Start()
    {
        coll = GetComponent<CircleCollider2D>();
        lastAttackTime = -2;
        attackCooldownTime = 8;
        originalRadius = coll.radius;
    }

    void Update()
    {
        if (Time.time - lastAttackTime >= attackCooldownTime)
        {
            Attack();
        }
    }

    public override void Attack()
    {
        GetComponent<Animator>().SetTrigger("transform");
        lastAttackTime = Time.time;
        StartCoroutine(TransformBack());
    }

    IEnumerator TransformBack()
    {
        yield return new WaitForSeconds(.5f);
        coll.radius *= 2f;

        yield return new WaitForSeconds(4.0f);
        coll.radius = originalRadius;

        GetComponent<Animator>().SetTrigger("transformBack");
    }

    public void UpdateAnimClipTimes()
    {
        AnimationClip[] clips = GetComponent<Animator>().runtimeAnimatorController.animationClips;
        foreach(AnimationClip clip in clips)
        {
            switch(clip.name)
            {
                case "attack":
                    attackTime = clip.length;
                    break;
            }
        }
    }

    public override void Move()
    {
        return;
    }
}
