﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RainbowAcorn : SpawnableObject, Poolable
{
    private List<List<Vector3>> spawnCoordinates = new List<List<Vector3>>();
    private List<Vector3> pointsToVisit = new List<Vector3>();
    private Vector3 pointToVisit;

    private int numPointsToVisit;

    private bool pointsSet;
    public bool doneVisiting;

    private float minTimeAtPoint;
    private float maxTimeAtPoint;
    private float speed;
    private float startTimeAtPoint;
    private float minTimeBetweenPoints;

    // Start is called before the first frame update
    void Start()
    {
        minTimeBetweenPoints = 1;
        pointToVisit = new Vector3(0, 0, 0);
        doneVisiting = false;

        numPointsToVisit = 10;

        minTimeAtPoint = .5f;
        maxTimeAtPoint = 1.5f;
        speed = 5;

        StartCoroutine(SetPoints());
    }

    // Update is called once per frame
    void Update()
    {
        //MoveAcorn();
    }

    void MoveAcorn()
    {
        float step = Time.deltaTime * speed;

        if (doneVisiting && Time.time - startTimeAtPoint >= minTimeBetweenPoints)
        {
            pointToVisit = pointsToVisit[Random.Range(0, pointsToVisit.Count)];
            doneVisiting = false;
        }

        if (pointsSet && !doneVisiting)
        {
            transform.position = Vector3.MoveTowards(transform.position, pointToVisit, step);
            Debug.Log("visiting " + pointToVisit);

            if (Vector3.Distance(transform.position, pointToVisit) < 0.001f)
            {
                StartCoroutine(Pause());
            }
        } 
    }

    IEnumerator SetPoints()
    {
        yield return new WaitForSeconds(.5f);
        spawnCoordinates = SpawnManager.Instance.spawnCoordinates;

        int randomAnchorPoint = Random.Range(0, spawnCoordinates.Count);

        int xMin = Mathf.Max(0, (int) spawnIndex.x - 3);
        int xMax = Mathf.Min(spawnCoordinates.Count, (int) spawnIndex.x + 3);

        int yMin = Mathf.Max(0, (int) spawnIndex.y - 3);
        int yMax = Mathf.Min(spawnCoordinates.Count, (int) spawnIndex.y + 3);

        for (int i = 0; i < numPointsToVisit; i++)
        {
            int x = Random.Range(xMin, xMax);
            int y = Random.Range(yMin, yMax);

            pointsToVisit.Add(spawnCoordinates[x][y]);
        }

        startTimeAtPoint = Time.time;
        pointsSet = true;
        doneVisiting = true;
    }

    IEnumerator Pause()
    {
        startTimeAtPoint = Time.time;
        yield return new WaitForSeconds(.5f);
        doneVisiting = true;
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "player")
        {
            AudioManager.Instance.Play("blue3");
            GetComponent<Animator>().SetTrigger("collected");
            GameEvents.instance.EnablePowerup();
            StartCoroutine(Deactivate());
        }
    }

    IEnumerator Deactivate()
    {
        yield return new WaitForSeconds(.65f);
        ObjectPooler.Instance.Deactivate(gameObject);

    }

    public void Reset()
    {
        pointsSet = false;
        doneVisiting = false;
    }

    public void Ready()
    {
        StartCoroutine(SetPoints());
    }

    public override void Setup()
    {
        spawnPadding = 0;
    }
}
