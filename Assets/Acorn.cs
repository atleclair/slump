﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Acorn : SpawnableObject, Poolable, Magnetizable
{


    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "player")
        {
            AudioManager.Instance.Play("crack");
           if (EnergyManager.Instance.CountEnergy() < 3)
           {
               EnergyManager.Instance.ReplenishEnergy();
           }
           ObjectPooler.Instance.Deactivate(gameObject);
        }


    }

    public override void Setup()
    {
        spawnPadding = 2;
    }

    void Poolable.Ready()
    {
        return;
    }

    void Poolable.Reset()
    {
        return;
    }

    public void MagnetizeToPlayer()
    {
        StartCoroutine(MagnetMove());
    }

    public IEnumerator MagnetMove()
    {
        while (transform.position != GameManager.sharedInstance.player.transform.position)
        {
            transform.position = Vector2.MoveTowards(
            new Vector2(transform.position.x, transform.position.y),
            GameManager.sharedInstance.player.transform.position, 4 * Time.deltaTime);
            yield return null;
        }
    }
}
