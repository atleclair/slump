﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cameraFocus : MonoBehaviour
{
    public static cameraFocus instance;

    private float cameraHeight;
    private float cameraWidth;
    private Vector3 screenSize_pixel;
    private Vector3 screenSize_world;

    private float oneHalf_cameraHeight;
    private float oneThird_cameraHeight;
    private float oneFourth_cameraHeight;
    private float oneFifth_cameraHeight;

    private float distanceFromPlayer;

    private float playerPositionY;

    public float height;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            //DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        cameraHeight = Camera.main.pixelHeight;
        cameraWidth = Camera.main.pixelWidth;
        screenSize_pixel = new Vector3(cameraWidth, cameraHeight);
        screenSize_world = Camera.main.ScreenToWorldPoint(screenSize_pixel);
        
        oneHalf_cameraHeight = screenSize_world.y / 2;
        oneThird_cameraHeight = screenSize_world.y / 3;
        oneFourth_cameraHeight = screenSize_world.y / 4;
        oneFifth_cameraHeight = screenSize_world.y / 5;

        distanceFromPlayer = oneHalf_cameraHeight - oneFifth_cameraHeight;
    }

    // Update is called once per frame
    void Update()
    {
        height = GameManager.sharedInstance.player.transform.position.y + distanceFromPlayer;
        transform.position = new Vector3(GameManager.sharedInstance.player.transform.position.x, height);
    }
}
