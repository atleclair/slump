﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ammo : MonoBehaviour
{
    public static Ammo Instance;

    private int maxAmmo;
    private int currAmmo;

    private int secondsPerAmmo;

    private float timeOfAmmoGain;

    private int ammoToGain;

    private int maxSatellites;


    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }

        else
        {
            Destroy(gameObject);
        }
    }

    void Start()
    {
        timeOfAmmoGain = Time.time;
        secondsPerAmmo = 5;
        currAmmo = 1;
        maxAmmo = 4;
        ammoToGain = 1;

        maxSatellites = 4;
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.time - timeOfAmmoGain >= secondsPerAmmo)
        {
            if (currAmmo < maxAmmo)
            {
                //AddSatellite();
                currAmmo += ammoToGain;
            }
            timeOfAmmoGain = Time.time;
        }
    }

    public void UseAmmo(int ammo)
    {
        if (currAmmo - ammo < 0)
        {
            Debug.Log("Not enough ammo");
            return;
        }

        currAmmo -= ammo;
    }

    public int GetAmmo()
    {
        return currAmmo;
    }
}
