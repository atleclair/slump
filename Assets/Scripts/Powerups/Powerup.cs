﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Powerup : MonoBehaviour
{
    protected int currentPowerup;
    protected int numTemp;
    protected int numCollected;
    protected int numRequired;
    protected Dictionary<int, int> energyRequired = new Dictionary<int, int>();

    void Start()
    {
    }

    private void InitializeEnergyRequirement()
    {
    }
    public void AddTemporary(int numToAdd)
    {
        numTemp += numToAdd;
    }
    private void BankFireflies()
    {
    }

    private void LoseFireflies()
    {
    }

    virtual public void ActivatePowerup(int powerup)
    {
    }

}

