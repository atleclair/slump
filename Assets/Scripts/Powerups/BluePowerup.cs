﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// The blue powerup provides mobility enhancements to the player
public class BluePowerup : Powerup
{
    void Start()
    {
        InitializeEnergyRequirement();
        base.currentPowerup = 1;
        base.numTemp = 0;
        base.numCollected = 0;
        base.numRequired = energyRequired[currentPowerup];
    }

    private void InitializeEnergyRequirement()
    {
        energyRequired.Add(1, 20);
        energyRequired.Add(2, 30);
        energyRequired.Add(3, 40);
        energyRequired.Add(4, 50);
        energyRequired.Add(5, 60);
        energyRequired.Add(6, 70);
        energyRequired.Add(7, 80);
    }

    private void BankFireflies()
    {
        numCollected += numTemp;
        numTemp = 0;
    }

    private void LoseFireflies()
    {
        numCollected /= 2;
    }

    override public void ActivatePowerup(int powerup)
    {
        string message = "";

        switch (powerup) 
        {
            case 0:
                message = "JUMP HEIGHT INCREASED";
                GameEvents.instance.PowerupActivated("blue1");
                break;
            case 1:
                message = "DOUBLE JUMP";
                GameEvents.instance.PowerupActivated("blue2");
                break;
            case 2:
                message = "JUMP SPEED INCREASED";
                GameEvents.instance.PowerupActivated("blue3");
                break;
            case 3:
                message = "DASH UNLOCKED";
                GameEvents.instance.PowerupActivated("blue4");
                break;
            case 4:
                message = "REDUCED CHARGE TIME";
                GameEvents.instance.PowerupActivated("blue5");
                break;
            case 5:
                message = "WALL KICK";
                GameEvents.instance.PowerupActivated("blue6");
                break;
        }

        PowerupManager.Instance.DisplayPowerupText(message);
    }

}
