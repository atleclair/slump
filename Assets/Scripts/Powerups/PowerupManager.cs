﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PowerupManager : MonoBehaviour
{
    public static PowerupManager Instance;

    [SerializeField] private Text blueText;
    [SerializeField] private Text orangeText;
    [SerializeField] private Text pinkText;
    [SerializeField] private TextMeshProUGUI powerupText;

    [SerializeField] Powerup bluePowerup;
    [SerializeField] Powerup pinkPowerup;
    [SerializeField] Powerup orangePowerup;

    private int pointValue;
    private int maxPointValue;
    private CollectibleItem lastCollectedItem;
    private float fadeoutDuration;

    Dictionary<string, Powerup> powerDict = new Dictionary<string, Powerup>();

    List<int> numRequired = new List<int>();

    // the number of collectibles currently in the inventory
    Dictionary<string, int> numCollected = new Dictionary<string, int>();

    // the rank of upgrade for a specific color
    Dictionary<string, int> powerupRank = new Dictionary<string, int>();

    void Awake()
    {
        powerupText.enabled = false;

        powerDict.Add("blue", bluePowerup);
        powerDict.Add("pink", pinkPowerup);
        powerDict.Add("orange", orangePowerup);

        numCollected.Add("blue", 0);
        numCollected.Add("orange", 0);
        numCollected.Add("pink", 0);

        powerupRank.Add("blue", 0);
        powerupRank.Add("orange", 0);
        powerupRank.Add("pink", 0);

        numRequired.Add(11115);
        numRequired.Add(10);
        numRequired.Add(15);
        numRequired.Add(20);
        numRequired.Add(25);
        numRequired.Add(30);

        pointValue = 1;
        maxPointValue = 3;

        fadeoutDuration = 1.5f;

        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    void Start()
    {
        GameEvents.instance.OnFireflyPickup += OnFireflyPickup;
    }

    // Adds number of fireflies to the appropriate color's temporary storage
    public void OnFireflyPickup(CollectibleItem collectible)
    {
        
        if (lastCollectedItem != null)
        {
            // award bonus for collecting pickups of the same color in a row
            if (collectible.name == lastCollectedItem.name)
            {
                if (pointValue < maxPointValue)
                {
                    pointValue++;
                }
            }

            else
            {
                lastCollectedItem = collectible;
                pointValue = 1;
            }
        }

        else
        {
            lastCollectedItem = collectible;
            pointValue = 1;
        }

        PlayPowerupSound(collectible.name);
        Powerup p = powerDict[collectible.name];
        p.AddTemporary(1);
        IncrementPowerupScore(collectible.name, pointValue);
    }

    private void IncrementPowerupScore(string powerup, int points)
    {
        int powerupScore = numCollected[powerup];
        int scoreRequired = numRequired[powerupRank[powerup]];

        if (powerup == "blue" || powerup == "orange" || powerup == "pink")
        {
            powerupScore += points;
        }

        // if the required number of pickups have been collected, activate
        // the powerup and reset the current counter
        if (powerupScore >= scoreRequired)
        {
            AudioManager.Instance.Play("powerupComplete");

            // set new score to excess over the required threshold
            int excess = powerupScore - scoreRequired;
            powerupScore = excess;

            // invoke powerup for hitting cap
            Powerup p = powerDict[powerup];
            p.ActivatePowerup(powerupRank[powerup]);

            // set the outline of the player to the color of the powerup completed
            SetPlayerOutline(powerup);

            // increment the powerup rank the player is currently on
            int powerupRankTemp = powerupRank[powerup];
            powerupRankTemp++;
            powerupRank[powerup] = powerupRankTemp;
        }

        numCollected[powerup] = powerupScore;
        GameEvents.instance.PowerupUpdate(powerup);
    }

    public void DisplayPowerupText(string message)
    {
        Vector3 startPos = powerupText.transform.position;
        Vector3 offset = new Vector3(0, 25f, 0);
        Vector3 endPos = startPos + offset;

        StartCoroutine(DisplayText(message, startPos, endPos));
    }

    private IEnumerator DisplayText(string message, Vector3 start, Vector3 end)
    {
        powerupText.enabled = true;
        powerupText.text = message;

        float animationLength = 2;
        float timer = 0.0f;

        //while we still have animation time
        while (timer <= animationLength)
        {
          
            //lerp our position
            transform.position = Vector3.Lerp(start, end, 1);

            //add time passed so far
            timer += Time.deltaTime;

            //wait till next frame
            yield return null;
        }

        powerupText.CrossFadeAlpha(0f, fadeoutDuration, false);
        yield return new WaitForSeconds(fadeoutDuration);
        powerupText.alpha = 1;

        // reset text to beginning position
        powerupText.transform.position = start;
        powerupText.enabled = false;
    }

    private void PlayPowerupSound(string name)
    {
        string soundToPlay = name + (pointValue).ToString();
        AudioManager.Instance.Play(soundToPlay);
    }

    public int GetNumRequired(int rank)
    {
        return numRequired[rank];
    }

    public int GetNumCollected(string name)
    {
        if (name == "pink" || name == "blue" || name == "orange")
        {
            return numCollected[name];
        }
        else
        {
            Debug.Log("not a color");
            return -1;
        }
    }

    public int GetPowerupRank(string name)
    {
        if (name == "pink" || name == "blue" || name == "orange")
        {
            return powerupRank[name];
        }
        else
        {
            Debug.Log("not a color");
            return -1;
        }
    }

    private void SetPlayerOutline(string powerupColor)
    {
        StartCoroutine(SettingPlayerOutline(powerupColor));
    }

    private IEnumerator SettingPlayerOutline(string powerupColor)
    {
        float duration = 1.0f;
        Player.Instance.SetMaterial(powerupColor);
        yield return new WaitForSeconds(duration);
        Player.Instance.SetMaterial("normal");
    }
}
