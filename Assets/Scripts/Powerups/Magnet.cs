﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Magnet : MonoBehaviour
{
    public static Magnet Instance;

    private bool isMagnetized;

    void Start()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else 
        {
            Destroy(gameObject);
        }
    }

    public float activeTime = 30f;

    void Update()
    {
        transform.position = GameManager.sharedInstance.player.transform.position;
    }

    IEnumerator Deactivate()
    {
        yield return new WaitForSeconds(activeTime);
        isMagnetized = false;
        Player.Instance.SetMaterial("normal");

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.GetComponent<Magnetizable>() != null)
        {
            if (isMagnetized)
            {
                Magnetizable m = (Magnetizable) collision.GetComponent<Magnetizable>();
                m.MagnetizeToPlayer();
            }
        }
    }

    public void Magnetize()
    {
        Player.Instance.SetMaterial("blue");
        isMagnetized = true;
        StartCoroutine(Deactivate());
    }
}
