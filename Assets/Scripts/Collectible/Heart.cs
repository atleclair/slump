﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Heart : SpawnableObject, Magnetizable, Poolable
{
    private int weightToSpawn = 20;
    private int healAmount = 1;

    public float amplitude = .4f;
    public float frequency = 1f;

    // Position Storage Variables
    Vector3 posOffset = new Vector3();
    Vector3 tempPos = new Vector3();

    private bool isCollected;

    private void Start()
    {
        spawnPadding = 3;
    }

    private void Update()
    {
        //tempPos = posOffset;
        //tempPos.y += Mathf.Sin(Time.fixedTime * Mathf.PI * frequency) * amplitude;

        //transform.position = tempPos;
    }

    public bool CanSpawn()
    {
        int rand = Random.Range(0, 99);
        if (rand <= weightToSpawn) 
        {
            return true; 
        }
        else { return false; }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag.Contains("player") && !isCollected) 
        {
            isCollected = true;
            AnalyticsEvents.Instance.HeartCollected();
            GameEvents.instance.HeartPickup(healAmount);
            AudioManager.Instance.Play("blue3");
            GetComponent<Animator>().SetTrigger("collected");
            StartCoroutine(DestroyAfterAnim());
        }
    }

    IEnumerator DestroyAfterAnim()
    {
        yield return new WaitForSeconds(.55f);
        ObjectPooler.Instance.Deactivate(gameObject);
    }

    public override void Setup()
    {
        spawnPadding = 3;
    }

    public void MagnetizeToPlayer()
    {
        StartCoroutine(MagnetMove());
    }

    public IEnumerator MagnetMove()
    {
        while (transform.position != GameManager.sharedInstance.player.transform.position)
        {
            transform.position = Vector2.MoveTowards(
            new Vector2(transform.position.x, transform.position.y),
            GameManager.sharedInstance.player.transform.position, 4 * Time.deltaTime);
            yield return null;
        }
    }

    public void Reset()
    {
        isCollected= false;
    }

    public void Ready()
    {
        return;
    }
}
