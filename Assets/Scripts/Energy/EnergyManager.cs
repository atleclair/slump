﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnergyManager : MonoBehaviour
{
    public static EnergyManager Instance;

    public const int numEnergy = 3;

    public Energy normalEnergy;
    public Energy bounceEnergy;
    public Energy penetrateEnergy;

    private List<Energy> activeEnergy = new List<Energy>();
    private Queue<Energy> deactiveEnergy = new Queue<Energy>();

    private float timeToReplenish;
    private float timer;
    // each time a spirit is collected, the time to regen the next energy 
    // is decreased
    private float spiritTimeDiscount; 

    public Material nonHighlightMaterial;
    public Material highlightMaterial;

    private void Awake() 
    {
        if (Instance == null)
        {
            Instance = this;
        }

        else 
        {
            Destroy(gameObject);
        }
    }
    
    void Start()
    {
        GameEvents.instance.OnSpiritPickup += UpdateTimer;
        GameEvents.instance.OnOrange3 += DecreaseReplenishTime;

        activeEnergy.Add(normalEnergy);
        activeEnergy.Add(bounceEnergy);
        activeEnergy.Add(penetrateEnergy);

        timeToReplenish = 5f;
        timer = float.MaxValue;
        spiritTimeDiscount = 1f;
    }

    void Update()
    {
        //CheckReplenish();
    }

    // return index of energy
    public int UseEnergy()
    {
        for (int i = 0; i < numEnergy; i++)
        {
            if (activeEnergy[i] != null)
            {
                // add it to the deactive queue to be added back later
                deactiveEnergy.Enqueue(activeEnergy[i]);

                // store the value of the energy before nulling it out 
                Energy temp = activeEnergy[i];

                // hide energy 
                activeEnergy[i].GetComponent<SpriteRenderer>().enabled = false;
                activeEnergy[i] = null;

                timer = Time.time;
                return temp.index;
            }
        }
        return -1;
    }

    private void CheckReplenish()
    {

        if (CountEnergy() < numEnergy && Time.time - timer >= timeToReplenish)
        {
            ReplenishEnergy();
            timer = Time.time;
        }
    }

    public void ReplenishEnergy()
    {
        if (deactiveEnergy.Count != 0)
        {
        Energy e = deactiveEnergy.Dequeue();
        activeEnergy[e.index] = e;
        e.GetComponent<SpriteRenderer>().enabled = true;
        }
    }

    // return lowest ranking energy
    public Energy GetEnergy()
    {
        if (CountEnergy() > 0)
        {
            for (int i = 0; i < numEnergy; i++)
            {
                if (activeEnergy[i] != null)
                {
                    return activeEnergy[i];
                }
            }
        }
        return null;
    }

    public bool CanUseEnergy()
    {
        if (CountEnergy() >= 1)
        {
            return true;
        }

        return false;
    }

    public bool CanUseSpecialAttack()
    {
        if (CountEnergy() == numEnergy)
        {
            return true;
        }

        return false;
    }

    private void UpdateTimer()
    {
        timer -= spiritTimeDiscount;
    }

    public void HighlightEnergy(int index)
    {
        if (activeEnergy[index] != null)
        {
            activeEnergy[index].GetComponent<SpriteRenderer>().material = highlightMaterial;
        }
    }

    public void HighlightAllEnergy()
    {
        for (int i = 0; i < numEnergy; i++)
        {
            if (activeEnergy[i] != null)
            {
                HighlightEnergy(i);
            }
        }
    }

    public void UnhighlightEnergy()
    {
        for (int i = 0; i < numEnergy; i++)
        {
            if (activeEnergy[i] != null)
            {
                activeEnergy[i].GetComponent<SpriteRenderer>().material = nonHighlightMaterial;
            }
        }
    }

    public int CountEnergy()
    {
        int count = 0;

        for (int i = 0; i < numEnergy; i++)
        {
            if (activeEnergy[i] != null)
            {
                count++;
            }
        }
        return count;
    }

    // Orange 3
    private void DecreaseReplenishTime(string powerup)
    {
        timeToReplenish /= 1.5f;
    }


}
