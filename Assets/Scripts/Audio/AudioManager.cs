﻿using UnityEngine.Audio; 
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class AudioManager : MonoBehaviour
{
    public static AudioManager Instance;

    public List<Sound> sounds = new List<Sound>();

    bool themePlaying;

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(this);
        }

        else if (Instance != this)
        {
            Destroy(gameObject);
        }

        foreach (Sound s in sounds)
        {
            s.source = gameObject.AddComponent<AudioSource>();
            s.source.clip = s.clip;
            s.source.volume = s.volume;
        }
    }

    private void Start()
    {
        if (!themePlaying)
        {
            PlayAndLoop("altTheme");
            themePlaying = true;
        }
    }

    public Sound Play(string name)
    {
        Sound s = sounds.Find(sound => sound.name == name);
        if (s != null) s.source.Play();
        return s;
    }

    public void Stop(string name)
    {
        Sound s = sounds.Find(sound => sound.name == name);
        if (s != null) s.source.Stop();
    }

    public void StopAll()
    {
        for (int i = 0; i < sounds.Count; i++)
        {
            Sound s = sounds[i];
            s.source.Stop();
        }
    }

    public void PlayAndLoop(string name)
    {
        Sound s = Play(name);
        s.source.loop = true;
    }
    
}
