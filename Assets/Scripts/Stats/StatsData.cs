﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class StatsData
{
    public float totalDistance;
    public float highScore;
    public int totalLeaves;
    public int totalEnemies;
    public int slugPoints;

    public StatsData(Stats stats)
    {
        totalDistance = stats.totalDistance;
        highScore = stats.highScore;
        totalLeaves = stats.totalLeaves;
        totalEnemies = stats.totalEnemies;
        slugPoints = stats.slugPoints;
    }
}
