﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// this class stores cumulative data about the player's entire history on the game

public class Stats : MonoBehaviour
{
    public static Stats Instance;

    public float totalDistance;
    public float highScore;
    public int totalLeaves;
    public int totalEnemies;
    public int slugPoints;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        GameEvents.instance.OnResultsCalculated += StoreResults;
    }

    void StoreResults(Dictionary<string, int> results)
    {
        LoadStats();
        if (results["distance"] > highScore)
        {
            highScore = results["distance"];
        }

        Debug.Log("Total Distance: " + totalDistance);
        Debug.Log("Results Distance: " + results["distance"]);

        totalDistance += results["distance"];
        totalLeaves += results["leaves"];
        slugPoints += results["slugPoints"];
        SaveStats();
    }

    public void SaveStats()
    {
        SaveLoad.SaveData(this);
        Debug.Log("Saved");
    }

    public void LoadStats()
    {
        StatsData data = SaveLoad.LoadData();

        totalDistance = data.totalDistance;
        highScore = data.highScore;
        totalLeaves = data.totalLeaves;
        totalEnemies = data.totalEnemies;
        slugPoints = data.slugPoints;
        Debug.Log("loaded");
    }
    

}
