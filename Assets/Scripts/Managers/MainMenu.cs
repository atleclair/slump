﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    private void Start()
    {
        Stats.Instance.LoadStats();
        AudioManager.Instance.PlayAndLoop("MainTheme");
    }

    public void Menu()
    {
        SceneManager.LoadSceneAsync(0, LoadSceneMode.Single);
    }

    public void PlayGame()
    {
        SceneManager.LoadSceneAsync(1, LoadSceneMode.Single);
    }

    public void LoadQuests()
    {
        SceneManager.LoadSceneAsync(2, LoadSceneMode.Single);
    }

    public void LoadShop()
    {
        SceneManager.LoadSceneAsync(3, LoadSceneMode.Single);
    }

    public void LoadTrophyRoom()
    {
        SceneManager.LoadSceneAsync(4, LoadSceneMode.Single);
    }

    public void LoadSettings()
    {
        SceneManager.LoadSceneAsync(5, LoadSceneMode.Single);
    }
}
