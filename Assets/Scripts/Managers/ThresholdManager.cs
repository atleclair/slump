﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThresholdManager : MonoBehaviour
{
    public static ThresholdManager Instance;

    private List<int> biomeThreshold = new List<int>();
    public int currentBiome = 0;


    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        biomeThreshold.Add(50);
        biomeThreshold.Add(1000);
        biomeThreshold.Add(1500);
        biomeThreshold.Add(2000);
        biomeThreshold.Add(2500);
    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager.sharedInstance.player.transform.position.y > biomeThreshold[currentBiome])
        {
            currentBiome++;
            GameEvents.instance.BiomeChange();
        }
    }



}
