﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ManageScore : MonoBehaviour
{
    public static ManageScore sharedInstance;

    private int points;
    private int energyAmount;
    private float distance;
    public Momentum momentum;

    private void Awake()
    {
        if(sharedInstance == null)
        {
            sharedInstance = this;
           //DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        points = 0;
        energyAmount = 100;
        distance = 0;
    }

    public void AddPoints()
    {

        points += 1;
    }

    public void SubtractEnergy()
    {
        if (energyAmount >= 10)
        {
            energyAmount -= 10;
        }
    }

    public void AddEnergy()
    {
        if (energyAmount <= 90)
        {
            energyAmount += 10;
        }
    }

    public int GetPoints()
    {
        return points;
    }

    public float GetEnergy()
    {
        return momentum.GetMomentum();
    }

    public void SetDistance(float characterDistance)
    {
        if (characterDistance < 0)
        {
            characterDistance += Mathf.Abs(characterDistance);
        }
        if (characterDistance > distance)
        {
            distance = characterDistance;
        }
    }

    public float GetDistance()
    {
        return distance;
    }
}
