﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Experimental.Rendering.Universal;
using UnityEngine;
using TMPro;

public class CollectibleItem : SpawnableObject, Poolable, Magnetizable
{

    [SerializeField] ParticleSystem ps;

    protected bool isMagnetized;

    protected float amplitude = .5f;
    protected float frequency = 1f;

    // Position Storage Variables
    Vector3 posOffset = new Vector3();
    Vector3 tempPos = new Vector3();

    public bool offsetSet = false;

    private bool isCollected;

    public string name;

    public void Awake()
    {
        isMagnetized = false;
        amplitude = Random.Range(.047f, 0.5f);
        frequency = 1f;
    }

    private void OnEnable()
    {
        GetComponent<Animator>().Play("idle", -1, Random.Range(0.0f, 1.0f));
        GameEvents.instance.OnMagnetize += MagnetizeToPlayer;
    }

    void SetOffset()
    {
        posOffset = transform.position;
        offsetSet = true;
    }

    public void Update()
    {
        if (!isMagnetized && offsetSet)
        {
            tempPos = posOffset;
            tempPos.y += Mathf.Sin(Time.fixedTime * Mathf.PI * frequency) * amplitude;

            transform.position = tempPos;
        }
    }

    public override void Setup()
    {
        spawnPadding = 1;
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag.Contains("player") && !isCollected)
        {
            GetComponent<Animator>().SetTrigger("collected");
            GameEvents.instance.FireflyPickup(this);
            StartCoroutine(Collect());
        }
    }

    private IEnumerator Collect()
    {
        isCollected = true;
        AnalyticsEvents.Instance.ItemCollected(this);

        yield return new WaitForSeconds(.45f);
        ObjectPooler.Instance.Deactivate(gameObject);
    }

    public void MagnetizeToPlayer()
    {
        StartCoroutine(MagnetMove());
    }

    public IEnumerator MagnetMove()
    {
        isMagnetized = true;
        while (transform.position != GameManager.sharedInstance.player.transform.position)
        {
            transform.position = Vector2.MoveTowards(
        new Vector2(transform.position.x, transform.position.y),
        GameManager.sharedInstance.player.transform.position, 4 * Time.deltaTime);
            yield return null;
        }

    }

    public void Reset()
    {
        offsetSet = false;
        isMagnetized = false;
        isCollected = false;
    }

    void Poolable.Ready()
    {
        amplitude = Random.Range(.047f, 0.5f);
        frequency = 1f;
        offsetSet = false;
        SetOffset();
    }
}
