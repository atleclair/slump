﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class AnalyticsEvents : MonoBehaviour
{
    public static AnalyticsEvents Instance;

    void Awake()
    {
        Instance = this;
    }

    public event Action OnJump;
    public void Jump()
    {
        OnJump?.Invoke();
    }

    public event Action OnPlayerHit;
    public void PlayerHit()
    {
        OnPlayerHit?.Invoke();
    }

    public event Action OnEnemyHit;
    public void EnemyHit()
    {
        OnEnemyHit?.Invoke();
    }

    public event Action OnEnemyKilled;
    public void EnemyKilled()
    {
        OnEnemyKilled?.Invoke();
    }

    public event Action<CollectibleItem> OnItemCollected;
    public void ItemCollected(CollectibleItem item)
    {
        OnItemCollected?.Invoke(item);
    }

    public event Action OnHeartCollected;
    public void HeartCollected()
    {
        OnHeartCollected?.Invoke();
    }

    public event Action OnSpiritCollected;
    public void SpiritCollected()
    {
        OnSpiritCollected?.Invoke();
    }


}
