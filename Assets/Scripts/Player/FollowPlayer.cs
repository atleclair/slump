﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FollowPlayer : MonoBehaviour
{
    private Vector3 cameraFocus_pixel;
    public float speed = 2.0f; 
    private Camera cam;

    private static Vector3 playerPos;

    private void Start()
    {
        cam = Camera.main;
    }
    
    void Update()
    {
        // the cmaera focus pixel is placed above the player so the camera tracks this
        // rather than the player themselves
        cameraFocus_pixel = Camera.main.WorldToScreenPoint(cameraFocus.instance.transform.position);

        if (cameraFocus_pixel.y >= cam.pixelHeight / 2)
        {
            float interpolation = speed * Time.deltaTime;

            Vector3 position = this.transform.position;
            position.y = Mathf.Lerp(this.transform.position.y, cameraFocus.instance.height, interpolation);
            this.transform.position = position;
        }

        if (playerPos.y < -5)
        {
            GameManager.sharedInstance.GameOver();
        }
    }

    public static Vector3 GetPlayerPos()
    {
        return playerPos;
    }
}