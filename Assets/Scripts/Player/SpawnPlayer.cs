﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPlayer : MonoBehaviour
{
    float mousePosX;
    float mousePosY;
    Vector3 direction;
    const float mousePosZ = 5;

    private Vector3 startingPos;
    [SerializeField] float speed;

    public GameObject player;

    private bool isSpawned;

    // Start is called before the first frame update
    void Start()
    {
        startingPos = this.transform.position;
        speed = 25;
        isSpawned = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (!isSpawned)
        {
        if (Input.GetMouseButton(0)) 
        {
        direction = (startingPos - Input.mousePosition);
        mousePosX = Input.mousePosition.x;
        mousePosY = Input.mousePosition.y;
        this.transform.position = Camera.main.ScreenToWorldPoint(new Vector3(mousePosX, mousePosY, mousePosZ));
        }

        if (Input.GetMouseButtonUp(0)) PlayerSpawn();
        }

        else 
        {
            this.gameObject.SetActive(false);
        }
    }

    private void PlayerSpawn()
    {
        player.gameObject.SetActive(true);
        player.transform.position = Camera.main.ScreenToWorldPoint(new Vector3(mousePosX, mousePosY, mousePosZ));
        isSpawned = true;
        player.GetComponent<Rigidbody2D>().velocity = direction.normalized * speed;
    }
}
