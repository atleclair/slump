﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public static Player Instance;

    SpriteRenderer spriteRenderer;
    Rigidbody2D rigidBody;
    Animator animator;

    [SerializeField] 
    public float HP;
    public float maxHP;

    [SerializeField] 
    float invulnerabilityTime;
    float timeOfLastDamage;

    public Magnet magnet;

    [SerializeField] private ParticleSystem deathParticles;
    [SerializeField] private ParticleSystem healParticles;

    public bool materialSet = false;
    public Material normal;
    public Material blue;
    public Material orange;
    public Material pink;
    public Material white;

    SpriteRenderer sr;

    Color slugColor;
    const float colorMax = 255;
    const float lowHealthThreshold = colorMax / 2;

    public bool isShielded;


    private void Awake()
    {
        HP = 3;
        maxHP = HP;
    }

    private void Start()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }

        sr = GetComponent<SpriteRenderer>();

        slugColor = new Color();
        slugColor.r = 255;

        GameEvents.instance.OnPlayerDamage += TakeDamage;
        GameEvents.instance.OnHeartPickup += GainHP;

        GameEvents.instance.OnPink1 += AddHeartAndHeal;
        GameEvents.instance.OnPink2 += IncreaseInvulnerabilityTime;
        GameEvents.instance.OnPink3 += AddHeartAndHeal;
        GameEvents.instance.OnPink5 += ActivateMagnet;

        GameEvents.instance.OnLeavesFull += ShieldPlayer;
        GameEvents.instance.OnLeavesEmpty += UnshieldPlayer;

        GameEvents.instance.OnGameOver += Death;

        invulnerabilityTime = 2;

        spriteRenderer = GetComponent<SpriteRenderer>();
        rigidBody = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
    }

    private void Update()
    {
        if (transform.position.y < ScreenBoundsManager.Instance.bottomOfScreen_Y - .5)
        {
            GameEvents.instance.GameOver();
            gameObject.SetActive(false);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag.Contains("enemyProjectile"))
        {
            if (!isShielded)
            {
                EnemyProjectile ep = collision.GetComponent<EnemyProjectile>();
                GameEvents.instance.PlayerDamage(ep.DealDamage());
            }
            return;
        }

        if (collision.gameObject.tag.Contains("enemy") && !collision.gameObject.tag.Equals("enemyTile"))
        {
            Enemy enemy = collision.GetComponent<Enemy>();
            if (!isShielded)
            {
                GameEvents.instance.PlayerDamage(enemy.DealDamage());
            }
            else
            {
                AudioManager.Instance.Play("thud");
                enemy.TakeDamage(1);
            }
        }


    }

    public void GainHP(float gain)
    {
        //Instantiate(healParticles, GameManager.sharedInstance.player.transform.position, Quaternion.identity);
        //healParticles.Play();
        if (HP + gain > maxHP) HP = maxHP;
        else HP += gain;
    }

    public void TakeDamage(float damage)
    {
        if (Time.time - timeOfLastDamage >= invulnerabilityTime)
        {
            LoseHP(damage);
        }
    }

    private void LoseHP(float damage)
    {
        ScreenShake.Instance.TriggerShake(.1f);
        AudioManager.Instance.Play("Damage");
        timeOfLastDamage = Time.time;

        if (HP - damage <= 0)
        {
            HP = 0;
            GetComponent<Animator>().SetBool("isJumping", false);
            GetComponent<Animator>().SetBool("isDead", true);
            GameEvents.instance.GameOver();
        }

        else
        {
            HP -= damage;
            StartCoroutine(DamageFlash());
        }

        float percent = HP / maxHP;
        //ChangeColor(percent);
    }

    private IEnumerator DamageFlash()
    {
        float timeBetweenFlashes = .1f;
        while (Time.time - timeOfLastDamage <= invulnerabilityTime)
        {
            yield return new WaitForSeconds(timeBetweenFlashes);
            if (spriteRenderer.color == Color.gray) spriteRenderer.color = Color.white;
            else { spriteRenderer.color = Color.gray; }
        }
        spriteRenderer.color = Color.white;
    }

    private void ChangeColor(float percent)
    {
        Debug.Log(percent);
        if (percent <= 25)
        {
            slugColor.b = 0;
            slugColor.g = 8;
            slugColor.r = colorMax;
        }

        else
        {
            float percentOfMax = lowHealthThreshold * percent;

            slugColor.b = percentOfMax;
            slugColor.g = percentOfMax;
        }

        slugColor.a = 1;

        sr.color = slugColor;
    }

    private void Death()
    {
        Instantiate(deathParticles, transform.position, Quaternion.identity);
        deathParticles.Play();

        ScreenShake.Instance.TriggerShake(.1f);
        AudioManager.Instance.Play("playerdeath");
        rigidBody.constraints = RigidbodyConstraints2D.FreezeAll;
        StartCoroutine(FinishDeath());

    }

    IEnumerator FinishDeath()
    {
        yield return new WaitForSeconds(.5f);
        gameObject.SetActive(false);
    }

    public float GetHP()
    {
        return HP;
    }

    private void AddHeartAndHeal(string powerup)
    {
        maxHP++;
        if (HP + 1 <= maxHP) { HP++; }
    }

    private void IncreaseInvulnerabilityTime(string powerup)
    {
        invulnerabilityTime *= 1.5f;
    }

    public void SetMaterial(string materialName)
    {
        Material m = null;

        switch (materialName)
        {
            case ("normal"):
                m = normal;
                materialSet = false;
                break;
            case ("blue"):
                m = blue;
                materialSet = true;
                break;
            case ("orange"):
                materialSet = true;
                m = orange;
                break;
            case ("pink"):
                materialSet = true;
                m = pink;
                break;
            case ("white"):
                materialSet = true;
                m = white;
                break;
            default:
                materialSet = false;
                m = normal;
                break;
        }
        GetComponent<SpriteRenderer>().material = m;
    }

    private void ActivateMagnet(string powerup)
    {
        Instantiate(magnet, transform.position, Quaternion.identity);
    }

    public void ShieldPlayer()
    {
        GetComponent<SpriteRenderer>().material = white;
        isShielded = true;
    }

    public void UnshieldPlayer()
    {
        GetComponent<SpriteRenderer>().material = normal;
        isShielded = false;
    }
}






