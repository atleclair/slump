﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_MoveHorizontal : Enemy, Poolable
{
    private bool valuesSet;
    private float yCoordinate;
    private float leftXCoordinate;
    private float rightXCoordinate;
    protected float speedStep;
    protected int maxSpeed;

    Rigidbody2D rb;

    [SerializeField] private ParticleSystem ps;


    private void OnEnable()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    private void InitValues()
    {
        hitPoints = 1;
        attackDamage = .5f;
        moveSpeed = 3;
        speedStep = 0f;
        maxSpeed = 7;

        yCoordinate = transform.position.y;
        leftXCoordinate = ScreenBoundsManager.Instance.spawnableArea_leftPerimeter;
        rightXCoordinate = ScreenBoundsManager.Instance.spawnableArea_rightPerimeter;

        valuesSet = true;

        // start the movement in a direction
        if (Random.Range(0, 1) > .5)
        {
            MoveRight();
        } else
        {
            MoveLeft();
        }
    }

    private void Update()
    {
        if (valuesSet) Move();
    }

    public override void Attack()
    {
        throw new System.NotImplementedException();
    }

    public override void Move()
    {
        if (transform.position.x <= leftXCoordinate)
        {

            MoveRight();
        }

        if (transform.position.x >= rightXCoordinate)
        {
            MoveLeft();
        }
    }

    private void MoveLeft()
    {
            if (moveSpeed < maxSpeed)
            {
                moveSpeed += speedStep;
            }
        rb.velocity = -transform.right * moveSpeed;
    }

    private void MoveRight()
    {
            if (moveSpeed < maxSpeed)
            {
                moveSpeed += speedStep;
            }
        rb.velocity = transform.right * moveSpeed;
    }

    public override void OnDeath()
    {
        Instantiate(spirit, transform.position, Quaternion.identity);
        Instantiate(ps, transform.position, Quaternion.identity);
        ps.Play();
        ObjectPooler.Instance.Deactivate(gameObject);
    }

    public void Ready()
    {
        valuesSet = false;
        InitValues();
    }

    public void Reset()
    {
        valuesSet = false;
    }
}
