﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_MoveHorizontal_v2 : Enemy_MoveHorizontal
{
    void Start()
    {
        moveSpeed = 1f;
        speedStep = .25f;
        maxSpeed = 7;
    }
}
