﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// The bombadier enemy spawns bombs in 3 random spots on the page that it was spawned

public class Enemy_Bombadier : Enemy
{
    [SerializeField]
    public List<List<Vector3>> bombCoordinates = new List<List<Vector3>>();

    public ParticleSystem particleSystem;

    private float timeBetweenAttacks;
    private float timeBetweenBombs;
    private float timer;
    private float deathTime;
    private int numBombs;
    private int bombDistance;

    int lowerX;
    int upperX;
    int lowerY;
    int upperY;

    public Bomb bomb;

    public bool listInitialized = false;


    void Awake()
    {
        attackDamage = 1;
        timeBetweenAttacks = 4;
        timeBetweenBombs = 0.2f;
        numBombs = 10;
        bombDistance = 7;
    }

    // Start is called before the first frame update
    public void Start()
    {
        // get most recent spawn coordinates from SpawnManager
        GameEvents.instance.OnSpawnComplete += CopyCoordinates;

        timer = float.MaxValue;
        UpdateAnimClipTimes();
        StartCoroutine(Prepare());
    }

    IEnumerator Prepare()
    {
        yield return new WaitForSeconds(.5f);
        GameEvents.instance.RequestSpawnCoordinates();
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.time - timer >= timeBetweenAttacks)
        {
            timer = Time.time;
            Attack();
        }
    }

    void CopyCoordinates(List<List<Vector3>> coords)
    {
        if (!listInitialized)
        {
            //Debug.Log("here");
            bombCoordinates = new List<List<Vector3>>(coords);

            // for (int i = 0; i < coords.Count; i++)
            // {
            //     for (int j = 0; j < coords[i].Count; j++)
            //     {
            //         bombCoordinates[i][j] = coords[i][j];
            //     }
            // }


            listInitialized = true;
            timer = Time.time;

            // only spawn bombs within 5 vertical units of bombadier
            lowerX = (int) Mathf.Max(0, spawnIndex.x - bombDistance);
            upperX = (int) Mathf.Min(bombCoordinates.Count - 1, spawnIndex.x + bombDistance);

            lowerY = (int) Mathf.Max(0, spawnIndex.y - bombDistance);
            upperY = (int) Mathf.Min(bombCoordinates.Count - 1, spawnIndex.y + bombDistance);
        }
    }

    public override void Attack()
    {
        GetComponent<Animator>().SetBool("isIdling", false);
        GetComponent<Animator>().SetBool("isAttacking", true);

        List<Vector3> bombIndex = new List<Vector3>();



        for (int i = 0; i < numBombs; i++)
        {
            int x = Random.Range(lowerY, upperY);
            int y = Random.Range(lowerX, upperX);
            bombIndex.Add(new Vector3(x, y, 0));
        }
        StartCoroutine(SpawnBombs(bombIndex));
    }

    private IEnumerator SpawnBombs(List<Vector3> bombIndex)
    {
                        Debug.Log("Index [0][0] = " + bombCoordinates[0][0]);
                Debug.Log("Index [7][7] = " + bombCoordinates[7][7]);
        for (int i = 0; i < bombIndex.Count; i++)
        {
            Vector3 coord = bombIndex[i];
            int x = (int) coord.x;
            int y = (int) coord.y;
            //Debug.Log("I'm at " + bombCoordinates[(int)spawnIndex.x][(int)spawnIndex.y] + " and I am spawning a bomb at " + bombCoordinates[x][y]);
            if (bombCoordinates[x][y] != SpawnManager.usedPoint)
            {


                Instantiate(bomb, bombCoordinates[x][y], Quaternion.identity);
                yield return new WaitForSeconds(timeBetweenBombs);
            }
        }

        yield return new WaitForSeconds(1);

        GetComponent<Animator>().SetBool("isIdling", true);
        GetComponent<Animator>().SetBool("isAttacking", false);
    }

    public override void Move()
    {
        return;
    }

    public override void OnDeath()
    {
        Instantiate(particleSystem, transform.position, Quaternion.identity);
        Instantiate(spirit, transform.position, Quaternion.identity);
        StartCoroutine(Death());
    }

    private IEnumerator Death()
    {
        GetComponent<Animator>().SetBool("isDead", true);
        yield return new WaitForSeconds(deathTime);
        Destroy(gameObject);
    }

    public void UpdateAnimClipTimes()
    {
        AnimationClip[] clips = GetComponent<Animator>().runtimeAnimatorController.animationClips;
        foreach(AnimationClip clip in clips)
        {
            switch(clip.name)
            {
                case "wizard_death":
                    deathTime = clip.length;
                    break;
                
            }
        }
    }
}
