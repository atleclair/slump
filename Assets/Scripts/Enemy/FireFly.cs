﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireFly : MonoBehaviour {

    public float chargeTime;
    public float attackDuration;
    public float attackCooldownTime;
    float lastAttackTime;
    float colliderRadius;

    public GameObject lightningAttack;
    public Animator animator;

    CircleCollider2D cc;

    private void Start()
    {
        lastAttackTime = 3;
        cc = GetComponent<CircleCollider2D>();
        animator = GetComponent<Animator>();
        animator.SetBool("isFinished", true);
        colliderRadius = cc.radius;
    }

    void Update() 
    {
        if (Time.time - lastAttackTime >= attackCooldownTime)
        {
            lastAttackTime = Time.time;
            float secondsUntilNextAttack = Random.Range(1, 3);
            StartCoroutine(PrepAttack(secondsUntilNextAttack));
        }
	}

    IEnumerator PrepAttack(float timeToWait)
    {
        yield return new WaitForSeconds(timeToWait);
        StartCoroutine(ChargeLightning());
    }

    IEnumerator ChargeLightning()
    {
        animator.SetBool("isCharging", true);
        yield return new WaitForSeconds(chargeTime);
        cc.radius = colliderRadius * 2.5f;
        animator.SetBool("isCharging", false);
        StartCoroutine(ShootLightning());
    }

    IEnumerator ShootLightning()
    {
        animator.SetBool("isAttacking", true);
        yield return new WaitForSeconds(attackDuration);
        animator.SetBool("isAttacking", false);
        animator.SetBool("isFinished", true);
        cc.radius = colliderRadius;
    }

}
