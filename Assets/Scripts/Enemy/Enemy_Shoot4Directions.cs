﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_Shoot4Directions : Enemy, Poolable
{
    [SerializeField] protected EnemyProjectile projectile;
    protected float projectileSpeed;
    protected Vector3[] directions = new Vector3[4];

    [SerializeField] protected float timeBetweenAttacks;
    protected float timeOfAttack;

    [SerializeField] protected ParticleSystem ps;

    protected int maxIndex;
    protected int currIndex;

    protected float amplitude;
    protected float frequency;

    // Position Storage Variables
    Vector3 posOffset = new Vector3();
    Vector3 tempPos = new Vector3();

    void Init()
    {
        timeBetweenAttacks = 1f;
        
        // up
        directions[0] = (new Vector3(0, 100, 0));
        // right
        directions[1] = (new Vector3(100, 0, 0));
        // down
        directions[2] = (new Vector3(0, -100, 0));
        // left
        directions[3] = (new Vector3(-100, 0, 0));

        //timeBetweenAttacks = .5f;
        projectileSpeed = 10;

        maxIndex = directions.Length - 1;
        currIndex = Random.Range(0, directions.Length - 1);

        attackDamage = .5f;
        hitPoints = 1;

        // data for oscilation behavior
        posOffset = transform.position;
        amplitude = .03f;
        frequency = 1f;
    }

    public override void Attack()
    {
        if (this.gameObject.transform.position.y > ScreenBoundsManager.Instance.bottomOfScreen_Y &&
            this.gameObject.transform.position.y < ScreenBoundsManager.Instance.topOfScreen_Y)
        {
            AudioManager.Instance.Play("enemyShoot");
            Attack(directions[GetNextDirectionIndex(currIndex)]);
        }

        timeOfAttack = Time.time;
    }

    protected void Attack(Vector3 directionToShoot)
    {
        EnemyProjectile ep = Instantiate(projectile, transform.position, Quaternion.identity);
        ep.Shoot(directionToShoot);
    }

    private int GetNextDirectionIndex(int index)
    {
        if (index == maxIndex)
        {
            currIndex = 0;
        }

        else currIndex++;
        return currIndex;
    }

    public override void Move()
    {
        // enemy doesn't move
        return;
    }

    public override void OnDeath()
    {
        Instantiate(spirit, transform.position, Quaternion.identity);
        Instantiate(ps, transform.position, Quaternion.identity);
        ps.Play();
        ObjectPooler.Instance.Deactivate(gameObject);
    }

    private void Update()
    {
        // bob 
        tempPos = posOffset;
        tempPos.y += Mathf.Sin(Time.fixedTime * Mathf.PI * frequency) * amplitude;
        transform.position = tempPos;

        CheckAttack();
    }

    void CheckAttack()
    {
        if (Time.time - timeOfAttack >= timeBetweenAttacks)
        {
            Attack();
        }
    }

    public void Reset()
    {
        return;
    }

    public void Ready()
    {
        Init();
    }
}
