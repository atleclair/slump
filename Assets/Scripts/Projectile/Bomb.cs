﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bomb : Enemy
{
    private float timeToExplode;
    private float timeOfExplosion;
    private int damage;

    public void Start()
    {
        UpdateAnimClipTimes();
        timeToExplode = 2;
        //timeOfExplosion = 1;
        StartCoroutine(BlowUp());
        GetComponent<CircleCollider2D>().enabled = false;
    }

    private IEnumerator BlowUp()
    {
        yield return new WaitForSeconds(timeToExplode);

        GetComponent<Animator>().SetBool("isExploding", true);
        GetComponent<CircleCollider2D>().enabled = true;
        yield return new WaitForSeconds(timeOfExplosion);
        gameObject.SetActive(false);
    }

    


    // None of these methods are used

    public override void Attack()
    {
        return;
    }

    public override void Move()
    {
        return;
    }

    public override void OnDeath()
    {
        return;
    }

    public void UpdateAnimClipTimes()
    {
        AnimationClip[] clips = GetComponent<Animator>().runtimeAnimatorController.animationClips;
        foreach(AnimationClip clip in clips)
        {
            switch(clip.name)
            {
                case "bomb_exploding":
                    timeOfExplosion = clip.length;
                    break;
                
            }
        }
    }
}
