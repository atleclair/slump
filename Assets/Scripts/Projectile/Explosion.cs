﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosion : MonoBehaviour
{
    private int damage = 2;

    private void Start()
    {
        Destroy(gameObject, .5f);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag.Contains("enemy"))
        {
            AnalyticsEvents.Instance.EnemyHit();
            Enemy e = collision.gameObject.GetComponent<Enemy>();
            if (e != null) e.TakeDamage(damage);
        }
    }
}
