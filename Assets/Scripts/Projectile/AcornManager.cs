﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AcornManager : MonoBehaviour
{
    public static AcornManager Instance;

    public float speed;
    public float activeTime;
    public bool isPiercing;
    public bool canExplode;
    public bool isBouncy;
    public bool shotgunEnabled;

    void Start()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else Destroy(gameObject);

        GameEvents.instance.OnAcornPower_Distance += IncreaseSpeedAndDistance;
        GameEvents.instance.OnAcornPower_Bounce += BounceEnhancement;
        GameEvents.instance.OnAcornPower_Pierce += PierceEnhancement;
        GameEvents.instance.OnAcornPower_Explode += AddExplosion;
        GameEvents.instance.OnAcornPower_Shotgun += EnableShotgun;

        speed = 12f;
        activeTime = 1;
    }

    void IncreaseSpeedAndDistance()
    {
        speed = 20f;
        activeTime = 2;
    }

    void BounceEnhancement()
    {
        isBouncy = true;
    }

    void PierceEnhancement()
    {
        isPiercing = true;
    }

    void AddExplosion()
    {
        canExplode = true;
    }

    void EnableShotgun()
    {
        shotgunEnabled = true;
    }
}
