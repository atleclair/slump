﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BounceProjectile : Projectile
{
    // Start is called before the first frame update
    void Start()
    {
        speed = 12f;
        screenShakeAmount = .02f;
        activeTime = 2;
    }

    public override void OnCollisionEnter2D(Collision2D collision)
    {
        return;
    }

}
