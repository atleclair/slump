﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{
    public static SpawnManager Instance;

    public List<List<Vector3>> spawnCoordinates = new List<List<Vector3>>();
    private List<SpawnableObject> objectsInPlay = new List<SpawnableObject>();

    private const int numColumns = 15;
    private const int numRows = 15;

    public static Vector3 usedPoint = new Vector3(-1, -1, -1);

    private int currentRespawnAttempt = 0;
    private int maxSpawnRetries = 100;

    private float horizontalDistBtwnSpawn;
    private float verticalDistBtwnSpawn;

    private float objectsSpawned;

    public bool listInitialized = false;
    public bool spawnThresholdSet = false;

    // current Y of the player
    private float playerY;

    // the Y that the spawn trigger point has to cross to initating spawning
    public float spawnThreshold_Y;

    // Start is called before the first frame update
    void Start()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else 
        { 
        Destroy(gameObject); 
        }

        GameEvents.instance.OnRequestSpawnCoordinates += SendSpawnCoordinates;

        StartCoroutine(SetSpawnThreshold());
        InitList();
        StartCoroutine(FirstScreenSpawn());
    }

    IEnumerator FirstScreenSpawn()
    {
        yield return new WaitForSeconds(1);
        Setup();
    }

    // on start, initially set the first spawn threshold to top of screen
    IEnumerator SetSpawnThreshold()
    {
        yield return new WaitForSeconds(1);

        yield return new WaitUntil(() => ScreenBoundsManager.Instance.pointsInitialized);

        spawnThreshold_Y = ScreenBoundsManager.Instance.topOfScreen_Y;

        spawnThresholdSet = true;
    }

    void Update()
    {
        if (spawnThresholdSet)
        {
            playerY = GameManager.sharedInstance.player.GetComponent<Transform>().position.y;
            CheckSpawn();
        }
    }

    // prepares the screen for new objects to spawn 
    private void Setup()
    {
        // 1. empty list of previous spawn points
        ClearList(spawnCoordinates);
        // 2. remove objects that are below the player's vision
        //ClearObjects();
        // 3. create new spawn points 
        CreateSpawnPoints(ScreenBoundsManager.Instance.topOfScreen_Y);
        // 4. send event to spawn enemies / objects
        GameEvents.instance.ReadyToSpawn();
    }

    // check to see if the player has moved high enough up to spawn new objects
    private void CheckSpawn()
    {
        if (ScreenBoundsManager.Instance.bottomOfScreen_Y >= spawnThreshold_Y)
        {
            spawnThreshold_Y = spawnThreshold_Y + ScreenBoundsManager.Instance.verticalScreenDistance;
            Setup();
        }
    }

    // on start, fills nested lists with dummy values
    private void InitList()
    {
        for (int row = 0; row < numRows; row++)
        {
            List<Vector3> tempList = new List<Vector3>();

            for (int column = 0; column < numColumns; column++)
            {
                tempList.Add(new Vector3(0, 0, 0));
            }
            spawnCoordinates.Add(tempList);
        }
        listInitialized = true;
    }

    // clears all the lists inside the outer list
    private void ClearList(List<List<Vector3>> list)
    {
        for (int row = 0; row < list.Count; row++)
        {
            for (int column = 0; column < numColumns; column++)
            {
                if (list[row].Count > 0) list[row].Clear();
            }
        }
    }

    // based on the height of the player, new available spawn coordinates are
    // created
    private void CreateSpawnPoints(float bottomOfSpawn)
    {
        float xCoordinate = ScreenBoundsManager.Instance.spawnableArea_leftPerimeter;
        float yCoordinate = bottomOfSpawn;

        verticalDistBtwnSpawn = ScreenBoundsManager.Instance.verticalScreenDistance / numRows;
        horizontalDistBtwnSpawn = ScreenBoundsManager.Instance.horizontalScreenDistance / numColumns;

        for (int row = 0; row < numRows; row++)
        {
            yCoordinate += verticalDistBtwnSpawn;
            xCoordinate = ScreenBoundsManager.Instance.spawnableArea_leftPerimeter + horizontalDistBtwnSpawn;

            for (int column = 0; column < numColumns; column++)
            {
                spawnCoordinates[row].Add(new Vector3(xCoordinate, yCoordinate, 0));
                xCoordinate += horizontalDistBtwnSpawn;
            }
        }

    }

    // spawns object in play space
    public void Spawn(SpawnableObject toSpawn)
    {
        toSpawn.Setup();
        GetSpawnPoint(toSpawn, padding: toSpawn.GetPadding());
    }

    private void AddToInPlay(SpawnableObject obj)
    {
        objectsInPlay.Add(obj);
    }

    // removes objects that are spawned below the player's FOV
    //private void ClearObjects()
    //{
    //    Queue<SpawnableObject> toDelete = new Queue<SpawnableObject>();

    //    foreach (SpawnableObject obj in objectsInPlay)
    //    {
    //        if (obj != null && obj.transform.position.y < ScreenBoundsManager.Instance.bottomOfScreen_Y)
    //        {
    //            toDelete.Enqueue(obj);
    //        }
    //    }

    //    while (toDelete.Count > 0)
    //    {
    //        SpawnableObject deleted = toDelete.Dequeue();
    //        if (deleted != null) Destroy(deleted.gameObject);
    //    }
    //}

    // checks for a spawnable coordinate point and if one is found, spawns object
    private bool GetSpawnPoint(SpawnableObject toSpawn, int padding)
    {
        int spawnRow = Random.Range(0, numRows - 1);
        int spawnColumn = Random.Range(1, numColumns - 2);

        Vector3 spawnIndex = new Vector3(spawnRow, spawnColumn, 0);
        Vector3 spawnPoint_World = spawnCoordinates[spawnRow][spawnColumn];

        // if the chosen spawn point passes all checks, allow the object to spawn
        if (spawnPoint_World != usedPoint)
        {
            if (CheckSpawnPadding(spawnIndex, padding) && CheckSpawnRow_Column(spawnIndex))
            {
                SpawnObject(toSpawn, spawnPoint_World, spawnIndex);
                spawnCoordinates[spawnRow][spawnColumn] = usedPoint;
                currentRespawnAttempt = 0;
                return true;
            }
        }

        // chosen spawn point didn't work, try again
        if (currentRespawnAttempt < maxSpawnRetries)
        {
            currentRespawnAttempt++;
            GetSpawnPoint(toSpawn, padding);
        }

        // chosen spawn point didn't work, move on to next object that is trying to be spawned
        currentRespawnAttempt = 0;
        return false;
    }

    // instantiates the object at the given coordinate point
    private void SpawnObject(SpawnableObject toSpawn, Vector3 spawnPoint, Vector3 spawnIndex)
    {
        SpawnableObject so = ObjectPooler.Instance.Retrieve(toSpawn.gameObject.tag).GetComponent<SpawnableObject>();
        so.transform.position = spawnPoint;
        if (so.GetComponent<Poolable>() != null)
        {
            so.GetComponent<Poolable>().Ready();
        }

        so.spawnIndex.x = spawnIndex.y;
        so.spawnIndex.y = spawnIndex.x;
    }

    // checks around the object to tbe spawned 
    private bool CheckSpawnPadding(Vector3 spawnIndex, int padding)
    {
        int x = (int)spawnIndex.x;
        int y = (int)spawnIndex.y;

        for (int horizontalOffset = -padding; horizontalOffset <= padding; horizontalOffset++)
        {
            for (int verticalOffset = -padding; verticalOffset <= padding; verticalOffset++)
            {
                if (spawnIndex.x + horizontalOffset >= 0 && spawnIndex.x + horizontalOffset < numColumns)
                {
                    if (spawnIndex.y + verticalOffset >= 0 && spawnIndex.y + verticalOffset < numRows)
                    {

                        if (spawnCoordinates[x + horizontalOffset][y + verticalOffset] == usedPoint)
                        {
                            return false;
                        }
                    }
                }
            }
        }
        return true;
    }

    // checks the rows and columns of the object to be spawned
    private bool CheckSpawnRow_Column(Vector3 spawnIndex)
    {
        for (int row = 0; row < numRows; row++)
        {
            if (spawnCoordinates[row][(int)spawnIndex.y] == usedPoint)
            {
                return false;
            }
        }

        for (int column = 0; column < numColumns; column++)
        {
            if (spawnCoordinates[(int)spawnIndex.x][column] == usedPoint)
            {
                return false;
            }
        }
        return true;
    }

    private void SendSpawnCoordinates()
    {
        GameEvents.instance.SpawnComplete(spawnCoordinates);
    }
}
