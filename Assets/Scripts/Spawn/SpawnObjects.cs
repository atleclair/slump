﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnObjects : MonoBehaviour
{
    public Camera camera;

    public GameObject player;
    public GameObject[] walls;
    public GameObject[] coins;
    public GameObject[] enemies;
    private Vector3 screenBounds;
    private Vector3 cameraPosWorld;
    private List<float> possibleWallX = new List<float>();
    Queue obstacleQueue;

    // max number of obstacles / coins that can be spawned in stage
    public int spawnThreshold;
    // number of obstacles being spawned in stage
    private int numObstacles;
    // number of coins being spawned in stage
    private int numFireflies;
    // an array holding y values of obstacles being spawned
    private float[] obstacleHeights;
    // an array holding the y values of the coins being spawned
    private float[] coinHeights;
    // the current bottom x value of the screen
    private float screenBottom;
    // the height when new obstacles / coins are spawned
    private float triggerHeight;

    private float charPosY;

    Vector2 spawnableArea;

    void Start()
    {
        cameraPosWorld = camera.GetComponent<Transform>().position;
        screenBounds = camera.ScreenToWorldPoint(new Vector2(Screen.width, Screen.height));
        spawnableArea = new Vector2(screenBounds.x * -.66f, screenBounds.x * .66f);
        triggerHeight = cameraPosWorld.y + camera.orthographicSize;
        possibleWallX.Add(-.45f);
        possibleWallX.Add(0);
        possibleWallX.Add(.45f);
        PrepareStage();
    }

    // Update is called once per frame
    void Update()
    {
        cameraPosWorld = camera.GetComponent<Transform>().position;

        // when the bottom of the screen passes the original top of the screen
        // spawn new objects two screen heights above
        // set the new trigger height to be the top of the screen
        if (cameraPosWorld.y - camera.orthographicSize >= triggerHeight)
        {
           //PrepareStage();
            triggerHeight = cameraPosWorld.y + camera.orthographicSize;
        }
       
    }

    private void SpawnObstacle(float obstacleHeight)
    {
        int randomObstacle = Random.Range(0, walls.Length);
        GameObject o = Instantiate(walls[randomObstacle]) as GameObject;
        float wallWidth = SpawnWalls.SharedInstance.GetWallWidth();

        int randomWallX = Random.Range(0, 3);
        o.transform.position = new Vector2(GameManager.sharedInstance.screenBounds.x * possibleWallX[randomWallX], obstacleHeight);
    }

    private void SpawnCoin(float coinHeight)
    {
        int randomFirefly;

        int randomSeed = Random.Range(0, 20);
        if (randomSeed > 1) randomFirefly = Random.Range(0, coins.Length - 1);
        else randomFirefly = 3;

        GameObject o = Instantiate(coins[randomFirefly]) as GameObject;
        float objectBounds = screenBounds.x - (o.GetComponent<SpriteRenderer>().bounds.extents.x);
        o.transform.position = new Vector2(Random.Range(-objectBounds, objectBounds), coinHeight);
    }

    private void DestroyObjects()
    {
        GameObject go = obstacleQueue.Peek() as GameObject;
        if (go.transform.position.y < 0)
        {
            obstacleQueue.Dequeue();
            Destroy(go);
        }
    }

    private void PrepareStage()
    {
        numFireflies = Random.Range(3, 10);
        numObstacles = Random.Range(2, spawnThreshold);
        SpawnEnemy();

        for (int i = 0; i < numFireflies; i++)
        {
            // get a y value for a coin spawn that is outside player's FOV
            float coinPosY = Random.Range(cameraPosWorld.y + camera.orthographicSize, cameraPosWorld.y + camera.orthographicSize * 3);
            // ensure coins aren't being spawned too close to one another
            //for (int j = 0; j < coinHeights.Length; j++)
            //{
            //    while (Mathf.Abs(coinPosY - coinHeights[j]) < 5)
            //    {
            //        coinPosY = Random.Range(screenBounds.y, screenBounds.y * 2);
            //    }
            //}
            //coinHeights[i] = coinPosY;
            SpawnCoin(coinPosY);
        }

        for (int i = 0; i < numObstacles; i++)
        {
            float obstaclePos = Random.Range(cameraPosWorld.y + camera.orthographicSize, cameraPosWorld.y + camera.orthographicSize * 3);
            //SpawnObstacle(obstaclePos);
        }

        //for (int i = 0; i < coinHeights.Length; i++)
        //{
        //    coinHeights[i] = 0;
        //}
    }

    private void SpawnEnemy()
    {
        float enemyPosY = Random.Range(cameraPosWorld.y + camera.orthographicSize, cameraPosWorld.y + camera.orthographicSize * 3);
        GameObject o = Instantiate(enemies[Random.Range(0, enemies.Length)]) as GameObject;
        GameManager.sharedInstance.AddSpawnedObject(o);
        float objectBounds = screenBounds.x - (o.GetComponent<SpriteRenderer>().bounds.extents.x);
        o.transform.position = new Vector2(Random.Range(-objectBounds, objectBounds), enemyPosY);
    }
}
