﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// This class stores the lists of all items/enemies that can spawn in the game
// Based on the distance the player has traveled, the frequency and difficulty
// of these objects will change.

// This class will pick from the lists and then call the SpawnManager to 
// instantiate the objects in the game

public class Spawner : MonoBehaviour
{
    private GameObject player;

    [SerializeField] private List<Enemy> rank1Enemies = new List<Enemy>();
    [SerializeField] private List<Enemy> rank2Enemies = new List<Enemy>();
    [SerializeField] private List<Enemy> rank3Enemies = new List<Enemy>();

    [SerializeField] private List<CollectibleItem> collectibles = new List<CollectibleItem>();
    [SerializeField] private Heart heart;
    [SerializeField] private RainbowAcorn r_acorn;
    [SerializeField] private Acorn acorn;
    [SerializeField] private MagnetItem magnet;
    [SerializeField] private Sap sap;

    // populated with chosen objects to spawn on a given screen
    private List<SpawnableObject> toSpawn = new List<SpawnableObject>();

    private void Start()
    {
        player = GameManager.sharedInstance.player;
        GameEvents.instance.OnSpawnReady += BeginSpawn;
    }

    private void BeginSpawn()
    {
        float playerHeight = player.transform.position.y;

        // pick enemies based on player height to be added to spawn list
        ChooseEnemies(playerHeight);

        // pick collectible items to be added to spawn list
        ChooseCollectibles(playerHeight);

        // spawn enemies from spawn list
        Spawn();
    }

    private void ChooseEnemies(float playerHeight)
    {
        if (playerHeight < 75)
        {
            ReadyEnemies(rank1Enemies, rank2Enemies, 98, numToSpawn: 2);
        }

        if (playerHeight >= 75 && playerHeight < 200)
        {
            ReadyEnemies(rank1Enemies, rank2Enemies, weight_Easy: 50, numToSpawn: 3);
        }

        if (playerHeight >= 200 && playerHeight < 300)
        {
            ReadyEnemies(rank1Enemies, rank2Enemies, 20, 4);
        }

        if (playerHeight >= 300)
        {
            ReadyEnemies(rank2Enemies, rank2Enemies, 20, 6);
        }
    }

    // pass in two lists
    // roll to see from which list the enemy will be spawned
    // choose an enemy from the chosen list at random and added it to list to be spawned
    // precondition: weight must be < 100
    private void ReadyEnemies(List<Enemy> easierEnemies, List<Enemy> harderEnemies, int weight_Easy, int numToSpawn)
    {
        List<Enemy> chosenList = new List<Enemy>();
        if (weight_Easy > 100 || weight_Easy < 0)
        {
            Debug.Log("Weight must be between 0 and 10");
            return;
        }

        for (int i = 0; i < numToSpawn; i++)
        {
            // RNG to figure out if a tough enemy  or easy enemy should be spawned
            int randomRoll = Random.Range(0, 100);

            if (randomRoll <= weight_Easy)
            { 
                chosenList = easierEnemies;
            } 
            else
            {
                chosenList = harderEnemies;
            }

            // pick an enemy from the chosen list to be added to the spawn list
            int randomEnemy = Random.Range(0, chosenList.Count);
            toSpawn.Add(chosenList[randomEnemy]);
        }
    }

    private void ChooseCollectibles(float playerHeight)
    {
        int SapRoll = Random.Range(0, 5);
        if (SapRoll == 4) toSpawn.Add(sap);

        int magnetRoll = Random.Range(0, 5);
        if (magnetRoll == 4) toSpawn.Add(magnet);

        // 1 in 3 chance of spawning rainbow acorn
        int randomRoll = Random.Range(0, 3);
        if (randomRoll == 2) toSpawn.Add(r_acorn);

        // spawn between 0 and 2 acorns
        for (int i = 0; i < randomRoll; i++)
        {
            toSpawn.Add(acorn);
        }
        // spawn either 1 or 2 regular acorns 

        if (heart.CanSpawn()) { toSpawn.Add(heart); }

        if (playerHeight < 5000)
        {
            ReadyCollectibleItems(collectibles, 3);
        }
    }

    private void ReadyCollectibleItems(List<CollectibleItem> collectibles, int numToSpawn)
    {
         
        for (int i = 0; i < numToSpawn; i++)
        {
            toSpawn.Add(collectibles[Random.Range(0, collectibles.Count)]);
        }
    }

    private void Spawn()
    {
        //ShuffleList();

        foreach (SpawnableObject so in toSpawn)
        {
            SpawnManager.Instance.Spawn(so);
        }
        toSpawn.Clear();
    }

    private void ShuffleList()
    {
        for (int i = 0; i < toSpawn.Count; i++)
        {
            SpawnableObject temp = toSpawn[i];
            int randomIndex = Random.Range(i, toSpawn.Count);
            toSpawn[i] = toSpawn[randomIndex];
            toSpawn[randomIndex] = temp;
        }
    }


}
