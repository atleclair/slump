﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationManager : MonoBehaviour
{
    public Animator playerAnimator;
    
    private enum PlayerState
    {
        Idle,
        Sliding,
        Windup,
        Jump
    }

    PlayerState playerState;

    private void Start()
    {
        GameEvents.instance.OnPlayerIdle += AnimatePlayerIdle;
        GameEvents.instance.OnPlayerSliding += AnimatePlayerSliding;
        GameEvents.instance.OnPlayerWindup += AnimatePlayerWindup;
        GameEvents.instance.OnPlayerJump += AnimatePlayerJump;
        GameEvents.instance.OnPlayerDoubleJump += AnimatePlayerDoubleJump;
    }

    void AnimatePlayerIdle()
    {
        if (playerState != PlayerState.Idle)
        {
            playerState = PlayerState.Idle;

            playerAnimator.SetBool("isDashing", false);
            playerAnimator.SetBool("isJumping", false);
            playerAnimator.SetBool("isDashing", false);

            playerAnimator.SetBool("isIdling", true);
        }
    }

    void AnimatePlayerSliding()
    {
        if (playerState != PlayerState.Sliding)
        {
            playerState = PlayerState.Sliding;

            playerAnimator.SetBool("isJumping", false);
            playerAnimator.SetBool("isIdling", false);
            playerAnimator.SetBool("isWindingUp", false);

            playerAnimator.SetBool("isSliding", true);
        }
    }

    void AnimatePlayerWindup()
    {        
        if (playerState!= PlayerState.Windup)
        {
            playerState = PlayerState.Windup;

            playerAnimator.SetBool("isJumping", false);
            playerAnimator.SetBool("isIdling", false);
            playerAnimator.SetBool("isSliding", false);

            playerAnimator.SetBool("isWindingUp", true);
        }
    }

    void AnimatePlayerJump()
    {
        if (playerState != PlayerState.Jump)
        {
            playerState = PlayerState.Jump;

            playerAnimator.SetBool("isIdling", false);
            playerAnimator.SetBool("isSliding", false);
            playerAnimator.SetBool("isWindingUp", false);
        
        playerAnimator.SetBool("isJumping", true);
        }
    }

    void AnimatePlayerDoubleJump()
    {
        playerAnimator.SetBool("isJumping", false);
        playerAnimator.SetBool("isDoubleJumping", true);
    }


}
