﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sap : SpawnableObject, Poolable
{
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "player")
        {
            GameEvents.instance.SapPickup();
            ObjectPooler.Instance.Deactivate(gameObject);
        }
    }

    public void Ready()
    {
        return;
    }

    public void Reset()
    {
        return;
    }

    public override void Setup()
    {
        spawnPadding = 2;
    }
}
