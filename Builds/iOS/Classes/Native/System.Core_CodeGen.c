﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.String SR::Format(System.String,System.Object)
extern void SR_Format_mC35B52FE843D9C9D465B6B544FA184058A46E0A9 ();
// 0x00000002 System.Exception System.Linq.Error::ArgumentNull(System.String)
extern void Error_ArgumentNull_mCA126ED8F4F3B343A70E201C44B3A509690F1EA7 ();
// 0x00000003 System.Exception System.Linq.Error::ArgumentOutOfRange(System.String)
extern void Error_ArgumentOutOfRange_mACFCB068F4E0C4EEF9E6EDDD59E798901C32C6C9 ();
// 0x00000004 System.Exception System.Linq.Error::MoreThanOneMatch()
extern void Error_MoreThanOneMatch_m85C3617F782E9F2333FC1FDF42821BE069F24623 ();
// 0x00000005 System.Exception System.Linq.Error::NoElements()
extern void Error_NoElements_m17188AC2CF25EB359A4E1DDE9518A98598791136 ();
// 0x00000006 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Where(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000007 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Select(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TResult>)
// 0x00000008 System.Func`2<TSource,System.Boolean> System.Linq.Enumerable::CombinePredicates(System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,System.Boolean>)
// 0x00000009 System.Func`2<TSource,TResult> System.Linq.Enumerable::CombineSelectors(System.Func`2<TSource,TMiddle>,System.Func`2<TMiddle,TResult>)
// 0x0000000A System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::SelectMany(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Collections.Generic.IEnumerable`1<TResult>>)
// 0x0000000B System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::SelectManyIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Collections.Generic.IEnumerable`1<TResult>>)
// 0x0000000C System.Linq.IOrderedEnumerable`1<TSource> System.Linq.Enumerable::OrderBy(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>)
// 0x0000000D System.Linq.IOrderedEnumerable`1<TSource> System.Linq.Enumerable::ThenBy(System.Linq.IOrderedEnumerable`1<TSource>,System.Func`2<TSource,TKey>)
// 0x0000000E System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Distinct(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000000F System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::DistinctIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEqualityComparer`1<TSource>)
// 0x00000010 TSource[] System.Linq.Enumerable::ToArray(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000011 System.Collections.Generic.List`1<TSource> System.Linq.Enumerable::ToList(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000012 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Cast(System.Collections.IEnumerable)
// 0x00000013 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::CastIterator(System.Collections.IEnumerable)
// 0x00000014 TSource System.Linq.Enumerable::FirstOrDefault(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000015 TSource System.Linq.Enumerable::Last(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000016 TSource System.Linq.Enumerable::SingleOrDefault(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000017 TSource System.Linq.Enumerable::ElementAt(System.Collections.Generic.IEnumerable`1<TSource>,System.Int32)
// 0x00000018 System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000019 System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000001A System.Int32 System.Linq.Enumerable::Count(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000001B System.Int32 System.Linq.Enumerable::Count(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000001C System.Boolean System.Linq.Enumerable::Contains(System.Collections.Generic.IEnumerable`1<TSource>,TSource)
// 0x0000001D System.Boolean System.Linq.Enumerable::Contains(System.Collections.Generic.IEnumerable`1<TSource>,TSource,System.Collections.Generic.IEqualityComparer`1<TSource>)
// 0x0000001E System.Void System.Linq.Enumerable_Iterator`1::.ctor()
// 0x0000001F TSource System.Linq.Enumerable_Iterator`1::get_Current()
// 0x00000020 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_Iterator`1::Clone()
// 0x00000021 System.Void System.Linq.Enumerable_Iterator`1::Dispose()
// 0x00000022 System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_Iterator`1::GetEnumerator()
// 0x00000023 System.Boolean System.Linq.Enumerable_Iterator`1::MoveNext()
// 0x00000024 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_Iterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000025 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_Iterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000026 System.Object System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerator.get_Current()
// 0x00000027 System.Collections.IEnumerator System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000028 System.Void System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerator.Reset()
// 0x00000029 System.Void System.Linq.Enumerable_WhereEnumerableIterator`1::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000002A System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereEnumerableIterator`1::Clone()
// 0x0000002B System.Void System.Linq.Enumerable_WhereEnumerableIterator`1::Dispose()
// 0x0000002C System.Boolean System.Linq.Enumerable_WhereEnumerableIterator`1::MoveNext()
// 0x0000002D System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereEnumerableIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x0000002E System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereEnumerableIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x0000002F System.Void System.Linq.Enumerable_WhereArrayIterator`1::.ctor(TSource[],System.Func`2<TSource,System.Boolean>)
// 0x00000030 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereArrayIterator`1::Clone()
// 0x00000031 System.Boolean System.Linq.Enumerable_WhereArrayIterator`1::MoveNext()
// 0x00000032 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereArrayIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000033 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereArrayIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000034 System.Void System.Linq.Enumerable_WhereListIterator`1::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000035 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereListIterator`1::Clone()
// 0x00000036 System.Boolean System.Linq.Enumerable_WhereListIterator`1::MoveNext()
// 0x00000037 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereListIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000038 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereListIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000039 System.Void System.Linq.Enumerable_WhereSelectEnumerableIterator`2::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x0000003A System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Clone()
// 0x0000003B System.Void System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Dispose()
// 0x0000003C System.Boolean System.Linq.Enumerable_WhereSelectEnumerableIterator`2::MoveNext()
// 0x0000003D System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x0000003E System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x0000003F System.Void System.Linq.Enumerable_WhereSelectArrayIterator`2::.ctor(TSource[],System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x00000040 System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectArrayIterator`2::Clone()
// 0x00000041 System.Boolean System.Linq.Enumerable_WhereSelectArrayIterator`2::MoveNext()
// 0x00000042 System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable_WhereSelectArrayIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x00000043 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectArrayIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x00000044 System.Void System.Linq.Enumerable_WhereSelectListIterator`2::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x00000045 System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2::Clone()
// 0x00000046 System.Boolean System.Linq.Enumerable_WhereSelectListIterator`2::MoveNext()
// 0x00000047 System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable_WhereSelectListIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x00000048 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x00000049 System.Void System.Linq.Enumerable_<>c__DisplayClass6_0`1::.ctor()
// 0x0000004A System.Boolean System.Linq.Enumerable_<>c__DisplayClass6_0`1::<CombinePredicates>b__0(TSource)
// 0x0000004B System.Void System.Linq.Enumerable_<>c__DisplayClass7_0`3::.ctor()
// 0x0000004C TResult System.Linq.Enumerable_<>c__DisplayClass7_0`3::<CombineSelectors>b__0(TSource)
// 0x0000004D System.Void System.Linq.Enumerable_<SelectManyIterator>d__17`2::.ctor(System.Int32)
// 0x0000004E System.Void System.Linq.Enumerable_<SelectManyIterator>d__17`2::System.IDisposable.Dispose()
// 0x0000004F System.Boolean System.Linq.Enumerable_<SelectManyIterator>d__17`2::MoveNext()
// 0x00000050 System.Void System.Linq.Enumerable_<SelectManyIterator>d__17`2::<>m__Finally1()
// 0x00000051 System.Void System.Linq.Enumerable_<SelectManyIterator>d__17`2::<>m__Finally2()
// 0x00000052 TResult System.Linq.Enumerable_<SelectManyIterator>d__17`2::System.Collections.Generic.IEnumerator<TResult>.get_Current()
// 0x00000053 System.Void System.Linq.Enumerable_<SelectManyIterator>d__17`2::System.Collections.IEnumerator.Reset()
// 0x00000054 System.Object System.Linq.Enumerable_<SelectManyIterator>d__17`2::System.Collections.IEnumerator.get_Current()
// 0x00000055 System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable_<SelectManyIterator>d__17`2::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
// 0x00000056 System.Collections.IEnumerator System.Linq.Enumerable_<SelectManyIterator>d__17`2::System.Collections.IEnumerable.GetEnumerator()
// 0x00000057 System.Void System.Linq.Enumerable_<DistinctIterator>d__68`1::.ctor(System.Int32)
// 0x00000058 System.Void System.Linq.Enumerable_<DistinctIterator>d__68`1::System.IDisposable.Dispose()
// 0x00000059 System.Boolean System.Linq.Enumerable_<DistinctIterator>d__68`1::MoveNext()
// 0x0000005A System.Void System.Linq.Enumerable_<DistinctIterator>d__68`1::<>m__Finally1()
// 0x0000005B TSource System.Linq.Enumerable_<DistinctIterator>d__68`1::System.Collections.Generic.IEnumerator<TSource>.get_Current()
// 0x0000005C System.Void System.Linq.Enumerable_<DistinctIterator>d__68`1::System.Collections.IEnumerator.Reset()
// 0x0000005D System.Object System.Linq.Enumerable_<DistinctIterator>d__68`1::System.Collections.IEnumerator.get_Current()
// 0x0000005E System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_<DistinctIterator>d__68`1::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
// 0x0000005F System.Collections.IEnumerator System.Linq.Enumerable_<DistinctIterator>d__68`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000060 System.Void System.Linq.Enumerable_<CastIterator>d__99`1::.ctor(System.Int32)
// 0x00000061 System.Void System.Linq.Enumerable_<CastIterator>d__99`1::System.IDisposable.Dispose()
// 0x00000062 System.Boolean System.Linq.Enumerable_<CastIterator>d__99`1::MoveNext()
// 0x00000063 System.Void System.Linq.Enumerable_<CastIterator>d__99`1::<>m__Finally1()
// 0x00000064 TResult System.Linq.Enumerable_<CastIterator>d__99`1::System.Collections.Generic.IEnumerator<TResult>.get_Current()
// 0x00000065 System.Void System.Linq.Enumerable_<CastIterator>d__99`1::System.Collections.IEnumerator.Reset()
// 0x00000066 System.Object System.Linq.Enumerable_<CastIterator>d__99`1::System.Collections.IEnumerator.get_Current()
// 0x00000067 System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable_<CastIterator>d__99`1::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
// 0x00000068 System.Collections.IEnumerator System.Linq.Enumerable_<CastIterator>d__99`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000069 System.Linq.IOrderedEnumerable`1<TElement> System.Linq.IOrderedEnumerable`1::CreateOrderedEnumerable(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
// 0x0000006A System.Void System.Linq.Set`1::.ctor(System.Collections.Generic.IEqualityComparer`1<TElement>)
// 0x0000006B System.Boolean System.Linq.Set`1::Add(TElement)
// 0x0000006C System.Boolean System.Linq.Set`1::Find(TElement,System.Boolean)
// 0x0000006D System.Void System.Linq.Set`1::Resize()
// 0x0000006E System.Int32 System.Linq.Set`1::InternalGetHashCode(TElement)
// 0x0000006F System.Collections.Generic.IEnumerator`1<TElement> System.Linq.OrderedEnumerable`1::GetEnumerator()
// 0x00000070 System.Linq.EnumerableSorter`1<TElement> System.Linq.OrderedEnumerable`1::GetEnumerableSorter(System.Linq.EnumerableSorter`1<TElement>)
// 0x00000071 System.Collections.IEnumerator System.Linq.OrderedEnumerable`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000072 System.Linq.IOrderedEnumerable`1<TElement> System.Linq.OrderedEnumerable`1::System.Linq.IOrderedEnumerable<TElement>.CreateOrderedEnumerable(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
// 0x00000073 System.Void System.Linq.OrderedEnumerable`1::.ctor()
// 0x00000074 System.Void System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::.ctor(System.Int32)
// 0x00000075 System.Void System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.IDisposable.Dispose()
// 0x00000076 System.Boolean System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::MoveNext()
// 0x00000077 TElement System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.Collections.Generic.IEnumerator<TElement>.get_Current()
// 0x00000078 System.Void System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.Collections.IEnumerator.Reset()
// 0x00000079 System.Object System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.Collections.IEnumerator.get_Current()
// 0x0000007A System.Void System.Linq.OrderedEnumerable`2::.ctor(System.Collections.Generic.IEnumerable`1<TElement>,System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
// 0x0000007B System.Linq.EnumerableSorter`1<TElement> System.Linq.OrderedEnumerable`2::GetEnumerableSorter(System.Linq.EnumerableSorter`1<TElement>)
// 0x0000007C System.Void System.Linq.EnumerableSorter`1::ComputeKeys(TElement[],System.Int32)
// 0x0000007D System.Int32 System.Linq.EnumerableSorter`1::CompareKeys(System.Int32,System.Int32)
// 0x0000007E System.Int32[] System.Linq.EnumerableSorter`1::Sort(TElement[],System.Int32)
// 0x0000007F System.Void System.Linq.EnumerableSorter`1::QuickSort(System.Int32[],System.Int32,System.Int32)
// 0x00000080 System.Void System.Linq.EnumerableSorter`1::.ctor()
// 0x00000081 System.Void System.Linq.EnumerableSorter`2::.ctor(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean,System.Linq.EnumerableSorter`1<TElement>)
// 0x00000082 System.Void System.Linq.EnumerableSorter`2::ComputeKeys(TElement[],System.Int32)
// 0x00000083 System.Int32 System.Linq.EnumerableSorter`2::CompareKeys(System.Int32,System.Int32)
// 0x00000084 System.Void System.Linq.Buffer`1::.ctor(System.Collections.Generic.IEnumerable`1<TElement>)
// 0x00000085 TElement[] System.Linq.Buffer`1::ToArray()
// 0x00000086 System.Linq.Expressions.ExpressionType System.Linq.Expressions.Expression::get_NodeType()
extern void Expression_get_NodeType_m230B6FE36D83B480D5ABBD01A1BA27A6F33BFCE3 ();
// 0x00000087 System.Void System.Linq.Expressions.Expression::.cctor()
extern void Expression__cctor_m6C47FA07B808E8507CCCEACDCFE8EE668AEC409C ();
// 0x00000088 System.Exception System.Linq.Expressions.Error::ExtensionNodeMustOverrideProperty(System.Object)
extern void Error_ExtensionNodeMustOverrideProperty_mB79A086A50C7F12802A53324D7AF8C4E7EE208C6 ();
// 0x00000089 System.Linq.Expressions.Expression System.Linq.Expressions.LambdaExpression::get_Body()
extern void LambdaExpression_get_Body_m7D6D43418EE260E6523847D5C9E866FC466C89D0 ();
// 0x0000008A System.Reflection.MemberInfo System.Linq.Expressions.MemberExpression::get_Member()
extern void MemberExpression_get_Member_m28610D227E14379794673549E914931312AA1272 ();
// 0x0000008B System.Linq.Expressions.Expression System.Linq.Expressions.MemberExpression::get_Expression()
extern void MemberExpression_get_Expression_m5FD4FC0BEE3268821D97C01C87BEB0C1F9D26351 ();
// 0x0000008C System.Reflection.MemberInfo System.Linq.Expressions.MemberExpression::GetMember()
extern void MemberExpression_GetMember_m35CBBB03CFAFC32A700D877C35A4CD70C8C3FCA2 ();
// 0x0000008D System.String System.Linq.Expressions.Strings::ExtensionNodeMustOverrideProperty(System.Object)
extern void Strings_ExtensionNodeMustOverrideProperty_m2468ED1C894BD378104B522523876152F9D1F0D0 ();
// 0x0000008E System.Void System.Dynamic.Utils.CacheDict`2::.ctor(System.Int32)
// 0x0000008F System.Int32 System.Dynamic.Utils.CacheDict`2::AlignSize(System.Int32)
// 0x00000090 System.Exception System.Dynamic.Utils.ContractUtils::get_Unreachable()
extern void ContractUtils_get_Unreachable_mE78FFD6E65C4EB6EA0FB53A98EBE5AA477309E2F ();
// 0x00000091 System.Void System.Collections.Generic.HashSet`1::.ctor()
// 0x00000092 System.Void System.Collections.Generic.HashSet`1::.ctor(System.Collections.Generic.IEqualityComparer`1<T>)
// 0x00000093 System.Void System.Collections.Generic.HashSet`1::.ctor(System.Collections.Generic.IEnumerable`1<T>)
// 0x00000094 System.Void System.Collections.Generic.HashSet`1::.ctor(System.Collections.Generic.IEnumerable`1<T>,System.Collections.Generic.IEqualityComparer`1<T>)
// 0x00000095 System.Void System.Collections.Generic.HashSet`1::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x00000096 System.Void System.Collections.Generic.HashSet`1::CopyFrom(System.Collections.Generic.HashSet`1<T>)
// 0x00000097 System.Void System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.Add(T)
// 0x00000098 System.Void System.Collections.Generic.HashSet`1::Clear()
// 0x00000099 System.Boolean System.Collections.Generic.HashSet`1::Contains(T)
// 0x0000009A System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32)
// 0x0000009B System.Boolean System.Collections.Generic.HashSet`1::Remove(T)
// 0x0000009C System.Int32 System.Collections.Generic.HashSet`1::get_Count()
// 0x0000009D System.Boolean System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
// 0x0000009E System.Collections.Generic.HashSet`1_Enumerator<T> System.Collections.Generic.HashSet`1::GetEnumerator()
// 0x0000009F System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.HashSet`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x000000A0 System.Collections.IEnumerator System.Collections.Generic.HashSet`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000000A1 System.Void System.Collections.Generic.HashSet`1::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x000000A2 System.Void System.Collections.Generic.HashSet`1::OnDeserialization(System.Object)
// 0x000000A3 System.Boolean System.Collections.Generic.HashSet`1::Add(T)
// 0x000000A4 System.Void System.Collections.Generic.HashSet`1::UnionWith(System.Collections.Generic.IEnumerable`1<T>)
// 0x000000A5 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[])
// 0x000000A6 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32,System.Int32)
// 0x000000A7 System.Collections.Generic.IEqualityComparer`1<T> System.Collections.Generic.HashSet`1::get_Comparer()
// 0x000000A8 System.Void System.Collections.Generic.HashSet`1::TrimExcess()
// 0x000000A9 System.Void System.Collections.Generic.HashSet`1::Initialize(System.Int32)
// 0x000000AA System.Void System.Collections.Generic.HashSet`1::IncreaseCapacity()
// 0x000000AB System.Void System.Collections.Generic.HashSet`1::SetCapacity(System.Int32)
// 0x000000AC System.Boolean System.Collections.Generic.HashSet`1::AddIfNotPresent(T)
// 0x000000AD System.Void System.Collections.Generic.HashSet`1::AddValue(System.Int32,System.Int32,T)
// 0x000000AE System.Boolean System.Collections.Generic.HashSet`1::AreEqualityComparersEqual(System.Collections.Generic.HashSet`1<T>,System.Collections.Generic.HashSet`1<T>)
// 0x000000AF System.Int32 System.Collections.Generic.HashSet`1::InternalGetHashCode(T)
// 0x000000B0 System.Void System.Collections.Generic.HashSet`1_Enumerator::.ctor(System.Collections.Generic.HashSet`1<T>)
// 0x000000B1 System.Void System.Collections.Generic.HashSet`1_Enumerator::Dispose()
// 0x000000B2 System.Boolean System.Collections.Generic.HashSet`1_Enumerator::MoveNext()
// 0x000000B3 T System.Collections.Generic.HashSet`1_Enumerator::get_Current()
// 0x000000B4 System.Object System.Collections.Generic.HashSet`1_Enumerator::System.Collections.IEnumerator.get_Current()
// 0x000000B5 System.Void System.Collections.Generic.HashSet`1_Enumerator::System.Collections.IEnumerator.Reset()
static Il2CppMethodPointer s_methodPointers[181] = 
{
	SR_Format_mC35B52FE843D9C9D465B6B544FA184058A46E0A9,
	Error_ArgumentNull_mCA126ED8F4F3B343A70E201C44B3A509690F1EA7,
	Error_ArgumentOutOfRange_mACFCB068F4E0C4EEF9E6EDDD59E798901C32C6C9,
	Error_MoreThanOneMatch_m85C3617F782E9F2333FC1FDF42821BE069F24623,
	Error_NoElements_m17188AC2CF25EB359A4E1DDE9518A98598791136,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	Expression_get_NodeType_m230B6FE36D83B480D5ABBD01A1BA27A6F33BFCE3,
	Expression__cctor_m6C47FA07B808E8507CCCEACDCFE8EE668AEC409C,
	Error_ExtensionNodeMustOverrideProperty_mB79A086A50C7F12802A53324D7AF8C4E7EE208C6,
	LambdaExpression_get_Body_m7D6D43418EE260E6523847D5C9E866FC466C89D0,
	MemberExpression_get_Member_m28610D227E14379794673549E914931312AA1272,
	MemberExpression_get_Expression_m5FD4FC0BEE3268821D97C01C87BEB0C1F9D26351,
	MemberExpression_GetMember_m35CBBB03CFAFC32A700D877C35A4CD70C8C3FCA2,
	Strings_ExtensionNodeMustOverrideProperty_m2468ED1C894BD378104B522523876152F9D1F0D0,
	NULL,
	NULL,
	ContractUtils_get_Unreachable_mE78FFD6E65C4EB6EA0FB53A98EBE5AA477309E2F,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
};
static const int32_t s_InvokerIndices[181] = 
{
	1,
	0,
	0,
	4,
	4,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	10,
	3,
	0,
	14,
	14,
	14,
	14,
	0,
	-1,
	-1,
	4,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
};
static const Il2CppTokenRangePair s_rgctxIndices[53] = 
{
	{ 0x02000005, { 77, 4 } },
	{ 0x02000006, { 81, 9 } },
	{ 0x02000007, { 92, 7 } },
	{ 0x02000008, { 101, 10 } },
	{ 0x02000009, { 113, 11 } },
	{ 0x0200000A, { 127, 9 } },
	{ 0x0200000B, { 139, 12 } },
	{ 0x0200000C, { 154, 1 } },
	{ 0x0200000D, { 155, 2 } },
	{ 0x0200000E, { 157, 12 } },
	{ 0x0200000F, { 169, 11 } },
	{ 0x02000010, { 180, 6 } },
	{ 0x02000012, { 186, 8 } },
	{ 0x02000014, { 194, 3 } },
	{ 0x02000015, { 199, 5 } },
	{ 0x02000016, { 204, 7 } },
	{ 0x02000017, { 211, 3 } },
	{ 0x02000018, { 214, 7 } },
	{ 0x02000019, { 221, 4 } },
	{ 0x02000024, { 225, 3 } },
	{ 0x02000027, { 228, 34 } },
	{ 0x02000029, { 262, 2 } },
	{ 0x06000006, { 0, 10 } },
	{ 0x06000007, { 10, 10 } },
	{ 0x06000008, { 20, 5 } },
	{ 0x06000009, { 25, 5 } },
	{ 0x0600000A, { 30, 1 } },
	{ 0x0600000B, { 31, 2 } },
	{ 0x0600000C, { 33, 2 } },
	{ 0x0600000D, { 35, 1 } },
	{ 0x0600000E, { 36, 1 } },
	{ 0x0600000F, { 37, 2 } },
	{ 0x06000010, { 39, 3 } },
	{ 0x06000011, { 42, 2 } },
	{ 0x06000012, { 44, 2 } },
	{ 0x06000013, { 46, 2 } },
	{ 0x06000014, { 48, 3 } },
	{ 0x06000015, { 51, 4 } },
	{ 0x06000016, { 55, 3 } },
	{ 0x06000017, { 58, 3 } },
	{ 0x06000018, { 61, 1 } },
	{ 0x06000019, { 62, 3 } },
	{ 0x0600001A, { 65, 2 } },
	{ 0x0600001B, { 67, 3 } },
	{ 0x0600001C, { 70, 2 } },
	{ 0x0600001D, { 72, 5 } },
	{ 0x0600002D, { 90, 2 } },
	{ 0x06000032, { 99, 2 } },
	{ 0x06000037, { 111, 2 } },
	{ 0x0600003D, { 124, 3 } },
	{ 0x06000042, { 136, 3 } },
	{ 0x06000047, { 151, 3 } },
	{ 0x06000072, { 197, 2 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[264] = 
{
	{ (Il2CppRGCTXDataType)2, 24465 },
	{ (Il2CppRGCTXDataType)3, 21115 },
	{ (Il2CppRGCTXDataType)2, 24466 },
	{ (Il2CppRGCTXDataType)2, 24467 },
	{ (Il2CppRGCTXDataType)3, 21116 },
	{ (Il2CppRGCTXDataType)2, 24468 },
	{ (Il2CppRGCTXDataType)2, 24469 },
	{ (Il2CppRGCTXDataType)3, 21117 },
	{ (Il2CppRGCTXDataType)2, 24470 },
	{ (Il2CppRGCTXDataType)3, 21118 },
	{ (Il2CppRGCTXDataType)2, 24471 },
	{ (Il2CppRGCTXDataType)3, 21119 },
	{ (Il2CppRGCTXDataType)2, 24472 },
	{ (Il2CppRGCTXDataType)2, 24473 },
	{ (Il2CppRGCTXDataType)3, 21120 },
	{ (Il2CppRGCTXDataType)2, 24474 },
	{ (Il2CppRGCTXDataType)2, 24475 },
	{ (Il2CppRGCTXDataType)3, 21121 },
	{ (Il2CppRGCTXDataType)2, 24476 },
	{ (Il2CppRGCTXDataType)3, 21122 },
	{ (Il2CppRGCTXDataType)2, 24477 },
	{ (Il2CppRGCTXDataType)3, 21123 },
	{ (Il2CppRGCTXDataType)3, 21124 },
	{ (Il2CppRGCTXDataType)2, 17898 },
	{ (Il2CppRGCTXDataType)3, 21125 },
	{ (Il2CppRGCTXDataType)2, 24478 },
	{ (Il2CppRGCTXDataType)3, 21126 },
	{ (Il2CppRGCTXDataType)3, 21127 },
	{ (Il2CppRGCTXDataType)2, 17905 },
	{ (Il2CppRGCTXDataType)3, 21128 },
	{ (Il2CppRGCTXDataType)3, 21129 },
	{ (Il2CppRGCTXDataType)2, 24479 },
	{ (Il2CppRGCTXDataType)3, 21130 },
	{ (Il2CppRGCTXDataType)2, 24480 },
	{ (Il2CppRGCTXDataType)3, 21131 },
	{ (Il2CppRGCTXDataType)3, 21132 },
	{ (Il2CppRGCTXDataType)3, 21133 },
	{ (Il2CppRGCTXDataType)2, 24481 },
	{ (Il2CppRGCTXDataType)3, 21134 },
	{ (Il2CppRGCTXDataType)2, 24482 },
	{ (Il2CppRGCTXDataType)3, 21135 },
	{ (Il2CppRGCTXDataType)3, 21136 },
	{ (Il2CppRGCTXDataType)2, 17935 },
	{ (Il2CppRGCTXDataType)3, 21137 },
	{ (Il2CppRGCTXDataType)2, 17936 },
	{ (Il2CppRGCTXDataType)3, 21138 },
	{ (Il2CppRGCTXDataType)2, 24483 },
	{ (Il2CppRGCTXDataType)3, 21139 },
	{ (Il2CppRGCTXDataType)2, 17940 },
	{ (Il2CppRGCTXDataType)2, 24484 },
	{ (Il2CppRGCTXDataType)3, 21140 },
	{ (Il2CppRGCTXDataType)2, 24485 },
	{ (Il2CppRGCTXDataType)2, 24486 },
	{ (Il2CppRGCTXDataType)2, 17943 },
	{ (Il2CppRGCTXDataType)2, 24487 },
	{ (Il2CppRGCTXDataType)2, 17945 },
	{ (Il2CppRGCTXDataType)2, 24488 },
	{ (Il2CppRGCTXDataType)3, 21141 },
	{ (Il2CppRGCTXDataType)2, 24489 },
	{ (Il2CppRGCTXDataType)2, 17948 },
	{ (Il2CppRGCTXDataType)2, 24490 },
	{ (Il2CppRGCTXDataType)2, 17950 },
	{ (Il2CppRGCTXDataType)2, 17952 },
	{ (Il2CppRGCTXDataType)2, 24491 },
	{ (Il2CppRGCTXDataType)3, 21142 },
	{ (Il2CppRGCTXDataType)2, 24492 },
	{ (Il2CppRGCTXDataType)2, 17955 },
	{ (Il2CppRGCTXDataType)2, 17957 },
	{ (Il2CppRGCTXDataType)2, 24493 },
	{ (Il2CppRGCTXDataType)3, 21143 },
	{ (Il2CppRGCTXDataType)2, 24494 },
	{ (Il2CppRGCTXDataType)3, 21144 },
	{ (Il2CppRGCTXDataType)3, 21145 },
	{ (Il2CppRGCTXDataType)2, 24495 },
	{ (Il2CppRGCTXDataType)2, 17962 },
	{ (Il2CppRGCTXDataType)2, 24496 },
	{ (Il2CppRGCTXDataType)2, 17964 },
	{ (Il2CppRGCTXDataType)3, 21146 },
	{ (Il2CppRGCTXDataType)3, 21147 },
	{ (Il2CppRGCTXDataType)2, 17967 },
	{ (Il2CppRGCTXDataType)3, 21148 },
	{ (Il2CppRGCTXDataType)3, 21149 },
	{ (Il2CppRGCTXDataType)2, 17979 },
	{ (Il2CppRGCTXDataType)2, 24497 },
	{ (Il2CppRGCTXDataType)3, 21150 },
	{ (Il2CppRGCTXDataType)3, 21151 },
	{ (Il2CppRGCTXDataType)2, 17981 },
	{ (Il2CppRGCTXDataType)2, 24324 },
	{ (Il2CppRGCTXDataType)3, 21152 },
	{ (Il2CppRGCTXDataType)3, 21153 },
	{ (Il2CppRGCTXDataType)2, 24498 },
	{ (Il2CppRGCTXDataType)3, 21154 },
	{ (Il2CppRGCTXDataType)3, 21155 },
	{ (Il2CppRGCTXDataType)2, 17991 },
	{ (Il2CppRGCTXDataType)2, 24499 },
	{ (Il2CppRGCTXDataType)3, 21156 },
	{ (Il2CppRGCTXDataType)3, 21157 },
	{ (Il2CppRGCTXDataType)3, 20443 },
	{ (Il2CppRGCTXDataType)3, 21158 },
	{ (Il2CppRGCTXDataType)2, 24500 },
	{ (Il2CppRGCTXDataType)3, 21159 },
	{ (Il2CppRGCTXDataType)3, 21160 },
	{ (Il2CppRGCTXDataType)2, 18003 },
	{ (Il2CppRGCTXDataType)2, 24501 },
	{ (Il2CppRGCTXDataType)3, 21161 },
	{ (Il2CppRGCTXDataType)3, 21162 },
	{ (Il2CppRGCTXDataType)3, 21163 },
	{ (Il2CppRGCTXDataType)3, 21164 },
	{ (Il2CppRGCTXDataType)3, 21165 },
	{ (Il2CppRGCTXDataType)3, 20449 },
	{ (Il2CppRGCTXDataType)3, 21166 },
	{ (Il2CppRGCTXDataType)2, 24502 },
	{ (Il2CppRGCTXDataType)3, 21167 },
	{ (Il2CppRGCTXDataType)3, 21168 },
	{ (Il2CppRGCTXDataType)2, 18016 },
	{ (Il2CppRGCTXDataType)2, 24503 },
	{ (Il2CppRGCTXDataType)3, 21169 },
	{ (Il2CppRGCTXDataType)3, 21170 },
	{ (Il2CppRGCTXDataType)2, 18018 },
	{ (Il2CppRGCTXDataType)2, 24504 },
	{ (Il2CppRGCTXDataType)3, 21171 },
	{ (Il2CppRGCTXDataType)3, 21172 },
	{ (Il2CppRGCTXDataType)2, 24505 },
	{ (Il2CppRGCTXDataType)3, 21173 },
	{ (Il2CppRGCTXDataType)3, 21174 },
	{ (Il2CppRGCTXDataType)2, 24506 },
	{ (Il2CppRGCTXDataType)3, 21175 },
	{ (Il2CppRGCTXDataType)3, 21176 },
	{ (Il2CppRGCTXDataType)2, 18033 },
	{ (Il2CppRGCTXDataType)2, 24507 },
	{ (Il2CppRGCTXDataType)3, 21177 },
	{ (Il2CppRGCTXDataType)3, 21178 },
	{ (Il2CppRGCTXDataType)3, 21179 },
	{ (Il2CppRGCTXDataType)3, 20460 },
	{ (Il2CppRGCTXDataType)2, 24508 },
	{ (Il2CppRGCTXDataType)3, 21180 },
	{ (Il2CppRGCTXDataType)3, 21181 },
	{ (Il2CppRGCTXDataType)2, 24509 },
	{ (Il2CppRGCTXDataType)3, 21182 },
	{ (Il2CppRGCTXDataType)3, 21183 },
	{ (Il2CppRGCTXDataType)2, 18049 },
	{ (Il2CppRGCTXDataType)2, 24510 },
	{ (Il2CppRGCTXDataType)3, 21184 },
	{ (Il2CppRGCTXDataType)3, 21185 },
	{ (Il2CppRGCTXDataType)3, 21186 },
	{ (Il2CppRGCTXDataType)3, 21187 },
	{ (Il2CppRGCTXDataType)3, 21188 },
	{ (Il2CppRGCTXDataType)3, 21189 },
	{ (Il2CppRGCTXDataType)3, 20466 },
	{ (Il2CppRGCTXDataType)2, 24511 },
	{ (Il2CppRGCTXDataType)3, 21190 },
	{ (Il2CppRGCTXDataType)3, 21191 },
	{ (Il2CppRGCTXDataType)2, 24512 },
	{ (Il2CppRGCTXDataType)3, 21192 },
	{ (Il2CppRGCTXDataType)3, 21193 },
	{ (Il2CppRGCTXDataType)3, 21194 },
	{ (Il2CppRGCTXDataType)3, 21195 },
	{ (Il2CppRGCTXDataType)3, 21196 },
	{ (Il2CppRGCTXDataType)3, 21197 },
	{ (Il2CppRGCTXDataType)2, 24513 },
	{ (Il2CppRGCTXDataType)2, 24514 },
	{ (Il2CppRGCTXDataType)3, 21198 },
	{ (Il2CppRGCTXDataType)2, 18084 },
	{ (Il2CppRGCTXDataType)2, 18078 },
	{ (Il2CppRGCTXDataType)3, 21199 },
	{ (Il2CppRGCTXDataType)2, 18077 },
	{ (Il2CppRGCTXDataType)2, 24515 },
	{ (Il2CppRGCTXDataType)3, 21200 },
	{ (Il2CppRGCTXDataType)3, 21201 },
	{ (Il2CppRGCTXDataType)3, 21202 },
	{ (Il2CppRGCTXDataType)2, 24516 },
	{ (Il2CppRGCTXDataType)3, 21203 },
	{ (Il2CppRGCTXDataType)2, 18100 },
	{ (Il2CppRGCTXDataType)2, 18092 },
	{ (Il2CppRGCTXDataType)3, 21204 },
	{ (Il2CppRGCTXDataType)3, 21205 },
	{ (Il2CppRGCTXDataType)2, 18091 },
	{ (Il2CppRGCTXDataType)2, 24517 },
	{ (Il2CppRGCTXDataType)3, 21206 },
	{ (Il2CppRGCTXDataType)3, 21207 },
	{ (Il2CppRGCTXDataType)3, 21208 },
	{ (Il2CppRGCTXDataType)2, 18104 },
	{ (Il2CppRGCTXDataType)3, 21209 },
	{ (Il2CppRGCTXDataType)2, 24518 },
	{ (Il2CppRGCTXDataType)3, 21210 },
	{ (Il2CppRGCTXDataType)3, 21211 },
	{ (Il2CppRGCTXDataType)3, 21212 },
	{ (Il2CppRGCTXDataType)2, 24519 },
	{ (Il2CppRGCTXDataType)2, 24520 },
	{ (Il2CppRGCTXDataType)3, 21213 },
	{ (Il2CppRGCTXDataType)3, 21214 },
	{ (Il2CppRGCTXDataType)2, 18120 },
	{ (Il2CppRGCTXDataType)3, 21215 },
	{ (Il2CppRGCTXDataType)2, 18121 },
	{ (Il2CppRGCTXDataType)2, 24521 },
	{ (Il2CppRGCTXDataType)3, 21216 },
	{ (Il2CppRGCTXDataType)3, 21217 },
	{ (Il2CppRGCTXDataType)2, 24522 },
	{ (Il2CppRGCTXDataType)3, 21218 },
	{ (Il2CppRGCTXDataType)2, 24523 },
	{ (Il2CppRGCTXDataType)3, 21219 },
	{ (Il2CppRGCTXDataType)3, 21220 },
	{ (Il2CppRGCTXDataType)3, 21221 },
	{ (Il2CppRGCTXDataType)2, 18140 },
	{ (Il2CppRGCTXDataType)3, 21222 },
	{ (Il2CppRGCTXDataType)2, 18148 },
	{ (Il2CppRGCTXDataType)3, 21223 },
	{ (Il2CppRGCTXDataType)2, 24524 },
	{ (Il2CppRGCTXDataType)2, 24525 },
	{ (Il2CppRGCTXDataType)3, 21224 },
	{ (Il2CppRGCTXDataType)3, 21225 },
	{ (Il2CppRGCTXDataType)3, 21226 },
	{ (Il2CppRGCTXDataType)3, 21227 },
	{ (Il2CppRGCTXDataType)3, 21228 },
	{ (Il2CppRGCTXDataType)3, 21229 },
	{ (Il2CppRGCTXDataType)2, 18164 },
	{ (Il2CppRGCTXDataType)2, 24526 },
	{ (Il2CppRGCTXDataType)3, 21230 },
	{ (Il2CppRGCTXDataType)3, 21231 },
	{ (Il2CppRGCTXDataType)2, 18168 },
	{ (Il2CppRGCTXDataType)3, 21232 },
	{ (Il2CppRGCTXDataType)2, 24527 },
	{ (Il2CppRGCTXDataType)2, 18178 },
	{ (Il2CppRGCTXDataType)2, 18176 },
	{ (Il2CppRGCTXDataType)2, 24528 },
	{ (Il2CppRGCTXDataType)3, 21233 },
	{ (Il2CppRGCTXDataType)2, 24529 },
	{ (Il2CppRGCTXDataType)2, 24530 },
	{ (Il2CppRGCTXDataType)3, 21234 },
	{ (Il2CppRGCTXDataType)2, 24531 },
	{ (Il2CppRGCTXDataType)3, 21235 },
	{ (Il2CppRGCTXDataType)3, 21236 },
	{ (Il2CppRGCTXDataType)2, 18218 },
	{ (Il2CppRGCTXDataType)3, 21237 },
	{ (Il2CppRGCTXDataType)2, 18218 },
	{ (Il2CppRGCTXDataType)3, 21238 },
	{ (Il2CppRGCTXDataType)2, 18235 },
	{ (Il2CppRGCTXDataType)3, 21239 },
	{ (Il2CppRGCTXDataType)3, 21240 },
	{ (Il2CppRGCTXDataType)3, 21241 },
	{ (Il2CppRGCTXDataType)2, 24532 },
	{ (Il2CppRGCTXDataType)3, 21242 },
	{ (Il2CppRGCTXDataType)3, 21243 },
	{ (Il2CppRGCTXDataType)3, 21244 },
	{ (Il2CppRGCTXDataType)2, 18215 },
	{ (Il2CppRGCTXDataType)3, 21245 },
	{ (Il2CppRGCTXDataType)3, 21246 },
	{ (Il2CppRGCTXDataType)2, 18220 },
	{ (Il2CppRGCTXDataType)3, 21247 },
	{ (Il2CppRGCTXDataType)1, 24533 },
	{ (Il2CppRGCTXDataType)2, 18219 },
	{ (Il2CppRGCTXDataType)3, 21248 },
	{ (Il2CppRGCTXDataType)1, 18219 },
	{ (Il2CppRGCTXDataType)1, 18215 },
	{ (Il2CppRGCTXDataType)2, 24532 },
	{ (Il2CppRGCTXDataType)2, 18219 },
	{ (Il2CppRGCTXDataType)2, 18217 },
	{ (Il2CppRGCTXDataType)2, 18221 },
	{ (Il2CppRGCTXDataType)3, 21249 },
	{ (Il2CppRGCTXDataType)3, 21250 },
	{ (Il2CppRGCTXDataType)3, 21251 },
	{ (Il2CppRGCTXDataType)2, 18216 },
	{ (Il2CppRGCTXDataType)3, 21252 },
	{ (Il2CppRGCTXDataType)2, 18231 },
};
extern const Il2CppCodeGenModule g_System_CoreCodeGenModule;
const Il2CppCodeGenModule g_System_CoreCodeGenModule = 
{
	"System.Core.dll",
	181,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	53,
	s_rgctxIndices,
	264,
	s_rgctxValues,
	NULL,
};
