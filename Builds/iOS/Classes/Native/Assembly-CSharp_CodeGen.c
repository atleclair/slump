﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void GameOverMenu::RestartGame()
extern void GameOverMenu_RestartGame_m4A041A23D9426E40157CF5016536DB122A94C840 ();
// 0x00000002 System.Void GameOverMenu::BackToMenu()
extern void GameOverMenu_BackToMenu_mDD1212E86D9E7AD1D9DE2CBD8EC4EE218F1595B5 ();
// 0x00000003 System.Void GameOverMenu::.ctor()
extern void GameOverMenu__ctor_m398A846133DBF19CBFC9CE4A237CF0026E881AA7 ();
// 0x00000004 System.Void ManagePerimeter::.ctor()
extern void ManagePerimeter__ctor_mC7BFA403B5D458A12336C86E8FE870ED1AA82585 ();
// 0x00000005 System.Void AimAndAttack::Start()
extern void AimAndAttack_Start_m27AC2D3B1B12B155864C4C61B927E12121791E9F ();
// 0x00000006 System.Void AimAndAttack::Update()
extern void AimAndAttack_Update_mFDA390CCB213C2FB04E654F7CE49E9A61C5AF0D9 ();
// 0x00000007 System.Void AimAndAttack::CheckDistance()
extern void AimAndAttack_CheckDistance_m3000D5D89B2CE82DDFA1FA524B609426E581C888 ();
// 0x00000008 System.Collections.IEnumerator AimAndAttack::WaitTime()
extern void AimAndAttack_WaitTime_mC71406FE4B30820FF614B275D4E9333D3853417E ();
// 0x00000009 System.Collections.IEnumerator AimAndAttack::Attack(UnityEngine.Vector3)
extern void AimAndAttack_Attack_m8C20C41412C9EB187DFF726E2235491F1DCC6827 ();
// 0x0000000A System.Void AimAndAttack::OnCollisionEnter2D(UnityEngine.Collision2D)
extern void AimAndAttack_OnCollisionEnter2D_m01E490BB18DCC1DF43319B1DC9097E1E433A9AD0 ();
// 0x0000000B System.Void AimAndAttack::.ctor()
extern void AimAndAttack__ctor_mE7EF81A0431220D7D7340FFEF2FFB65181F9978E ();
// 0x0000000C System.Void EnemyTouch::.ctor()
extern void EnemyTouch__ctor_mFDD2FE38EDDF2064C6B0A1ED339A8DF55CFA3AE8 ();
// 0x0000000D System.Void FireFly::Start()
extern void FireFly_Start_mE0FF4EA07DDE723AADD5BD01CC44F61379F11E52 ();
// 0x0000000E System.Void FireFly::Update()
extern void FireFly_Update_mB9C34AB0D9CFE4C874E334BF12C0FA8531438B54 ();
// 0x0000000F System.Collections.IEnumerator FireFly::PrepAttack(System.Single)
extern void FireFly_PrepAttack_m9C164D2AAF3EFD923E0802FB9A083BEACC992F02 ();
// 0x00000010 System.Collections.IEnumerator FireFly::ChargeLightning()
extern void FireFly_ChargeLightning_m19DECA1BA654F1FD09C27F41D1FD795D212B2363 ();
// 0x00000011 System.Collections.IEnumerator FireFly::ShootLightning()
extern void FireFly_ShootLightning_m94EDC436FA191956273D9B2D6C23AC7E11197206 ();
// 0x00000012 System.Void FireFly::OnCollisionEnter2D(UnityEngine.Collision2D)
extern void FireFly_OnCollisionEnter2D_m4FB81B087059CCFA062775D4BA6554D109250F7D ();
// 0x00000013 System.Void FireFly::.ctor()
extern void FireFly__ctor_mD9F6583A4D3F7EBC638665616236D4B81395D4D8 ();
// 0x00000014 System.Void FlyTrap::Awake()
extern void FlyTrap_Awake_m20F9C5A000CA32769FE6A9494101BF95F476137F ();
// 0x00000015 System.Void FlyTrap::OnCollisionEnter2D(UnityEngine.Collision2D)
extern void FlyTrap_OnCollisionEnter2D_m0541DB938A942248211961306701DD000755F577 ();
// 0x00000016 System.Collections.IEnumerator FlyTrap::Attack()
extern void FlyTrap_Attack_mDF9B00839C94A20DF13CB00722F9244AD558F85A ();
// 0x00000017 System.Void FlyTrap::Update()
extern void FlyTrap_Update_mC23F71056FF45F1EF6A3B001496658CE94125694 ();
// 0x00000018 System.Void FlyTrap::.ctor()
extern void FlyTrap__ctor_m04A42C50F7D56A86F2F1AB2C75486F841A6376BA ();
// 0x00000019 System.Void LightningAttack::OnCollisionEnter2D(UnityEngine.Collision2D)
extern void LightningAttack_OnCollisionEnter2D_m827FF2F47AC63368934777A8D45B16C97ADD4A66 ();
// 0x0000001A System.Void LightningAttack::.ctor()
extern void LightningAttack__ctor_m736BEAF9A46979F105DABC55019977B67C86FBEA ();
// 0x0000001B System.Void Patrol::Start()
extern void Patrol_Start_m3A4024FE7C256346D6AB36F725E4A2245D03F45C ();
// 0x0000001C System.Void Patrol::Update()
extern void Patrol_Update_mBD2B1D83FD0B56D05AC0581CC99A6A7073F4A441 ();
// 0x0000001D System.Void Patrol::PatrolPath(System.Single,System.Single)
extern void Patrol_PatrolPath_m375E0C5404A5F2C63D2D1B714818DC8FA053CB6A ();
// 0x0000001E System.Void Patrol::PausePatrol()
extern void Patrol_PausePatrol_m81ADF0499457D837D276E14BBA66F58916E9A9CE ();
// 0x0000001F System.Void Patrol::ResumePatrol()
extern void Patrol_ResumePatrol_m64CDE69E7C00A09F7434066D96DF3E6108B45CAA ();
// 0x00000020 System.Void Patrol::.ctor()
extern void Patrol__ctor_mCC3DA6E03C5863BB40538EED0F518F46CE66831D ();
// 0x00000021 System.Void BreakObject::Update()
extern void BreakObject_Update_m901A5E63EF08C0799D83534B30655C9F94F66913 ();
// 0x00000022 System.Void BreakObject::OnCollisionEnter2D(UnityEngine.Collision2D)
extern void BreakObject_OnCollisionEnter2D_m2ABF93DE7C1D07A34EFCBB5F3544A88D2F40C547 ();
// 0x00000023 System.Void BreakObject::.ctor()
extern void BreakObject__ctor_mC8B303EDC65E897BC9A87305759CEDF4D295B316 ();
// 0x00000024 System.Void CollectCoin::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void CollectCoin_OnTriggerEnter2D_m66AA818AED90FD132656B8BC85BF9637FE464AFD ();
// 0x00000025 System.Void CollectCoin::.ctor()
extern void CollectCoin__ctor_m2604E41E17F059AB6CEA0663A80FB8F30793C0FD ();
// 0x00000026 System.Void EnemyEnergy::Start()
extern void EnemyEnergy_Start_mE28802C8C093DDF7618DC1AD7012A6BA7A5FD8E1 ();
// 0x00000027 System.Void EnemyEnergy::Update()
extern void EnemyEnergy_Update_m02F5892F620A38840BEAF6877DE91BDAE43BADA9 ();
// 0x00000028 System.Void EnemyEnergy::.ctor()
extern void EnemyEnergy__ctor_m8B04CE902205EDEC5A8068E0784E5D618893D117 ();
// 0x00000029 System.Void FindGrapplingPoint::.ctor()
extern void FindGrapplingPoint__ctor_m6BE9F6EDBB2456A31E96942CF561CE2A3052160C ();
// 0x0000002A System.Void Gem::SetValues(System.Single,System.Int32)
extern void Gem_SetValues_m90DDCAB6480665FF5736FBB00FE47F62F931E420 ();
// 0x0000002B System.Void Gem::Start()
extern void Gem_Start_mB0F178AEE04F6B014AE575686729CB3BE73DD59D ();
// 0x0000002C System.Void Gem::Update()
extern void Gem_Update_mBCFA445505512609B1D0A045F567DDC85CC3F2CB ();
// 0x0000002D System.Void Gem::ThrowGem()
extern void Gem_ThrowGem_mD065029B005F13AECAC985525165F5A981AC746E ();
// 0x0000002E System.Void Gem::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void Gem_OnTriggerEnter2D_m40F7365F96899F34BDD0C0DCFFD523197C135566 ();
// 0x0000002F System.Void Gem::.ctor()
extern void Gem__ctor_m81577DAC39344D4EE83A17B5A836838ACA996636 ();
// 0x00000030 System.Void GrapplingHook::Start()
extern void GrapplingHook_Start_m5C54D663CC9F37744E8D8ED5A7171E428A831EEB ();
// 0x00000031 System.Void GrapplingHook::.ctor()
extern void GrapplingHook__ctor_mD5EBF32749DDDCB0A4E2631E14FD8BB497DBE610 ();
// 0x00000032 System.Void KillWall::OnCollisionEnter2D(UnityEngine.Collision2D)
extern void KillWall_OnCollisionEnter2D_m7829F8CA997A3A5676CACF5CD3265EA5A1AF20BD ();
// 0x00000033 System.Void KillWall::Update()
extern void KillWall_Update_m3AD886F9410E3E4EDC3F42A5E463A84F1DD8253F ();
// 0x00000034 System.Void KillWall::.ctor()
extern void KillWall__ctor_m1D7582E2BC825C188FCDE1973FEB2EC934AAE543 ();
// 0x00000035 System.Void Parallax::Start()
extern void Parallax_Start_m9774B4042DAD92E115B83BBCD41372C3769FE9AF ();
// 0x00000036 System.Void Parallax::Update()
extern void Parallax_Update_m94262B99102F6FF508B5AC680447AB5FF6230306 ();
// 0x00000037 System.Void Parallax::.ctor()
extern void Parallax__ctor_m54405085336E416F633A4B952F796BF1333228B9 ();
// 0x00000038 System.Void Ruby::Start()
extern void Ruby_Start_m927E3BEE3A4C812DD47F5AE9E2BAEB1BA90C4041 ();
// 0x00000039 System.Void Ruby::Update()
extern void Ruby_Update_m113F4A09C3A2DDA5A9A3C2A119C75A6DEEC30A4D ();
// 0x0000003A System.Void Ruby::.ctor()
extern void Ruby__ctor_mF8F52F96098FD06A02030A502C70C3B179F3F461 ();
// 0x0000003B System.Void ScreenShake::.ctor()
extern void ScreenShake__ctor_m486EB047157A617284A82D293E1ECCBD5F56EC62 ();
// 0x0000003C System.Void GameManager::Awake()
extern void GameManager_Awake_mE60F41F3186E80B2BAB293918745366D18508C0F ();
// 0x0000003D System.Void GameManager::Update()
extern void GameManager_Update_m07DC32583BF09EB71183725B7B95FA7B4716988A ();
// 0x0000003E System.Void GameManager::GameOver()
extern void GameManager_GameOver_m658E745BE197CC36CFAE422F8037F41A1B7FFFD3 ();
// 0x0000003F System.Void GameManager::RestartGame()
extern void GameManager_RestartGame_m1503EDBB7CBD522A762D8B37114E7AC96659FA86 ();
// 0x00000040 System.Boolean GameManager::IsCharacterAlive()
extern void GameManager_IsCharacterAlive_mD1D62CC44BED4D6F137E3819EC14F4B52F777F09 ();
// 0x00000041 System.Void GameManager::KillPlayer()
extern void GameManager_KillPlayer_m03AD86B9DBDD88EC3DCB535059ACD39957A55D79 ();
// 0x00000042 System.Void GameManager::EnemyKilled(UnityEngine.Vector2)
extern void GameManager_EnemyKilled_m4F03B343CBB34287ACB587BB9DA9AC03BF7793A2 ();
// 0x00000043 System.Collections.IEnumerator GameManager::SpawnEnemyEnergy(UnityEngine.GameObject,UnityEngine.Vector3)
extern void GameManager_SpawnEnemyEnergy_m6ED2C35DE160BD07279EE53FD795869C01CFDA1C ();
// 0x00000044 System.Void GameManager::.ctor()
extern void GameManager__ctor_mF7F1107D38DE91EB8A57C1C3BB1A932C50CD9693 ();
// 0x00000045 System.Void InputHandler::Awake()
extern void InputHandler_Awake_m5F930CA361CCBB6FF91D8242D0F5F34140B2C170 ();
// 0x00000046 System.Void InputHandler::Update()
extern void InputHandler_Update_m7835D52BDC816E70F683678D1B5EF5EE906DD4FA ();
// 0x00000047 System.Boolean InputHandler::TouchDown()
extern void InputHandler_TouchDown_m0FB9FF7EA7CEF7ABAB1C14294C50EB91F4031D88 ();
// 0x00000048 System.Boolean InputHandler::TouchUp()
extern void InputHandler_TouchUp_mFECC38B0A71344ED7F638109BB0B141F10ABA8A7 ();
// 0x00000049 System.Void InputHandler::.ctor()
extern void InputHandler__ctor_mE091B2759D08A7D2A019C9734ADD5B39D4ACD11F ();
// 0x0000004A System.Void Inventory::Awake()
extern void Inventory_Awake_m03B255C17B082423F82DF256F14EDB7A4912B21E ();
// 0x0000004B System.Void Inventory::Start()
extern void Inventory_Start_mF9115FBB18A7772CAD4ED60B31A2F128DA55DF83 ();
// 0x0000004C System.Void Inventory::AddToInventory(Gem)
extern void Inventory_AddToInventory_m5F8D81D95C617A278A66AA03A8C2884D3D250675 ();
// 0x0000004D Gem Inventory::RemoveFromInventory()
extern void Inventory_RemoveFromInventory_mB25EA0FF2138F1ADA439A3B4EF24A33B8DF65DD8 ();
// 0x0000004E System.Single Inventory::GetBackpackWeight()
extern void Inventory_GetBackpackWeight_m4B237C7B802B358B432B942FCE6404170F3F3812 ();
// 0x0000004F System.Single Inventory::GetTotalCurrency()
extern void Inventory_GetTotalCurrency_mBB33F7DE191BF1D091C1BAABADAB130ECC1DF1F6 ();
// 0x00000050 System.Void Inventory::.ctor()
extern void Inventory__ctor_mB946DCD27224D66DDEE96C7EE8355A7E3FE91CC7 ();
// 0x00000051 System.Void MainMenu::PlayGame()
extern void MainMenu_PlayGame_m4CD3D61E23D84AD1A018C84D561EAE39ED2D76F7 ();
// 0x00000052 System.Void MainMenu::.ctor()
extern void MainMenu__ctor_mF17B753D99BD98B88E949A5B9CA53892E19A6CD5 ();
// 0x00000053 System.Void ManageScore::Awake()
extern void ManageScore_Awake_mF2DCFD41E18C8068C3CF8D216F844294142D1C9F ();
// 0x00000054 System.Void ManageScore::Start()
extern void ManageScore_Start_m696BA5C87720F42307E1B18C4F443D8A1CF84E9F ();
// 0x00000055 System.Void ManageScore::AddPoints()
extern void ManageScore_AddPoints_m9ACFC712DD0D66352887F8052737ABFE57D7EE4F ();
// 0x00000056 System.Void ManageScore::SubtractEnergy()
extern void ManageScore_SubtractEnergy_mC7F98068A28D3FFD8878A1B8E0C0AD1658D08C70 ();
// 0x00000057 System.Void ManageScore::AddEnergy()
extern void ManageScore_AddEnergy_mACDB07BF15F1E20CFA126F6EFB6941FF898DCD66 ();
// 0x00000058 System.Int32 ManageScore::GetPoints()
extern void ManageScore_GetPoints_m3DA9EA5BD95AF657C8D4C363F13A0AEAEC88977C ();
// 0x00000059 System.Int32 ManageScore::GetEnergy()
extern void ManageScore_GetEnergy_mB4ECF116F0FA510882ECC7F8BA447DA44962B3EE ();
// 0x0000005A System.Void ManageScore::SetDistance(System.Single)
extern void ManageScore_SetDistance_m2908A68F86688117DA6EF4FC57C0AA08ACE8D85C ();
// 0x0000005B System.Single ManageScore::GetDistance()
extern void ManageScore_GetDistance_m007DC3BBB0AE74387730FC52CF1F4EE01ECFC291 ();
// 0x0000005C System.Void ManageScore::.ctor()
extern void ManageScore__ctor_m5FF340600B9BF4B8A0D764C866BFD4A6328DC4DB ();
// 0x0000005D System.Void ManageUI::Awake()
extern void ManageUI_Awake_m1EE1D672DA2CEF51CA96841E1FED3DF3ACF82501 ();
// 0x0000005E System.Void ManageUI::Update()
extern void ManageUI_Update_m56C88C58A98CC39657FB218A2ECE1891A8E02B60 ();
// 0x0000005F System.Void ManageUI::UpdateText()
extern void ManageUI_UpdateText_m486D9F4840B41F3A0DE338DE4913C6CE87CC7A93 ();
// 0x00000060 System.Void ManageUI::HideScoreText()
extern void ManageUI_HideScoreText_m99EDE7F6A8A02EAB62302D6541E7C2A0F895DE04 ();
// 0x00000061 System.Void ManageUI::ShowScoreText()
extern void ManageUI_ShowScoreText_mAFFCCD28AABF6FEC6D8DE40D019A588902E068F9 ();
// 0x00000062 System.Void ManageUI::.ctor()
extern void ManageUI__ctor_mD417ED31B5D59C8A74AE43FACC287823710CA760 ();
// 0x00000063 System.Void ObjectPooler::Awake()
extern void ObjectPooler_Awake_mE25E4226F8F8927FFA714DB869B4002C498520E8 ();
// 0x00000064 UnityEngine.GameObject ObjectPooler::getPooledObject()
extern void ObjectPooler_getPooledObject_m4D0F46D453C8F695E32F9F2F60731676A6F1ADC8 ();
// 0x00000065 System.Void ObjectPooler::CheckForInactive()
extern void ObjectPooler_CheckForInactive_m085476FD0B27F64F2862FF39FB8CE3610DC096AE ();
// 0x00000066 System.Void ObjectPooler::PrepareForReuse(UnityEngine.GameObject)
extern void ObjectPooler_PrepareForReuse_mC9773ADBDED1E5BFE852FD1CA5E0700B1EBD998D ();
// 0x00000067 System.Void ObjectPooler::.ctor()
extern void ObjectPooler__ctor_m649927772575A33D98CA8599915DEB5C0FD18352 ();
// 0x00000068 System.Void PlayAgain::RestartGame()
extern void PlayAgain_RestartGame_mD745D4B0812D2E9B48506F01D888DBBE63D98467 ();
// 0x00000069 System.Void PlayAgain::BackToMenu()
extern void PlayAgain_BackToMenu_mC1939C52A5440D56BCD3202A773AB07BE9F860EF ();
// 0x0000006A System.Void PlayAgain::.ctor()
extern void PlayAgain__ctor_m866DC30F2DD4E2260CF6E2DB49E87BEEEB74375E ();
// 0x0000006B System.Void SpawnEnemies::Start()
extern void SpawnEnemies_Start_m87D7F16DF1338668EF6A52B7A2E9395BD628CAE6 ();
// 0x0000006C System.Void SpawnEnemies::Update()
extern void SpawnEnemies_Update_m4A15B4C624986D1D57771DBB11364843535E8F91 ();
// 0x0000006D System.Void SpawnEnemies::.ctor()
extern void SpawnEnemies__ctor_m13F88F19FC7CEE95779AAD5950265F61444894FA ();
// 0x0000006E System.Void SpawnGrapplingPoints::Start()
extern void SpawnGrapplingPoints_Start_m05D29FF2CAB366F64D7CCE03DE9CDB93EA97607C ();
// 0x0000006F System.Void SpawnGrapplingPoints::Update()
extern void SpawnGrapplingPoints_Update_m3F6415C21FB814192F729A47128B00A88EEBC56E ();
// 0x00000070 System.Void SpawnGrapplingPoints::SwapPoints(UnityEngine.GameObject)
extern void SpawnGrapplingPoints_SwapPoints_m0DB3E30E8DC9FE8B8201FC1049CC8B4219C79638 ();
// 0x00000071 System.Void SpawnGrapplingPoints::.ctor()
extern void SpawnGrapplingPoints__ctor_m9246271CC59C3CDDCB2BFCAAD22B06A2FF9C411A ();
// 0x00000072 System.Void SpawnObjects::Start()
extern void SpawnObjects_Start_m290BD8FF74C84B0278F7F3767DE9209325D583A3 ();
// 0x00000073 System.Void SpawnObjects::Update()
extern void SpawnObjects_Update_mC4BFD9B7253705E0AD0A79D7AD2F58AB3ED56075 ();
// 0x00000074 System.Void SpawnObjects::SpawnObstacle(System.Single)
extern void SpawnObjects_SpawnObstacle_mC3B4B5FA49B259C32F9557C187446886D4DFC0A6 ();
// 0x00000075 System.Void SpawnObjects::SpawnCoin(System.Single)
extern void SpawnObjects_SpawnCoin_m0A3D4EB46699D4A16C18AAC69701130AE4F72BA8 ();
// 0x00000076 System.Void SpawnObjects::DestroyObjects()
extern void SpawnObjects_DestroyObjects_m9F5666BEBF79A87AB49FA6AD0D25C80CCB7A0010 ();
// 0x00000077 System.Void SpawnObjects::PrepareStage()
extern void SpawnObjects_PrepareStage_m07C8408CA81998E8372E8D9B6A1D3234D906BC2B ();
// 0x00000078 System.Void SpawnObjects::SpawnEnemy()
extern void SpawnObjects_SpawnEnemy_m75AD83486289CA2262DAC56F8B3B51E30547C18A ();
// 0x00000079 System.Void SpawnObjects::.ctor()
extern void SpawnObjects__ctor_mE6D3F0E539008648986DA4D502C4A00A596D3CB4 ();
// 0x0000007A System.Void SpawnWalls::Start()
extern void SpawnWalls_Start_m505B9A978AC3EF486829D5D8A99CFEB41172F4A8 ();
// 0x0000007B System.Void SpawnWalls::Update()
extern void SpawnWalls_Update_m76AA6719B5F83DECCCB25C320D12AE4254F91C33 ();
// 0x0000007C System.Void SpawnWalls::SpawnWall()
extern void SpawnWalls_SpawnWall_m4DD2141ABE093BD78AA18744892E9E2529ECD332 ();
// 0x0000007D System.Single SpawnWalls::GetWallWidth()
extern void SpawnWalls_GetWallWidth_mAF3FE9E0AA4A2C617C8D93C1AB7B4B503B4B2F23 ();
// 0x0000007E System.Void SpawnWalls::.ctor()
extern void SpawnWalls__ctor_m2ACE656DD9575C51F590F5134AAD7C83DDD4C9AE ();
// 0x0000007F System.Void TimeManager::Start()
extern void TimeManager_Start_m2A75447102599AF3F3B8EA72DC92DE6D464F7EB9 ();
// 0x00000080 System.Void TimeManager::Update()
extern void TimeManager_Update_mD4BBFB572CA604D72BF9459EFF493A41E565DBCD ();
// 0x00000081 System.Void TimeManager::SlowmoOn()
extern void TimeManager_SlowmoOn_m89CCE84A14F2E22AC83AB2510D8ADDF67404C00A ();
// 0x00000082 System.Void TimeManager::SlowmoOff()
extern void TimeManager_SlowmoOff_m1B0DDAD2F0D42D1485C8C6380F00B674F1CC452A ();
// 0x00000083 System.Void TimeManager::.ctor()
extern void TimeManager__ctor_m076923E11728FA19B987632C634A7348083042B0 ();
// 0x00000084 System.Void AttackEnemy::Start()
extern void AttackEnemy_Start_m421C3198F9197C4EEE47F98FDB177DED5FF15E31 ();
// 0x00000085 System.Void AttackEnemy::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void AttackEnemy_OnTriggerEnter2D_mF728A5F43B62CB40203067326DC9F71276D2287C ();
// 0x00000086 System.Void AttackEnemy::Update()
extern void AttackEnemy_Update_m342DDBE033FCD71B07208FD7E6A3AB9731176D93 ();
// 0x00000087 System.Void AttackEnemy::.ctor()
extern void AttackEnemy__ctor_m1CBB48D58ABDD3F95784C16FE7AF01F1F2B35569 ();
// 0x00000088 System.Void FlingPlayer::Start()
extern void FlingPlayer_Start_mAF399A6DF68D6D74B27573CBCB87231C94B6F0A9 ();
// 0x00000089 System.Void FlingPlayer::Update()
extern void FlingPlayer_Update_m78E6246C21FCED4BF4E9F112DB8A66D690B6D78E ();
// 0x0000008A System.Void FlingPlayer::OnMouseDrag()
extern void FlingPlayer_OnMouseDrag_m8459724A9AFEB1A9EF56145B6518CC9D35423AA4 ();
// 0x0000008B System.Void FlingPlayer::.ctor()
extern void FlingPlayer__ctor_m43F52FB38751F0E673D1310B53BBBEB1FD574B81 ();
// 0x0000008C System.Void FollowPlayer::Start()
extern void FollowPlayer_Start_m3D4944B71D74CF5E99F5032DC85E078F16A977C9 ();
// 0x0000008D System.Void FollowPlayer::Update()
extern void FollowPlayer_Update_m940D5A36DC60B8CF02F9DC5CE44AA288F3BEDB25 ();
// 0x0000008E UnityEngine.Vector3 FollowPlayer::GetPlayerPos()
extern void FollowPlayer_GetPlayerPos_mD5FAA6880228F0670BB9CEB47FC7D1A5D3B39361 ();
// 0x0000008F System.Void FollowPlayer::.ctor()
extern void FollowPlayer__ctor_m7A7391BDBACA4FB475491CC3B13D12271C5AE761 ();
// 0x00000090 System.Void Jump::Start()
extern void Jump_Start_m8C69302F327C33093CC36B4FC35DDA4B3A86BB72 ();
// 0x00000091 System.Void Jump::Update()
extern void Jump_Update_m512CDFCE74FAB60FDE33432DC017B27AEE0C9205 ();
// 0x00000092 System.Void Jump::JumpUp()
extern void Jump_JumpUp_m0FEA16D9A743C640C8221B80F65FB5A1E6DA856D ();
// 0x00000093 System.Void Jump::OnCollisionEnter2D(UnityEngine.Collision2D)
extern void Jump_OnCollisionEnter2D_mCD70688C0CEA4BF04E178F4B9205D94FB35D2814 ();
// 0x00000094 System.Void Jump::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void Jump_OnTriggerEnter2D_m6E7D9AD6871E0E6C76698F4C9130DD2410FC4A4A ();
// 0x00000095 System.Void Jump::ManageJump(System.Single)
extern void Jump_ManageJump_m8613688A808094B8B44E9C3C6F660911CA23DC9B ();
// 0x00000096 System.Void Jump::CheckSlide()
extern void Jump_CheckSlide_m70E042E8426B5A035E6944C8D8A473B838BA06F0 ();
// 0x00000097 System.Single Jump::GetForce()
extern void Jump_GetForce_mF3B40B57D33B3F6AE1B70622E9691D3C9C2514A6 ();
// 0x00000098 System.Void Jump::Flip()
extern void Jump_Flip_m90B92F4EFB7896CC876C65131663F860CDFF06A1 ();
// 0x00000099 System.Void Jump::.ctor()
extern void Jump__ctor_m5FF8975046C5683F92A333A924EC0C511D289C77 ();
// 0x0000009A System.Void ProjectileLine::Start()
extern void ProjectileLine_Start_m96297B46C10795F33927F76AD63EA4BF5B71BF39 ();
// 0x0000009B System.Void ProjectileLine::Update()
extern void ProjectileLine_Update_m77126BF61671D162B7E8D92DF70405D89144E58F ();
// 0x0000009C System.Void ProjectileLine::.ctor()
extern void ProjectileLine__ctor_mB9204E54F927B899530E9F9B7F56C7FC66495BD4 ();
// 0x0000009D System.Void SlowTeleport::Start()
extern void SlowTeleport_Start_m640E7C2655E6EE849FC328E65EAEAF396C725663 ();
// 0x0000009E System.Void SlowTeleport::Update()
extern void SlowTeleport_Update_m9325AE8AAB047E3AA5EA673639244DF70A93C30F ();
// 0x0000009F System.Collections.IEnumerator SlowTeleport::FlingPlayer()
extern void SlowTeleport_FlingPlayer_mFB6F7CEA86FF82D78C8C46BF43EC8FD53C8380F9 ();
// 0x000000A0 System.Void SlowTeleport::OnCollisionEnter2D(UnityEngine.Collision2D)
extern void SlowTeleport_OnCollisionEnter2D_mADE6D028C0023BCE3C63B911DE64935401F3832D ();
// 0x000000A1 System.Void SlowTeleport::ShootProjectile()
extern void SlowTeleport_ShootProjectile_m816BE24A2348E762C2D5A2010989A53E24D070FE ();
// 0x000000A2 System.Void SlowTeleport::.ctor()
extern void SlowTeleport__ctor_mAC64D4D38B0971E20D05BFCE333936A6D0BC1BBA ();
// 0x000000A3 System.Void Throw::Start()
extern void Throw_Start_m871695977E7220FC694F26515E7FC9CCF447D1BD ();
// 0x000000A4 System.Void Throw::Update()
extern void Throw_Update_m9A454E6D38540707F9151746158C8EB801F38433 ();
// 0x000000A5 UnityEngine.Vector3 Throw::GetRayCollisionTransform()
extern void Throw_GetRayCollisionTransform_m0C7C447E43910734615304E2A556B7EF31E3ABB7 ();
// 0x000000A6 System.Boolean Throw::GetIsRayCasting()
extern void Throw_GetIsRayCasting_m05A0D8DD4B39F0B4C568FB74B2E9FAD04ED6580B ();
// 0x000000A7 UnityEngine.Vector3 Throw::GetDirectionVector3(System.Single)
extern void Throw_GetDirectionVector3_mCFFE3C106C378E7AE924A94808EDB2C70C465E76 ();
// 0x000000A8 System.Collections.IEnumerator Throw::SpawnExplosion(UnityEngine.RaycastHit2D)
extern void Throw_SpawnExplosion_m2544543EBD637C9223DC6916DC79B5A0893557DC ();
// 0x000000A9 System.Void Throw::.ctor()
extern void Throw__ctor_mC3F9DD097E22416C9BF29185E8871A644B61AFB8 ();
// 0x000000AA System.Void JumpArc::Awake()
extern void JumpArc_Awake_m222E9C334472D1B386FC58A3E44FE2E55A2ACA3A ();
// 0x000000AB System.Void JumpArc::RenderArc(System.Single)
extern void JumpArc_RenderArc_m85A0B689C4C3D91AB492DF33C1670C51CEAC230A ();
// 0x000000AC UnityEngine.Vector3[] JumpArc::CalculateArcArray(System.Single)
extern void JumpArc_CalculateArcArray_m7B7695A66BE38ECADAD28E3CC59AE65F3C20CDC7 ();
// 0x000000AD UnityEngine.Vector3 JumpArc::CalculateArcPoint(System.Single,System.Single,System.Single)
extern void JumpArc_CalculateArcPoint_m1A992A1C234DCFDF1F27A761D5637BDCF195CBF4 ();
// 0x000000AE System.Void JumpArc::.ctor()
extern void JumpArc__ctor_mB9E53E3464B30BAAC3DA628C55B37AF5C3279D5F ();
// 0x000000AF System.Void SwipeDetector::Update()
extern void SwipeDetector_Update_m4C8655500371A9BCD7E24FCE10E0AA3C0F3360DE ();
// 0x000000B0 System.Void SwipeDetector::checkSwipe()
extern void SwipeDetector_checkSwipe_m0608B40F49318A12CC9430FC812E2D3A95806A61 ();
// 0x000000B1 System.Single SwipeDetector::verticalMove()
extern void SwipeDetector_verticalMove_m7A3D4EAC75D33896F5B95B5F2D7D558977F3FC9C ();
// 0x000000B2 System.Single SwipeDetector::horizontalValMove()
extern void SwipeDetector_horizontalValMove_m7B260E04DA09077B058D2059BEA265E21789F803 ();
// 0x000000B3 System.Void SwipeDetector::OnSwipeUp()
extern void SwipeDetector_OnSwipeUp_mBD165A4D1F5C2D78ACB2862A75F9258C5B6E1E88 ();
// 0x000000B4 System.Void SwipeDetector::OnSwipeDown()
extern void SwipeDetector_OnSwipeDown_mB91943243BD3BC492A1D260851097E3AE1E5A3B1 ();
// 0x000000B5 System.Void SwipeDetector::OnSwipeLeft()
extern void SwipeDetector_OnSwipeLeft_mB581B3F20B9CF39C39274743B3AE3009E0C257AE ();
// 0x000000B6 System.Void SwipeDetector::OnSwipeRight()
extern void SwipeDetector_OnSwipeRight_m83B6E2F3BAC614FCF8662CC8BC087091351BFE9C ();
// 0x000000B7 System.Void SwipeDetector::.ctor()
extern void SwipeDetector__ctor_m1D2AE83BFB7097D9C4177195E817D2F06B7BDE70 ();
// 0x000000B8 System.Void ChatController::OnEnable()
extern void ChatController_OnEnable_m168B1E78BFA288F42D4AE0A8F1424B8D68B07993 ();
// 0x000000B9 System.Void ChatController::OnDisable()
extern void ChatController_OnDisable_m49C4A6501BCC216F924B3C37F243D1B5B54A69FF ();
// 0x000000BA System.Void ChatController::AddToChatOutput(System.String)
extern void ChatController_AddToChatOutput_m5E6DF0E37CB2E9FBBEACCB6EEE6452AB14BBE94C ();
// 0x000000BB System.Void ChatController::.ctor()
extern void ChatController__ctor_m2C7AAB67386BA2DC6742585988B914B3FAB30013 ();
// 0x000000BC System.Void EnvMapAnimator::Awake()
extern void EnvMapAnimator_Awake_mDDD10A405C7152BEFA0ECEA0DCBD061B47C5802E ();
// 0x000000BD System.Collections.IEnumerator EnvMapAnimator::Start()
extern void EnvMapAnimator_Start_m630E0BFAB4D647BC38B99A70F522EF80D25F3C71 ();
// 0x000000BE System.Void EnvMapAnimator::.ctor()
extern void EnvMapAnimator__ctor_m2A8770DA2E27EC52F6A6F704831B732638C76E84 ();
// 0x000000BF System.Char TMPro.TMP_DigitValidator::Validate(System.String&,System.Int32&,System.Char)
extern void TMP_DigitValidator_Validate_mEC7653F2228D8AA66F69D6B3539ED342AEE57691 ();
// 0x000000C0 System.Void TMPro.TMP_DigitValidator::.ctor()
extern void TMP_DigitValidator__ctor_m4E1C1BEB96F76F2EE55E6FEC45D05F2AAC5DF325 ();
// 0x000000C1 System.Char TMPro.TMP_PhoneNumberValidator::Validate(System.String&,System.Int32&,System.Char)
extern void TMP_PhoneNumberValidator_Validate_mBE0169BE01459AA37111A289EC422DDB0D5E3479 ();
// 0x000000C2 System.Void TMPro.TMP_PhoneNumberValidator::.ctor()
extern void TMP_PhoneNumberValidator__ctor_mBF81DE006E19E49DAC3AFF685F8AF268A2FD0FFB ();
// 0x000000C3 TMPro.TMP_TextEventHandler_CharacterSelectionEvent TMPro.TMP_TextEventHandler::get_onCharacterSelection()
extern void TMP_TextEventHandler_get_onCharacterSelection_mF70DBE3FF43B3D6E64053D37A2FADF802533E1FF ();
// 0x000000C4 System.Void TMPro.TMP_TextEventHandler::set_onCharacterSelection(TMPro.TMP_TextEventHandler_CharacterSelectionEvent)
extern void TMP_TextEventHandler_set_onCharacterSelection_mDEC285B6A284CC2EC9729E3DC16E81A182890D21 ();
// 0x000000C5 TMPro.TMP_TextEventHandler_SpriteSelectionEvent TMPro.TMP_TextEventHandler::get_onSpriteSelection()
extern void TMP_TextEventHandler_get_onSpriteSelection_m395603314F8CD073897DCAB5513270C6ADD94BF4 ();
// 0x000000C6 System.Void TMPro.TMP_TextEventHandler::set_onSpriteSelection(TMPro.TMP_TextEventHandler_SpriteSelectionEvent)
extern void TMP_TextEventHandler_set_onSpriteSelection_m3D4E17778B0E3CC987A3EF74515E83CE39E3C094 ();
// 0x000000C7 TMPro.TMP_TextEventHandler_WordSelectionEvent TMPro.TMP_TextEventHandler::get_onWordSelection()
extern void TMP_TextEventHandler_get_onWordSelection_m415F4479934B1739658356B47DF4C2E90496AE2E ();
// 0x000000C8 System.Void TMPro.TMP_TextEventHandler::set_onWordSelection(TMPro.TMP_TextEventHandler_WordSelectionEvent)
extern void TMP_TextEventHandler_set_onWordSelection_m2EDD56E0024792DCE7F068228B4CA5A897808F4E ();
// 0x000000C9 TMPro.TMP_TextEventHandler_LineSelectionEvent TMPro.TMP_TextEventHandler::get_onLineSelection()
extern void TMP_TextEventHandler_get_onLineSelection_m8E724700CC5DF1197B103F87156576A52F62AB2B ();
// 0x000000CA System.Void TMPro.TMP_TextEventHandler::set_onLineSelection(TMPro.TMP_TextEventHandler_LineSelectionEvent)
extern void TMP_TextEventHandler_set_onLineSelection_m067512B3F057A225AF6DD251DD7E546FFF64CD93 ();
// 0x000000CB TMPro.TMP_TextEventHandler_LinkSelectionEvent TMPro.TMP_TextEventHandler::get_onLinkSelection()
extern void TMP_TextEventHandler_get_onLinkSelection_m221527467F0606DD3561E0FB0D7678AA8329AD5D ();
// 0x000000CC System.Void TMPro.TMP_TextEventHandler::set_onLinkSelection(TMPro.TMP_TextEventHandler_LinkSelectionEvent)
extern void TMP_TextEventHandler_set_onLinkSelection_mE3CE372F9FECD727FAB3B14D46439E0534EE8AA8 ();
// 0x000000CD System.Void TMPro.TMP_TextEventHandler::Awake()
extern void TMP_TextEventHandler_Awake_m67A37475531AC3EB75B43A640058AD52A605B8D9 ();
// 0x000000CE System.Void TMPro.TMP_TextEventHandler::LateUpdate()
extern void TMP_TextEventHandler_LateUpdate_mB0ABBED08D5494DFFF85D9B56D4446D96DDBDDF5 ();
// 0x000000CF System.Void TMPro.TMP_TextEventHandler::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextEventHandler_OnPointerEnter_mE1CAF8C68C2356069FEB1AA1B53A56E24E5CE333 ();
// 0x000000D0 System.Void TMPro.TMP_TextEventHandler::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextEventHandler_OnPointerExit_mB429546A32DCF6C8C64E703D07F9F1CDC697B009 ();
// 0x000000D1 System.Void TMPro.TMP_TextEventHandler::SendOnCharacterSelection(System.Char,System.Int32)
extern void TMP_TextEventHandler_SendOnCharacterSelection_mFBFC60A83107F26AA351246C10AB42CEB3A5A13C ();
// 0x000000D2 System.Void TMPro.TMP_TextEventHandler::SendOnSpriteSelection(System.Char,System.Int32)
extern void TMP_TextEventHandler_SendOnSpriteSelection_mAB964EB5171AB07C48AC64E06C6BEC6A9C323E09 ();
// 0x000000D3 System.Void TMPro.TMP_TextEventHandler::SendOnWordSelection(System.String,System.Int32,System.Int32)
extern void TMP_TextEventHandler_SendOnWordSelection_m3B76D7E79C65DB9D8E09EE834252C6E33C86D3AE ();
// 0x000000D4 System.Void TMPro.TMP_TextEventHandler::SendOnLineSelection(System.String,System.Int32,System.Int32)
extern void TMP_TextEventHandler_SendOnLineSelection_m9E9CAD5FA36FCA342A38EBD43E609A469E49F15F ();
// 0x000000D5 System.Void TMPro.TMP_TextEventHandler::SendOnLinkSelection(System.String,System.String,System.Int32)
extern void TMP_TextEventHandler_SendOnLinkSelection_m1C55C664BB488E25AE746B99438EEDAE5B2B8DE8 ();
// 0x000000D6 System.Void TMPro.TMP_TextEventHandler::.ctor()
extern void TMP_TextEventHandler__ctor_m189A5951F5C0FA5FB1D0CFC461FAA1EBD7AED1AE ();
// 0x000000D7 System.Collections.IEnumerator TMPro.Examples.Benchmark01::Start()
extern void Benchmark01_Start_m20668FA5AD3945F18B5045459057C330E0B4D1F4 ();
// 0x000000D8 System.Void TMPro.Examples.Benchmark01::.ctor()
extern void Benchmark01__ctor_m40EDCD3A3B6E8651A39C2220669A7689902C8B36 ();
// 0x000000D9 System.Collections.IEnumerator TMPro.Examples.Benchmark01_UGUI::Start()
extern void Benchmark01_UGUI_Start_mE8F5BC98EC6C16ECEBAD0FD78CD63E278B2DF215 ();
// 0x000000DA System.Void TMPro.Examples.Benchmark01_UGUI::.ctor()
extern void Benchmark01_UGUI__ctor_m7F24B3D019827130B3D5F2D3E8C3FF23425F98BE ();
// 0x000000DB System.Void TMPro.Examples.Benchmark02::Start()
extern void Benchmark02_Start_m3F848191079D3EF1E3B785830D74698325CA0BB7 ();
// 0x000000DC System.Void TMPro.Examples.Benchmark02::.ctor()
extern void Benchmark02__ctor_m3323414B806F63563E680918CC90EAF766A3D1AE ();
// 0x000000DD System.Void TMPro.Examples.Benchmark03::Awake()
extern void Benchmark03_Awake_m261B7F2CD25DC9E7144B2A2D167219A751AD9322 ();
// 0x000000DE System.Void TMPro.Examples.Benchmark03::Start()
extern void Benchmark03_Start_m649EFCC5BF0F199D102083583854DE87AC5EFBDD ();
// 0x000000DF System.Void TMPro.Examples.Benchmark03::.ctor()
extern void Benchmark03__ctor_m90649FDE30CC915363C5B61AA19A7DE874FF18ED ();
// 0x000000E0 System.Void TMPro.Examples.Benchmark04::Start()
extern void Benchmark04_Start_mFDF88CB6DD4C5641A418DB08E105F9F62B897777 ();
// 0x000000E1 System.Void TMPro.Examples.Benchmark04::.ctor()
extern void Benchmark04__ctor_mB07A2FD29BE4AFE284B47F2F610BDB7539F5A5DE ();
// 0x000000E2 System.Void TMPro.Examples.CameraController::Awake()
extern void CameraController_Awake_m5E24687E6D82C0EBC4984D01B90769B8FD8C38B3 ();
// 0x000000E3 System.Void TMPro.Examples.CameraController::Start()
extern void CameraController_Start_m257B81C6062A725785739AFE4C0DF84B8931EFB2 ();
// 0x000000E4 System.Void TMPro.Examples.CameraController::LateUpdate()
extern void CameraController_LateUpdate_m9660F57BCF4F8C2154D19B6B40208466E414DAEB ();
// 0x000000E5 System.Void TMPro.Examples.CameraController::GetPlayerInput()
extern void CameraController_GetPlayerInput_m0B63EA708A63AF6852E099FD40F7C4E18793560A ();
// 0x000000E6 System.Void TMPro.Examples.CameraController::.ctor()
extern void CameraController__ctor_m8379776EEE21556D56845974B8C505AAD366B656 ();
// 0x000000E7 System.Void TMPro.Examples.ObjectSpin::Awake()
extern void ObjectSpin_Awake_m2E5B2D7FA6FE2F3B5516BD829EDC5522187E6359 ();
// 0x000000E8 System.Void TMPro.Examples.ObjectSpin::Update()
extern void ObjectSpin_Update_mF8175B9157B852D3EC1BAF19D168858A8782BF0D ();
// 0x000000E9 System.Void TMPro.Examples.ObjectSpin::.ctor()
extern void ObjectSpin__ctor_m1F951082C07A983F89779737E5A6071DD7BA67EB ();
// 0x000000EA System.Void TMPro.Examples.ShaderPropAnimator::Awake()
extern void ShaderPropAnimator_Awake_m44ACA60771EECABCB189FC78027D4ECD9726D31A ();
// 0x000000EB System.Void TMPro.Examples.ShaderPropAnimator::Start()
extern void ShaderPropAnimator_Start_m57178B42FF0BB90ACA497EC1AA942CC3D4D54C32 ();
// 0x000000EC System.Collections.IEnumerator TMPro.Examples.ShaderPropAnimator::AnimateProperties()
extern void ShaderPropAnimator_AnimateProperties_mB34C25C714FAEA4792465A981BAE46778C4F2409 ();
// 0x000000ED System.Void TMPro.Examples.ShaderPropAnimator::.ctor()
extern void ShaderPropAnimator__ctor_mDFAE260FD15CD3E704E86A25A57880A33B817BC6 ();
// 0x000000EE System.Void TMPro.Examples.SimpleScript::Start()
extern void SimpleScript_Start_m0238BE0F5DF0A15743D4D4B1B64C0A86505D1B76 ();
// 0x000000EF System.Void TMPro.Examples.SimpleScript::Update()
extern void SimpleScript_Update_mB92D578CAC3E0A0AFB055C7FEF47601C8822A0F8 ();
// 0x000000F0 System.Void TMPro.Examples.SimpleScript::.ctor()
extern void SimpleScript__ctor_m0E919E8F3C12BAFF36B17E5692FCFA5AE602B2AA ();
// 0x000000F1 System.Void TMPro.Examples.SkewTextExample::Awake()
extern void SkewTextExample_Awake_mC70E117C1F921453D2F448CABA234FAA17A277ED ();
// 0x000000F2 System.Void TMPro.Examples.SkewTextExample::Start()
extern void SkewTextExample_Start_mE2308836BF90B959ABE6064CD2DDDFAF224F0F4A ();
// 0x000000F3 UnityEngine.AnimationCurve TMPro.Examples.SkewTextExample::CopyAnimationCurve(UnityEngine.AnimationCurve)
extern void SkewTextExample_CopyAnimationCurve_m3CE7B666BEF4CFFE9EB110C8D57D9A5F6385720B ();
// 0x000000F4 System.Collections.IEnumerator TMPro.Examples.SkewTextExample::WarpText()
extern void SkewTextExample_WarpText_m8B756AF1E1C065EEA486159E6C631A585B0C3461 ();
// 0x000000F5 System.Void TMPro.Examples.SkewTextExample::.ctor()
extern void SkewTextExample__ctor_m44F3CBD12A19C44A000D705FB4AB02E20432EC02 ();
// 0x000000F6 System.Void TMPro.Examples.TMP_ExampleScript_01::Awake()
extern void TMP_ExampleScript_01_Awake_mE2AAB8DF142D7BDB2C041CC7552A48745DBFDCFF ();
// 0x000000F7 System.Void TMPro.Examples.TMP_ExampleScript_01::Update()
extern void TMP_ExampleScript_01_Update_m1593A7650860FD2A478E10EA12A2601E918DD1EC ();
// 0x000000F8 System.Void TMPro.Examples.TMP_ExampleScript_01::.ctor()
extern void TMP_ExampleScript_01__ctor_m313B4F7ED747AD6979D8909858D0EF182C79BBC3 ();
// 0x000000F9 System.Void TMPro.Examples.TMP_FrameRateCounter::Awake()
extern void TMP_FrameRateCounter_Awake_m2540DCD733523BCBB1757724D8546AC3F1BEB16C ();
// 0x000000FA System.Void TMPro.Examples.TMP_FrameRateCounter::Start()
extern void TMP_FrameRateCounter_Start_mEF10D80C419582C6944313FD100E2FD1C5AD1319 ();
// 0x000000FB System.Void TMPro.Examples.TMP_FrameRateCounter::Update()
extern void TMP_FrameRateCounter_Update_mF4798814F4F86850BB9248CA192EF5B65FA3A92B ();
// 0x000000FC System.Void TMPro.Examples.TMP_FrameRateCounter::Set_FrameCounter_Position(TMPro.Examples.TMP_FrameRateCounter_FpsCounterAnchorPositions)
extern void TMP_FrameRateCounter_Set_FrameCounter_Position_m19C3C5E637FB3ED2B0869E7650A1C30A3302AF53 ();
// 0x000000FD System.Void TMPro.Examples.TMP_FrameRateCounter::.ctor()
extern void TMP_FrameRateCounter__ctor_m55E3726473BA4825AC0B7B7B7EA48D0C5CE8D646 ();
// 0x000000FE System.Void TMPro.Examples.TMP_TextEventCheck::OnEnable()
extern void TMP_TextEventCheck_OnEnable_mAFF9E7581B7B0C93A4A7D811C978FFCEC87B3784 ();
// 0x000000FF System.Void TMPro.Examples.TMP_TextEventCheck::OnDisable()
extern void TMP_TextEventCheck_OnDisable_m270DBB9CC93731104E851797D6BF55EACAE9158A ();
// 0x00000100 System.Void TMPro.Examples.TMP_TextEventCheck::OnCharacterSelection(System.Char,System.Int32)
extern void TMP_TextEventCheck_OnCharacterSelection_m4394BE3A0CA37D319AA10BE200A26CFD17EEAA8F ();
// 0x00000101 System.Void TMPro.Examples.TMP_TextEventCheck::OnSpriteSelection(System.Char,System.Int32)
extern void TMP_TextEventCheck_OnSpriteSelection_mCBF0B6754C607CA140C405FF5B681154AC861992 ();
// 0x00000102 System.Void TMPro.Examples.TMP_TextEventCheck::OnWordSelection(System.String,System.Int32,System.Int32)
extern void TMP_TextEventCheck_OnWordSelection_m4C290E23BBA708FE259A5F53921B7B98480E5B08 ();
// 0x00000103 System.Void TMPro.Examples.TMP_TextEventCheck::OnLineSelection(System.String,System.Int32,System.Int32)
extern void TMP_TextEventCheck_OnLineSelection_mF68BE3244AFD53E84E037B39443B5B3B50336FF5 ();
// 0x00000104 System.Void TMPro.Examples.TMP_TextEventCheck::OnLinkSelection(System.String,System.String,System.Int32)
extern void TMP_TextEventCheck_OnLinkSelection_m23569DD32B2D3C4599B8D855AE89178C92BA25C7 ();
// 0x00000105 System.Void TMPro.Examples.TMP_TextEventCheck::.ctor()
extern void TMP_TextEventCheck__ctor_m4B49D7387750432FA7A15A804ABD6793422E0632 ();
// 0x00000106 System.Void TMPro.Examples.TMP_TextInfoDebugTool::.ctor()
extern void TMP_TextInfoDebugTool__ctor_m2A2D1B42F97BD424B7C61813B83FE46C91575EFB ();
// 0x00000107 System.Void TMPro.Examples.TMP_TextSelector_A::Awake()
extern void TMP_TextSelector_A_Awake_mEE6FCD85F7A6FDA4CC3B51173865E53F010AB0FF ();
// 0x00000108 System.Void TMPro.Examples.TMP_TextSelector_A::LateUpdate()
extern void TMP_TextSelector_A_LateUpdate_mF02F95A5D14806665404997F9ABAEE288A9879A0 ();
// 0x00000109 System.Void TMPro.Examples.TMP_TextSelector_A::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_A_OnPointerEnter_m6D15B2FC399C52D9706DD85C796BAE40CA8362D3 ();
// 0x0000010A System.Void TMPro.Examples.TMP_TextSelector_A::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_A_OnPointerExit_m080D05700B1D3251085331369FCD2A131D45F963 ();
// 0x0000010B System.Void TMPro.Examples.TMP_TextSelector_A::.ctor()
extern void TMP_TextSelector_A__ctor_m6AB8BC86973365C192CF9EACA61459F2E0A5C88D ();
// 0x0000010C System.Void TMPro.Examples.TMP_TextSelector_B::Awake()
extern void TMP_TextSelector_B_Awake_m87D2FCFCEDEE1FA82DEF77A867D2DE56C3AA0973 ();
// 0x0000010D System.Void TMPro.Examples.TMP_TextSelector_B::OnEnable()
extern void TMP_TextSelector_B_OnEnable_mD1C87684FD94190654176B38EE7DC960795F08E8 ();
// 0x0000010E System.Void TMPro.Examples.TMP_TextSelector_B::OnDisable()
extern void TMP_TextSelector_B_OnDisable_m429F83E18507E278CA9E9B5A2AE891087ED0D830 ();
// 0x0000010F System.Void TMPro.Examples.TMP_TextSelector_B::ON_TEXT_CHANGED(UnityEngine.Object)
extern void TMP_TextSelector_B_ON_TEXT_CHANGED_m91D0E180681C5566066C366487B94A05FB376B12 ();
// 0x00000110 System.Void TMPro.Examples.TMP_TextSelector_B::LateUpdate()
extern void TMP_TextSelector_B_LateUpdate_m80F8343FAB19617468E94CD2B35636DBB9AC2064 ();
// 0x00000111 System.Void TMPro.Examples.TMP_TextSelector_B::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_B_OnPointerEnter_m9A938ED5B0D70633B9099F5C1B213FD50380116D ();
// 0x00000112 System.Void TMPro.Examples.TMP_TextSelector_B::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_B_OnPointerExit_mD481099225DF156CA7CA904AA1C81AF26A974D28 ();
// 0x00000113 System.Void TMPro.Examples.TMP_TextSelector_B::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_B_OnPointerClick_mE4A6507E55DD05BBC99F81212CF26F2F11179FBE ();
// 0x00000114 System.Void TMPro.Examples.TMP_TextSelector_B::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_B_OnPointerUp_m5E52652A02A561F2E8AB7F0C00E280C76A090F74 ();
// 0x00000115 System.Void TMPro.Examples.TMP_TextSelector_B::RestoreCachedVertexAttributes(System.Int32)
extern void TMP_TextSelector_B_RestoreCachedVertexAttributes_m01B9A1E989D57BE8837E99C4359BCB6DD847CB35 ();
// 0x00000116 System.Void TMPro.Examples.TMP_TextSelector_B::.ctor()
extern void TMP_TextSelector_B__ctor_mC42D87810C72234A3360C0965CC1B7F45AB4EE26 ();
// 0x00000117 System.Void TMPro.Examples.TMP_UiFrameRateCounter::Awake()
extern void TMP_UiFrameRateCounter_Awake_mFAF9F495C66394DC36E9C6BC96C9E880C4A3B0A9 ();
// 0x00000118 System.Void TMPro.Examples.TMP_UiFrameRateCounter::Start()
extern void TMP_UiFrameRateCounter_Start_mC4A3331333B1DFA82B184A0701FCE26395B8D301 ();
// 0x00000119 System.Void TMPro.Examples.TMP_UiFrameRateCounter::Update()
extern void TMP_UiFrameRateCounter_Update_mCA98BB5342C50F9CE247A858E1942410537E0DAF ();
// 0x0000011A System.Void TMPro.Examples.TMP_UiFrameRateCounter::Set_FrameCounter_Position(TMPro.Examples.TMP_UiFrameRateCounter_FpsCounterAnchorPositions)
extern void TMP_UiFrameRateCounter_Set_FrameCounter_Position_mDD0EAB08CE58340555A6654BDD5BEE015E6C6ACE ();
// 0x0000011B System.Void TMPro.Examples.TMP_UiFrameRateCounter::.ctor()
extern void TMP_UiFrameRateCounter__ctor_mE3DC8B24D2819C55B66AEAEB9C9B93AFDA9C4573 ();
// 0x0000011C System.Void TMPro.Examples.TMPro_InstructionOverlay::Awake()
extern void TMPro_InstructionOverlay_Awake_m951573D9BF0200A4C4605E043E92BBD2EB33BA7C ();
// 0x0000011D System.Void TMPro.Examples.TMPro_InstructionOverlay::Set_FrameCounter_Position(TMPro.Examples.TMPro_InstructionOverlay_FpsCounterAnchorPositions)
extern void TMPro_InstructionOverlay_Set_FrameCounter_Position_m39D0BB71DCCB67271B96F8A9082D7638E4E1A694 ();
// 0x0000011E System.Void TMPro.Examples.TMPro_InstructionOverlay::.ctor()
extern void TMPro_InstructionOverlay__ctor_m103EF0B8818B248077CB97909BA806477DCEB8A5 ();
// 0x0000011F System.Void TMPro.Examples.TeleType::Awake()
extern void TeleType_Awake_m3501F8FA1B762D22972B9B2BAC1E20561088882B ();
// 0x00000120 System.Collections.IEnumerator TMPro.Examples.TeleType::Start()
extern void TeleType_Start_m2A3F19E0F9F2C72D48DDF5A4208AF18AE7769E69 ();
// 0x00000121 System.Void TMPro.Examples.TeleType::.ctor()
extern void TeleType__ctor_m8B985E4023A01F963A74E0FE5E8758B979FB3C3A ();
// 0x00000122 System.Void TMPro.Examples.TextConsoleSimulator::Awake()
extern void TextConsoleSimulator_Awake_m8B1E7254BFB2D0C7D5A803AEFAFCD1B5327F79AD ();
// 0x00000123 System.Void TMPro.Examples.TextConsoleSimulator::Start()
extern void TextConsoleSimulator_Start_m85E6334AFE22350A5715F9E45843FD865EF60C9D ();
// 0x00000124 System.Void TMPro.Examples.TextConsoleSimulator::OnEnable()
extern void TextConsoleSimulator_OnEnable_mB6F523D582FE4789A5B95C086AA7C168A5DD5AF7 ();
// 0x00000125 System.Void TMPro.Examples.TextConsoleSimulator::OnDisable()
extern void TextConsoleSimulator_OnDisable_m1EF25B5345586DD26BB8615624358EFB21B485DB ();
// 0x00000126 System.Void TMPro.Examples.TextConsoleSimulator::ON_TEXT_CHANGED(UnityEngine.Object)
extern void TextConsoleSimulator_ON_TEXT_CHANGED_mD4A85AE6FE4CD3AFF790859DEFB7E4AAF9304AE5 ();
// 0x00000127 System.Collections.IEnumerator TMPro.Examples.TextConsoleSimulator::RevealCharacters(TMPro.TMP_Text)
extern void TextConsoleSimulator_RevealCharacters_m7BF445A3B7B6A259450593775D10DE0D4BD901AD ();
// 0x00000128 System.Collections.IEnumerator TMPro.Examples.TextConsoleSimulator::RevealWords(TMPro.TMP_Text)
extern void TextConsoleSimulator_RevealWords_mD7D62A1D326528506154148148166B9196A9B903 ();
// 0x00000129 System.Void TMPro.Examples.TextConsoleSimulator::.ctor()
extern void TextConsoleSimulator__ctor_mA40DB76E1D63318E646CF2AE921084D0FDF4C3CA ();
// 0x0000012A System.Void TMPro.Examples.TextMeshProFloatingText::Awake()
extern void TextMeshProFloatingText_Awake_mB40A823A322B9EFD776230600A131BAE996580C3 ();
// 0x0000012B System.Void TMPro.Examples.TextMeshProFloatingText::Start()
extern void TextMeshProFloatingText_Start_mBFC04A0247294E62BD58CB3AC83F85AE61C3FB4F ();
// 0x0000012C System.Collections.IEnumerator TMPro.Examples.TextMeshProFloatingText::DisplayTextMeshProFloatingText()
extern void TextMeshProFloatingText_DisplayTextMeshProFloatingText_mB0DEABA5CC4A6B556D76ED30A3CF08E7F0B42AFC ();
// 0x0000012D System.Collections.IEnumerator TMPro.Examples.TextMeshProFloatingText::DisplayTextMeshFloatingText()
extern void TextMeshProFloatingText_DisplayTextMeshFloatingText_m8AB7E0B8313124F67FEDE857012B9E56397147E2 ();
// 0x0000012E System.Void TMPro.Examples.TextMeshProFloatingText::.ctor()
extern void TextMeshProFloatingText__ctor_m610430DD6E4FD84EBF6C499FB4415B5000109627 ();
// 0x0000012F System.Void TMPro.Examples.TextMeshSpawner::Awake()
extern void TextMeshSpawner_Awake_m31920E8DD53AD295AAD8B259391A28E1A57862ED ();
// 0x00000130 System.Void TMPro.Examples.TextMeshSpawner::Start()
extern void TextMeshSpawner_Start_m189316ED7CD62EFD10B40A23E4072C2CEB5A516B ();
// 0x00000131 System.Void TMPro.Examples.TextMeshSpawner::.ctor()
extern void TextMeshSpawner__ctor_m3995DDE2D7E7CBF8087A3B61242F35E09AC94668 ();
// 0x00000132 System.Void TMPro.Examples.VertexColorCycler::Awake()
extern void VertexColorCycler_Awake_m19D37F0DDC4E1D64EA67101852383862DCAAED1E ();
// 0x00000133 System.Void TMPro.Examples.VertexColorCycler::Start()
extern void VertexColorCycler_Start_m2CFBFF7F45A76D16C29B570E3468AFEEC2D1C443 ();
// 0x00000134 System.Collections.IEnumerator TMPro.Examples.VertexColorCycler::AnimateVertexColors()
extern void VertexColorCycler_AnimateVertexColors_mDB7F380B912148C792F857E42BFB042C6A267260 ();
// 0x00000135 System.Void TMPro.Examples.VertexColorCycler::.ctor()
extern void VertexColorCycler__ctor_mBAF42937750A7A22DB5BF09823489FDE25375816 ();
// 0x00000136 System.Void TMPro.Examples.VertexJitter::Awake()
extern void VertexJitter_Awake_m32ACAC43EDE2595CD4FFB6802D58DEBC0F65B52C ();
// 0x00000137 System.Void TMPro.Examples.VertexJitter::OnEnable()
extern void VertexJitter_OnEnable_m63CC97434F60690EE234794C9C2AD3B25EC69486 ();
// 0x00000138 System.Void TMPro.Examples.VertexJitter::OnDisable()
extern void VertexJitter_OnDisable_mE5E221B893D3E53F3A9516082E2C4A9BE174DDF5 ();
// 0x00000139 System.Void TMPro.Examples.VertexJitter::Start()
extern void VertexJitter_Start_mC977D71742279824F9DD719DD1F5CB10269BC531 ();
// 0x0000013A System.Void TMPro.Examples.VertexJitter::ON_TEXT_CHANGED(UnityEngine.Object)
extern void VertexJitter_ON_TEXT_CHANGED_mE5AE5146D67DA15512283617C41F194AEDD6A4AC ();
// 0x0000013B System.Collections.IEnumerator TMPro.Examples.VertexJitter::AnimateVertexColors()
extern void VertexJitter_AnimateVertexColors_m6B361C3B93A2CC219B98AACFC59288432EE6AC1E ();
// 0x0000013C System.Void TMPro.Examples.VertexJitter::.ctor()
extern void VertexJitter__ctor_mD5B5049BB3640662DD69EB1E14789891E8B2E720 ();
// 0x0000013D System.Void TMPro.Examples.VertexShakeA::Awake()
extern void VertexShakeA_Awake_m6075DA429A021C8CB3F6BE9A8B9C64127288CD19 ();
// 0x0000013E System.Void TMPro.Examples.VertexShakeA::OnEnable()
extern void VertexShakeA_OnEnable_m39AA373478F796E7C66763AA163D35811721F5CD ();
// 0x0000013F System.Void TMPro.Examples.VertexShakeA::OnDisable()
extern void VertexShakeA_OnDisable_mA087E96D94CB8213D28D9A601BC25ED784BB8421 ();
// 0x00000140 System.Void TMPro.Examples.VertexShakeA::Start()
extern void VertexShakeA_Start_mDED2AEA47D2E2EF346DE85112420F6E95D9A3CFD ();
// 0x00000141 System.Void TMPro.Examples.VertexShakeA::ON_TEXT_CHANGED(UnityEngine.Object)
extern void VertexShakeA_ON_TEXT_CHANGED_m0B59A798E6B193FE68F6A20E7004B223D5A2993E ();
// 0x00000142 System.Collections.IEnumerator TMPro.Examples.VertexShakeA::AnimateVertexColors()
extern void VertexShakeA_AnimateVertexColors_m238AB73BE06E33312281577CC896CEB7BB175245 ();
// 0x00000143 System.Void TMPro.Examples.VertexShakeA::.ctor()
extern void VertexShakeA__ctor_m41CBBA607D90D45E21C98CCDF347AE27FB50392F ();
// 0x00000144 System.Void TMPro.Examples.VertexShakeB::Awake()
extern void VertexShakeB_Awake_m7CBA45BF5135680A823536A18325ECA621EF7A1A ();
// 0x00000145 System.Void TMPro.Examples.VertexShakeB::OnEnable()
extern void VertexShakeB_OnEnable_m39EBB983A4FFFF6DD1C7923C8C23FF09CFF2F6E2 ();
// 0x00000146 System.Void TMPro.Examples.VertexShakeB::OnDisable()
extern void VertexShakeB_OnDisable_m8E3858FC1C976F311628466C411675E352F134A5 ();
// 0x00000147 System.Void TMPro.Examples.VertexShakeB::Start()
extern void VertexShakeB_Start_m666FA35D389B109F01A5FC229D32664D880ADE09 ();
// 0x00000148 System.Void TMPro.Examples.VertexShakeB::ON_TEXT_CHANGED(UnityEngine.Object)
extern void VertexShakeB_ON_TEXT_CHANGED_mF4858E4385EAA74F5A3008C50B8AD180FCBC8517 ();
// 0x00000149 System.Collections.IEnumerator TMPro.Examples.VertexShakeB::AnimateVertexColors()
extern void VertexShakeB_AnimateVertexColors_m076A6C9D71EE8B5A54CD1CEDCA5AB15160112DD3 ();
// 0x0000014A System.Void TMPro.Examples.VertexShakeB::.ctor()
extern void VertexShakeB__ctor_m5CAAD9DFA7B4D9C561473D53CA9E3D8B78AE5606 ();
// 0x0000014B System.Void TMPro.Examples.VertexZoom::Awake()
extern void VertexZoom_Awake_mB18FF89A84E2AA75BDD486A698955A58E47686EE ();
// 0x0000014C System.Void TMPro.Examples.VertexZoom::OnEnable()
extern void VertexZoom_OnEnable_mCD27B81253963B3D0CD2F6BA7B161F0DFDC08114 ();
// 0x0000014D System.Void TMPro.Examples.VertexZoom::OnDisable()
extern void VertexZoom_OnDisable_mB32AD5B7DFF20E682BA4FC82B30C87707DD3BA10 ();
// 0x0000014E System.Void TMPro.Examples.VertexZoom::Start()
extern void VertexZoom_Start_m6C64C692D81F64FB7F3244C3F0E37799B159A0DE ();
// 0x0000014F System.Void TMPro.Examples.VertexZoom::ON_TEXT_CHANGED(UnityEngine.Object)
extern void VertexZoom_ON_TEXT_CHANGED_mC08504F9622CC709271C09EDB7A0DF1A35E45768 ();
// 0x00000150 System.Collections.IEnumerator TMPro.Examples.VertexZoom::AnimateVertexColors()
extern void VertexZoom_AnimateVertexColors_mEC9842E0BC31D9D4E66FD30E6467D5A9A19034D6 ();
// 0x00000151 System.Void TMPro.Examples.VertexZoom::.ctor()
extern void VertexZoom__ctor_mA4381FC291E17D67EA3C2292EAB8D3C959ADEA79 ();
// 0x00000152 System.Void TMPro.Examples.WarpTextExample::Awake()
extern void WarpTextExample_Awake_mF6785C4DC8316E573F20A8356393946F6ABFC88C ();
// 0x00000153 System.Void TMPro.Examples.WarpTextExample::Start()
extern void WarpTextExample_Start_m8E7AC9FF62E37EAB89F93FD0C1457555F6DCB086 ();
// 0x00000154 UnityEngine.AnimationCurve TMPro.Examples.WarpTextExample::CopyAnimationCurve(UnityEngine.AnimationCurve)
extern void WarpTextExample_CopyAnimationCurve_m2C738EA265E2B35868110EE1D8FCBD4F1D61C038 ();
// 0x00000155 System.Collections.IEnumerator TMPro.Examples.WarpTextExample::WarpText()
extern void WarpTextExample_WarpText_m27664A46276B3D615ECB12315F5E77C4F2AF29EE ();
// 0x00000156 System.Void TMPro.Examples.WarpTextExample::.ctor()
extern void WarpTextExample__ctor_mF5BA8D140958AD2B5D2C8C5DE937E21A5D283C9F ();
// 0x00000157 System.Void AimAndAttack_<WaitTime>d__13::.ctor(System.Int32)
extern void U3CWaitTimeU3Ed__13__ctor_m6EFDE64DF68EFB7586206C7635EB720CB01D2D92 ();
// 0x00000158 System.Void AimAndAttack_<WaitTime>d__13::System.IDisposable.Dispose()
extern void U3CWaitTimeU3Ed__13_System_IDisposable_Dispose_m4DBC5FE487C79A6BBF58FB9044A452A98600F021 ();
// 0x00000159 System.Boolean AimAndAttack_<WaitTime>d__13::MoveNext()
extern void U3CWaitTimeU3Ed__13_MoveNext_m5A2CDBB6E28874A27AC03DDF103532FC60FB2ADB ();
// 0x0000015A System.Object AimAndAttack_<WaitTime>d__13::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitTimeU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m495CC8A67DA245523E36EB3D988A2BF34215BF8B ();
// 0x0000015B System.Void AimAndAttack_<WaitTime>d__13::System.Collections.IEnumerator.Reset()
extern void U3CWaitTimeU3Ed__13_System_Collections_IEnumerator_Reset_m065691C9787C63C3F148E63048B889D8B6B35B81 ();
// 0x0000015C System.Object AimAndAttack_<WaitTime>d__13::System.Collections.IEnumerator.get_Current()
extern void U3CWaitTimeU3Ed__13_System_Collections_IEnumerator_get_Current_mD135528D1A68F768E4BAA645C9888F32BC127614 ();
// 0x0000015D System.Void AimAndAttack_<Attack>d__14::.ctor(System.Int32)
extern void U3CAttackU3Ed__14__ctor_m597078FFC38775D2D521D33A26B38DD3476D7C7E ();
// 0x0000015E System.Void AimAndAttack_<Attack>d__14::System.IDisposable.Dispose()
extern void U3CAttackU3Ed__14_System_IDisposable_Dispose_m6ADB88D43C55E299190B759F31E727643AB5B2C5 ();
// 0x0000015F System.Boolean AimAndAttack_<Attack>d__14::MoveNext()
extern void U3CAttackU3Ed__14_MoveNext_m684A3923E528084F17BBE7D616F61F6F6849F970 ();
// 0x00000160 System.Object AimAndAttack_<Attack>d__14::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAttackU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1ABB63BCCABEE7C5FC89262BC56129B58ECE9F67 ();
// 0x00000161 System.Void AimAndAttack_<Attack>d__14::System.Collections.IEnumerator.Reset()
extern void U3CAttackU3Ed__14_System_Collections_IEnumerator_Reset_mA9C9322BFAC275CE0290D5E1C585025DDDAD133C ();
// 0x00000162 System.Object AimAndAttack_<Attack>d__14::System.Collections.IEnumerator.get_Current()
extern void U3CAttackU3Ed__14_System_Collections_IEnumerator_get_Current_mB1EFCD0847F10DEBF498263AD815E52E608329F0 ();
// 0x00000163 System.Void FireFly_<PrepAttack>d__10::.ctor(System.Int32)
extern void U3CPrepAttackU3Ed__10__ctor_mF8E86FA4F71DB2E36A06AF07B89C94D8D9337EC6 ();
// 0x00000164 System.Void FireFly_<PrepAttack>d__10::System.IDisposable.Dispose()
extern void U3CPrepAttackU3Ed__10_System_IDisposable_Dispose_m2A1B1E10ECF2ECEEDBF2B56BE9FECAF3A28C4400 ();
// 0x00000165 System.Boolean FireFly_<PrepAttack>d__10::MoveNext()
extern void U3CPrepAttackU3Ed__10_MoveNext_mFB3180F88E515A8E1EBD8B0854973E7B22D8E04E ();
// 0x00000166 System.Object FireFly_<PrepAttack>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CPrepAttackU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD2B2DBD39108F4C02CFCE5D1B31AEB99A20E32D0 ();
// 0x00000167 System.Void FireFly_<PrepAttack>d__10::System.Collections.IEnumerator.Reset()
extern void U3CPrepAttackU3Ed__10_System_Collections_IEnumerator_Reset_mDEF48DE5D11D5F0C90AC61864907CB5B546F1693 ();
// 0x00000168 System.Object FireFly_<PrepAttack>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CPrepAttackU3Ed__10_System_Collections_IEnumerator_get_Current_m842ADB5A94304F03AAE5839A02E3EDF3EE409BEF ();
// 0x00000169 System.Void FireFly_<ChargeLightning>d__11::.ctor(System.Int32)
extern void U3CChargeLightningU3Ed__11__ctor_mA7C9E0F92BE1ACF2CC8F2B76F7CC47F373B1CF54 ();
// 0x0000016A System.Void FireFly_<ChargeLightning>d__11::System.IDisposable.Dispose()
extern void U3CChargeLightningU3Ed__11_System_IDisposable_Dispose_m4018E3A40DE9EA8B4CE337084F0A05EB3A235C0B ();
// 0x0000016B System.Boolean FireFly_<ChargeLightning>d__11::MoveNext()
extern void U3CChargeLightningU3Ed__11_MoveNext_m8B2E0F1C9AA7E60882698B0216EF21A1CDF2DCFF ();
// 0x0000016C System.Object FireFly_<ChargeLightning>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CChargeLightningU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE17D3C49DDB521ABB4D2F7E6DEE365EF69914BD3 ();
// 0x0000016D System.Void FireFly_<ChargeLightning>d__11::System.Collections.IEnumerator.Reset()
extern void U3CChargeLightningU3Ed__11_System_Collections_IEnumerator_Reset_mEAE53D5A53E78319AB96F82E43A713833E4870AC ();
// 0x0000016E System.Object FireFly_<ChargeLightning>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CChargeLightningU3Ed__11_System_Collections_IEnumerator_get_Current_m82FE2A1B29C92F22DFF285A215C5F8FE6663B95C ();
// 0x0000016F System.Void FireFly_<ShootLightning>d__12::.ctor(System.Int32)
extern void U3CShootLightningU3Ed__12__ctor_m1E4B764759A0891FDE7A4ADAEEAB5EE9FB419988 ();
// 0x00000170 System.Void FireFly_<ShootLightning>d__12::System.IDisposable.Dispose()
extern void U3CShootLightningU3Ed__12_System_IDisposable_Dispose_m9A8746EEE5535B1AC853376C650FA5DBCCAF2299 ();
// 0x00000171 System.Boolean FireFly_<ShootLightning>d__12::MoveNext()
extern void U3CShootLightningU3Ed__12_MoveNext_m581BC350AEB3D2B5309B46517614DA9DA8C0CFD0 ();
// 0x00000172 System.Object FireFly_<ShootLightning>d__12::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CShootLightningU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8C27075970AD8049785C100219F3C17394558A0D ();
// 0x00000173 System.Void FireFly_<ShootLightning>d__12::System.Collections.IEnumerator.Reset()
extern void U3CShootLightningU3Ed__12_System_Collections_IEnumerator_Reset_m7A3E578C7E6E76D7B3CAA3A8BA168CB10A54D370 ();
// 0x00000174 System.Object FireFly_<ShootLightning>d__12::System.Collections.IEnumerator.get_Current()
extern void U3CShootLightningU3Ed__12_System_Collections_IEnumerator_get_Current_m547FBF0A599B09B55BF7DA55B0568971486DC599 ();
// 0x00000175 System.Void FlyTrap_<Attack>d__9::.ctor(System.Int32)
extern void U3CAttackU3Ed__9__ctor_m7E34F89D67984F9CC37139A0B26DEFE2D64D2C04 ();
// 0x00000176 System.Void FlyTrap_<Attack>d__9::System.IDisposable.Dispose()
extern void U3CAttackU3Ed__9_System_IDisposable_Dispose_mAF10B9C8CA1338A3F47A71DEBD827480EA2BE6E3 ();
// 0x00000177 System.Boolean FlyTrap_<Attack>d__9::MoveNext()
extern void U3CAttackU3Ed__9_MoveNext_mB2B3EFADFFBE1DE4847D2A9D8EAE7E36D487C3BC ();
// 0x00000178 System.Object FlyTrap_<Attack>d__9::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAttackU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC873743E8FC808ECB53207C11F410DC17FFAAA16 ();
// 0x00000179 System.Void FlyTrap_<Attack>d__9::System.Collections.IEnumerator.Reset()
extern void U3CAttackU3Ed__9_System_Collections_IEnumerator_Reset_m1B3A764A454A5E0A4D8D42B725913EC7470ADFA2 ();
// 0x0000017A System.Object FlyTrap_<Attack>d__9::System.Collections.IEnumerator.get_Current()
extern void U3CAttackU3Ed__9_System_Collections_IEnumerator_get_Current_mF71DDCF7CFABFD68D966945737F3B8CA020C0C59 ();
// 0x0000017B System.Void GameManager_<SpawnEnemyEnergy>d__22::.ctor(System.Int32)
extern void U3CSpawnEnemyEnergyU3Ed__22__ctor_m84319FD9750DA290E45477A8EB03283397838120 ();
// 0x0000017C System.Void GameManager_<SpawnEnemyEnergy>d__22::System.IDisposable.Dispose()
extern void U3CSpawnEnemyEnergyU3Ed__22_System_IDisposable_Dispose_mD127393FDE121B0099B7115870F8DA7410852A38 ();
// 0x0000017D System.Boolean GameManager_<SpawnEnemyEnergy>d__22::MoveNext()
extern void U3CSpawnEnemyEnergyU3Ed__22_MoveNext_m61E560DB388866B235DB1C30064DA8C12CA86FA9 ();
// 0x0000017E System.Object GameManager_<SpawnEnemyEnergy>d__22::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSpawnEnemyEnergyU3Ed__22_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9211BB9B5E1DF842A4E7E2317DC718E2E471FC5B ();
// 0x0000017F System.Void GameManager_<SpawnEnemyEnergy>d__22::System.Collections.IEnumerator.Reset()
extern void U3CSpawnEnemyEnergyU3Ed__22_System_Collections_IEnumerator_Reset_mF38E04919E617D9C9CB9F6081439DC90809E62D7 ();
// 0x00000180 System.Object GameManager_<SpawnEnemyEnergy>d__22::System.Collections.IEnumerator.get_Current()
extern void U3CSpawnEnemyEnergyU3Ed__22_System_Collections_IEnumerator_get_Current_mD6F7429F60F200CFD83ABF44DE46387831711119 ();
// 0x00000181 System.Void SlowTeleport_<FlingPlayer>d__9::.ctor(System.Int32)
extern void U3CFlingPlayerU3Ed__9__ctor_m1F684303CF23A14485FEF9DAE6C899DE9EAA9334 ();
// 0x00000182 System.Void SlowTeleport_<FlingPlayer>d__9::System.IDisposable.Dispose()
extern void U3CFlingPlayerU3Ed__9_System_IDisposable_Dispose_m1AE5BBBBFF6779325FE2D0AF8D00E1E01B2D8773 ();
// 0x00000183 System.Boolean SlowTeleport_<FlingPlayer>d__9::MoveNext()
extern void U3CFlingPlayerU3Ed__9_MoveNext_m73F8B6B2B287F4BE734BDD18623D28EE714ABEF2 ();
// 0x00000184 System.Object SlowTeleport_<FlingPlayer>d__9::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CFlingPlayerU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF2F38480BF51C400ECCB1C92F35A8CC69201F09D ();
// 0x00000185 System.Void SlowTeleport_<FlingPlayer>d__9::System.Collections.IEnumerator.Reset()
extern void U3CFlingPlayerU3Ed__9_System_Collections_IEnumerator_Reset_m648DB59F2B7D18408C40B36E0F4CD2466AF061A0 ();
// 0x00000186 System.Object SlowTeleport_<FlingPlayer>d__9::System.Collections.IEnumerator.get_Current()
extern void U3CFlingPlayerU3Ed__9_System_Collections_IEnumerator_get_Current_mD4EB76448507C76A4F7460823744D0A6748CFF61 ();
// 0x00000187 System.Void Throw_<SpawnExplosion>d__18::.ctor(System.Int32)
extern void U3CSpawnExplosionU3Ed__18__ctor_mD1EAA2235FFF5719B585F9F4BB68343039DEC93D ();
// 0x00000188 System.Void Throw_<SpawnExplosion>d__18::System.IDisposable.Dispose()
extern void U3CSpawnExplosionU3Ed__18_System_IDisposable_Dispose_m33D8FFA5E462E3F5205EF9CDB3ED53E92E5323C3 ();
// 0x00000189 System.Boolean Throw_<SpawnExplosion>d__18::MoveNext()
extern void U3CSpawnExplosionU3Ed__18_MoveNext_mA77EAA1B2B6938EC1FEEF10766AB979DDB7650CB ();
// 0x0000018A System.Object Throw_<SpawnExplosion>d__18::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSpawnExplosionU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0865A7FCAE3068F3A3FB6BF53B0878205BAB578E ();
// 0x0000018B System.Void Throw_<SpawnExplosion>d__18::System.Collections.IEnumerator.Reset()
extern void U3CSpawnExplosionU3Ed__18_System_Collections_IEnumerator_Reset_m890302255E5B45BF27F6F60A165CDC72C79FF2EC ();
// 0x0000018C System.Object Throw_<SpawnExplosion>d__18::System.Collections.IEnumerator.get_Current()
extern void U3CSpawnExplosionU3Ed__18_System_Collections_IEnumerator_get_Current_m46F528D3EA63FE821E6B07A6C43103471F68AED1 ();
// 0x0000018D System.Void EnvMapAnimator_<Start>d__4::.ctor(System.Int32)
extern void U3CStartU3Ed__4__ctor_m8B0264798939C569742263D32E0054DBAB9AE6FF ();
// 0x0000018E System.Void EnvMapAnimator_<Start>d__4::System.IDisposable.Dispose()
extern void U3CStartU3Ed__4_System_IDisposable_Dispose_m3EFE2ADAD412045F666CFA1C8C9FF53AF92CBD75 ();
// 0x0000018F System.Boolean EnvMapAnimator_<Start>d__4::MoveNext()
extern void U3CStartU3Ed__4_MoveNext_m84F94A5CD6012300AC80698CDCA870A0A146E226 ();
// 0x00000190 System.Object EnvMapAnimator_<Start>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m129CB3E5CAFFA1D19D4988182EEF116F2086A637 ();
// 0x00000191 System.Void EnvMapAnimator_<Start>d__4::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_m345E255900454CC505A8AAE3BF6AEF3C06467EAB ();
// 0x00000192 System.Object EnvMapAnimator_<Start>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_m5920F51DCC2DC7C8BC98EE95D6CD4D7784997272 ();
// 0x00000193 System.Void TMPro.TMP_TextEventHandler_CharacterSelectionEvent::.ctor()
extern void CharacterSelectionEvent__ctor_mE2C306B8090F90261252C94D26AB5085580B11D5 ();
// 0x00000194 System.Void TMPro.TMP_TextEventHandler_SpriteSelectionEvent::.ctor()
extern void SpriteSelectionEvent__ctor_m9D9F101CB717ACD5449336DFFF70F86AE32BB6EC ();
// 0x00000195 System.Void TMPro.TMP_TextEventHandler_WordSelectionEvent::.ctor()
extern void WordSelectionEvent__ctor_mFD7F2937426D4AA1A8CBB13F62C3CC1D2061AD1E ();
// 0x00000196 System.Void TMPro.TMP_TextEventHandler_LineSelectionEvent::.ctor()
extern void LineSelectionEvent__ctor_mA23AFEC8E11183CF472044FA72B07AD28ED6E675 ();
// 0x00000197 System.Void TMPro.TMP_TextEventHandler_LinkSelectionEvent::.ctor()
extern void LinkSelectionEvent__ctor_m02CC491DBE4B2FF05A8FD4285813215ED3D323E5 ();
// 0x00000198 System.Void TMPro.Examples.Benchmark01_<Start>d__10::.ctor(System.Int32)
extern void U3CStartU3Ed__10__ctor_m328932E4B6124311CD738F2F84F69BC149209129 ();
// 0x00000199 System.Void TMPro.Examples.Benchmark01_<Start>d__10::System.IDisposable.Dispose()
extern void U3CStartU3Ed__10_System_IDisposable_Dispose_m209F531CE6ED7F07649497DD15817C1D7C1880A1 ();
// 0x0000019A System.Boolean TMPro.Examples.Benchmark01_<Start>d__10::MoveNext()
extern void U3CStartU3Ed__10_MoveNext_mD927C85D41034011055A7CA3AFFAF4E10464F65D ();
// 0x0000019B System.Object TMPro.Examples.Benchmark01_<Start>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m18BA91C8A20CBD6976D52E335563D9B42C1AE9A8 ();
// 0x0000019C System.Void TMPro.Examples.Benchmark01_<Start>d__10::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_m5A67B5BDE759A157229E6CF24E653B79B2AC0200 ();
// 0x0000019D System.Object TMPro.Examples.Benchmark01_<Start>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_mA521C1EFA357A9F4F4CAA68A4D0B85468764323C ();
// 0x0000019E System.Void TMPro.Examples.Benchmark01_UGUI_<Start>d__10::.ctor(System.Int32)
extern void U3CStartU3Ed__10__ctor_m9A8C7C0644996520AD443A4F7CA527BF05C54C3C ();
// 0x0000019F System.Void TMPro.Examples.Benchmark01_UGUI_<Start>d__10::System.IDisposable.Dispose()
extern void U3CStartU3Ed__10_System_IDisposable_Dispose_m94D1420C08F57F2901E2499D36778BB8F1C76932 ();
// 0x000001A0 System.Boolean TMPro.Examples.Benchmark01_UGUI_<Start>d__10::MoveNext()
extern void U3CStartU3Ed__10_MoveNext_m31AF957FFAEEED4BE0F39A1185C6112C4EB6F7AA ();
// 0x000001A1 System.Object TMPro.Examples.Benchmark01_UGUI_<Start>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m39B535B222104759319A54A6D7E2E81482A1F71E ();
// 0x000001A2 System.Void TMPro.Examples.Benchmark01_UGUI_<Start>d__10::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_mC2B435140D045B6A20FB105E0E2CBD625218CA74 ();
// 0x000001A3 System.Object TMPro.Examples.Benchmark01_UGUI_<Start>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_m9A98CCB7604AAD93919CAE48955C6A6CB8C38790 ();
// 0x000001A4 System.Void TMPro.Examples.ShaderPropAnimator_<AnimateProperties>d__6::.ctor(System.Int32)
extern void U3CAnimatePropertiesU3Ed__6__ctor_mB5F6ED6FCDA5BEAD56E22B64283D7A4D7F7EAE71 ();
// 0x000001A5 System.Void TMPro.Examples.ShaderPropAnimator_<AnimateProperties>d__6::System.IDisposable.Dispose()
extern void U3CAnimatePropertiesU3Ed__6_System_IDisposable_Dispose_m29AAE9560CA4EEB4A548A68ACA085EC9E4CB8EA5 ();
// 0x000001A6 System.Boolean TMPro.Examples.ShaderPropAnimator_<AnimateProperties>d__6::MoveNext()
extern void U3CAnimatePropertiesU3Ed__6_MoveNext_m4F2D37B672E95820F49489611196CDE334736157 ();
// 0x000001A7 System.Object TMPro.Examples.ShaderPropAnimator_<AnimateProperties>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimatePropertiesU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m21D2B0A0B0CADF520D05FE4948F1DE94CF119630 ();
// 0x000001A8 System.Void TMPro.Examples.ShaderPropAnimator_<AnimateProperties>d__6::System.Collections.IEnumerator.Reset()
extern void U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_Reset_m1E942C0FD32005FDBB182CF646FD2312BA273BC7 ();
// 0x000001A9 System.Object TMPro.Examples.ShaderPropAnimator_<AnimateProperties>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_get_Current_mFC3602799F1D07BB002093DFB879FC759384FDD3 ();
// 0x000001AA System.Void TMPro.Examples.SkewTextExample_<WarpText>d__7::.ctor(System.Int32)
extern void U3CWarpTextU3Ed__7__ctor_mA03118DB0FD3BF160500E127D1FACDAF45313047 ();
// 0x000001AB System.Void TMPro.Examples.SkewTextExample_<WarpText>d__7::System.IDisposable.Dispose()
extern void U3CWarpTextU3Ed__7_System_IDisposable_Dispose_m9D7F6A90DA911D77EE72A2824FF9690CED05FBC8 ();
// 0x000001AC System.Boolean TMPro.Examples.SkewTextExample_<WarpText>d__7::MoveNext()
extern void U3CWarpTextU3Ed__7_MoveNext_m7FE0DD003507BAD92E35CC5DACE5D043ADD766ED ();
// 0x000001AD System.Object TMPro.Examples.SkewTextExample_<WarpText>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWarpTextU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m349F81ECD49FF12E4009E2E56DB81974D68C6DAD ();
// 0x000001AE System.Void TMPro.Examples.SkewTextExample_<WarpText>d__7::System.Collections.IEnumerator.Reset()
extern void U3CWarpTextU3Ed__7_System_Collections_IEnumerator_Reset_m3122CE754238FB7815F7ABE8E7DFAF3AB7B03278 ();
// 0x000001AF System.Object TMPro.Examples.SkewTextExample_<WarpText>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CWarpTextU3Ed__7_System_Collections_IEnumerator_get_Current_m79BF250C4ADC29ACF11370E2B5BD4FFD78709565 ();
// 0x000001B0 System.Void TMPro.Examples.TeleType_<Start>d__4::.ctor(System.Int32)
extern void U3CStartU3Ed__4__ctor_m8231909D78A27061165C450481E233339F300046 ();
// 0x000001B1 System.Void TMPro.Examples.TeleType_<Start>d__4::System.IDisposable.Dispose()
extern void U3CStartU3Ed__4_System_IDisposable_Dispose_m6886DB5D83361607B72BBCCB7D484B9C0BFE1981 ();
// 0x000001B2 System.Boolean TMPro.Examples.TeleType_<Start>d__4::MoveNext()
extern void U3CStartU3Ed__4_MoveNext_m1CD1306C9E074D3F941AC906A48D3CA97C148774 ();
// 0x000001B3 System.Object TMPro.Examples.TeleType_<Start>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE50AC5FA9F27773C51DD3E4188A748BA0A513F8A ();
// 0x000001B4 System.Void TMPro.Examples.TeleType_<Start>d__4::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_m5D6EE5C4B2C20A433129D8BFD13DFC82681346A2 ();
// 0x000001B5 System.Object TMPro.Examples.TeleType_<Start>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_m1FA5600514131056D8198F8442F37A4A22A9F065 ();
// 0x000001B6 System.Void TMPro.Examples.TextConsoleSimulator_<RevealCharacters>d__7::.ctor(System.Int32)
extern void U3CRevealCharactersU3Ed__7__ctor_m48510711FC78DFEA9CF4603E1E75F4DF7C5F1489 ();
// 0x000001B7 System.Void TMPro.Examples.TextConsoleSimulator_<RevealCharacters>d__7::System.IDisposable.Dispose()
extern void U3CRevealCharactersU3Ed__7_System_IDisposable_Dispose_m5B88486625A74566DF3FC7BFB4CE327A58C57ED4 ();
// 0x000001B8 System.Boolean TMPro.Examples.TextConsoleSimulator_<RevealCharacters>d__7::MoveNext()
extern void U3CRevealCharactersU3Ed__7_MoveNext_mE45076151810A7C1F83802B7754DE92E812EABAB ();
// 0x000001B9 System.Object TMPro.Examples.TextConsoleSimulator_<RevealCharacters>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRevealCharactersU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9C330834C7113C8468CC1A09417B7C521CAE833B ();
// 0x000001BA System.Void TMPro.Examples.TextConsoleSimulator_<RevealCharacters>d__7::System.Collections.IEnumerator.Reset()
extern void U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_Reset_m25B719FFD0CAB1DFF2853FF47A4EE2032176E287 ();
// 0x000001BB System.Object TMPro.Examples.TextConsoleSimulator_<RevealCharacters>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_get_Current_m9A540E1B18E93F749F4BFD4C8597AEC9F2C199F7 ();
// 0x000001BC System.Void TMPro.Examples.TextConsoleSimulator_<RevealWords>d__8::.ctor(System.Int32)
extern void U3CRevealWordsU3Ed__8__ctor_mDF41BA5FE3D53FEC3CB8214FCA7853A1142DE70C ();
// 0x000001BD System.Void TMPro.Examples.TextConsoleSimulator_<RevealWords>d__8::System.IDisposable.Dispose()
extern void U3CRevealWordsU3Ed__8_System_IDisposable_Dispose_m46827499CD3657AF468926B6302D2340ED975965 ();
// 0x000001BE System.Boolean TMPro.Examples.TextConsoleSimulator_<RevealWords>d__8::MoveNext()
extern void U3CRevealWordsU3Ed__8_MoveNext_mE0CB1189CFAD7F7B3E74438A4528D9BFAABB48DE ();
// 0x000001BF System.Object TMPro.Examples.TextConsoleSimulator_<RevealWords>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRevealWordsU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4A3E941DF6C67BC9ACEFEAA09D11167B3F3A38EC ();
// 0x000001C0 System.Void TMPro.Examples.TextConsoleSimulator_<RevealWords>d__8::System.Collections.IEnumerator.Reset()
extern void U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_Reset_m967B427886233CDB5DFFEA323F02A89CE9330CC8 ();
// 0x000001C1 System.Object TMPro.Examples.TextConsoleSimulator_<RevealWords>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_get_Current_m0C1B2941BEC04593993127F6D9DCDBA6FAE7CC20 ();
// 0x000001C2 System.Void TMPro.Examples.TextMeshProFloatingText_<DisplayTextMeshProFloatingText>d__12::.ctor(System.Int32)
extern void U3CDisplayTextMeshProFloatingTextU3Ed__12__ctor_m13B7271203EDC80E649C1CE40F09A93BDA2633DF ();
// 0x000001C3 System.Void TMPro.Examples.TextMeshProFloatingText_<DisplayTextMeshProFloatingText>d__12::System.IDisposable.Dispose()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__12_System_IDisposable_Dispose_m3D1611AA38746EF0827F5260DADCC361DD56DF0C ();
// 0x000001C4 System.Boolean TMPro.Examples.TextMeshProFloatingText_<DisplayTextMeshProFloatingText>d__12::MoveNext()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__12_MoveNext_m03111B7039F928512A7A53F8DA9C04671AA8D7EE ();
// 0x000001C5 System.Object TMPro.Examples.TextMeshProFloatingText_<DisplayTextMeshProFloatingText>d__12::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m55B08E77035437C2B75332F29214C33214417414 ();
// 0x000001C6 System.Void TMPro.Examples.TextMeshProFloatingText_<DisplayTextMeshProFloatingText>d__12::System.Collections.IEnumerator.Reset()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__12_System_Collections_IEnumerator_Reset_m6737F2D695AB295240F15C5B0B4F24A59106BFDA ();
// 0x000001C7 System.Object TMPro.Examples.TextMeshProFloatingText_<DisplayTextMeshProFloatingText>d__12::System.Collections.IEnumerator.get_Current()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__12_System_Collections_IEnumerator_get_Current_m6BD4D2442BDDDFB6859CFE646182580A0A1E130A ();
// 0x000001C8 System.Void TMPro.Examples.TextMeshProFloatingText_<DisplayTextMeshFloatingText>d__13::.ctor(System.Int32)
extern void U3CDisplayTextMeshFloatingTextU3Ed__13__ctor_m7E4C3B87E56A7B23D725D653E52ADE02554EAE3E ();
// 0x000001C9 System.Void TMPro.Examples.TextMeshProFloatingText_<DisplayTextMeshFloatingText>d__13::System.IDisposable.Dispose()
extern void U3CDisplayTextMeshFloatingTextU3Ed__13_System_IDisposable_Dispose_m485A7C4CF3496858A72CBA647B29BC610F39FE39 ();
// 0x000001CA System.Boolean TMPro.Examples.TextMeshProFloatingText_<DisplayTextMeshFloatingText>d__13::MoveNext()
extern void U3CDisplayTextMeshFloatingTextU3Ed__13_MoveNext_m6A3C88B1149D12B58E6E580BC04622F553ED1424 ();
// 0x000001CB System.Object TMPro.Examples.TextMeshProFloatingText_<DisplayTextMeshFloatingText>d__13::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDisplayTextMeshFloatingTextU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2EE54B2AC8AAE44BACF8EE8954A6D824045CFC55 ();
// 0x000001CC System.Void TMPro.Examples.TextMeshProFloatingText_<DisplayTextMeshFloatingText>d__13::System.Collections.IEnumerator.Reset()
extern void U3CDisplayTextMeshFloatingTextU3Ed__13_System_Collections_IEnumerator_Reset_m8524C9700DEF2DE7A28BBFDB938FE159985E86EE ();
// 0x000001CD System.Object TMPro.Examples.TextMeshProFloatingText_<DisplayTextMeshFloatingText>d__13::System.Collections.IEnumerator.get_Current()
extern void U3CDisplayTextMeshFloatingTextU3Ed__13_System_Collections_IEnumerator_get_Current_m2293C6A327D4D1CCC1ACBC90DBE00DC1C6F39EBE ();
// 0x000001CE System.Void TMPro.Examples.VertexColorCycler_<AnimateVertexColors>d__3::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__3__ctor_m3D7543ED636AFCD2C59E834668568DB2A4005F6A ();
// 0x000001CF System.Void TMPro.Examples.VertexColorCycler_<AnimateVertexColors>d__3::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__3_System_IDisposable_Dispose_m3F7092E831D4CFDACC5B6254958DFEC2D313D0EE ();
// 0x000001D0 System.Boolean TMPro.Examples.VertexColorCycler_<AnimateVertexColors>d__3::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__3_MoveNext_mC0D42DAE0A614F2B91AF1F9A2F8C0AF471CA0AE4 ();
// 0x000001D1 System.Object TMPro.Examples.VertexColorCycler_<AnimateVertexColors>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6597EE639379454CADA3823A1B955FEFBAF894BD ();
// 0x000001D2 System.Void TMPro.Examples.VertexColorCycler_<AnimateVertexColors>d__3::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_Reset_m9943586181F26EEE58A42138CE0489DDF07EA359 ();
// 0x000001D3 System.Object TMPro.Examples.VertexColorCycler_<AnimateVertexColors>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_get_Current_m2D4E4AA5EEB4F07F283E61018685199A4C2D56BD ();
// 0x000001D4 System.Void TMPro.Examples.VertexJitter_<AnimateVertexColors>d__11::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__11__ctor_mC74A801C40038DA74D856FACFBAD12F3BC3E11E7 ();
// 0x000001D5 System.Void TMPro.Examples.VertexJitter_<AnimateVertexColors>d__11::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_m07C61223FC885322B6066E81CB130879661D5A72 ();
// 0x000001D6 System.Boolean TMPro.Examples.VertexJitter_<AnimateVertexColors>d__11::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__11_MoveNext_m04CD6FB321DE3AD8D5890766C1F2CAAE4112EDF2 ();
// 0x000001D7 System.Object TMPro.Examples.VertexJitter_<AnimateVertexColors>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m35F5F559A34D3F9BE1E5DD636FEAF517164A6B07 ();
// 0x000001D8 System.Void TMPro.Examples.VertexJitter_<AnimateVertexColors>d__11::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_m325AE901312C158848B79B302EBA7BE847C93D49 ();
// 0x000001D9 System.Object TMPro.Examples.VertexJitter_<AnimateVertexColors>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_mA688DA41E2C04FF9774F607794C114057FA055C6 ();
// 0x000001DA System.Void TMPro.Examples.VertexShakeA_<AnimateVertexColors>d__11::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__11__ctor_m89953000A887F8C0931B0E98B484FBAAC37748C5 ();
// 0x000001DB System.Void TMPro.Examples.VertexShakeA_<AnimateVertexColors>d__11::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_m9AFBB2A87D38A1358F9EB09D617075D72DEED19B ();
// 0x000001DC System.Boolean TMPro.Examples.VertexShakeA_<AnimateVertexColors>d__11::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__11_MoveNext_m9F607EF7DBDFFC4FB2307B0EC4C7F33EEE63BBE8 ();
// 0x000001DD System.Object TMPro.Examples.VertexShakeA_<AnimateVertexColors>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF47F7A3AB51F9BC1B8F74E10FD82B29C1B223DCD ();
// 0x000001DE System.Void TMPro.Examples.VertexShakeA_<AnimateVertexColors>d__11::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_m99B988724C4F53F7F8334C5F633C8B1603185ADD ();
// 0x000001DF System.Object TMPro.Examples.VertexShakeA_<AnimateVertexColors>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_mE9127628FC1726149DA7C8FE95A7D4CFB1EE1655 ();
// 0x000001E0 System.Void TMPro.Examples.VertexShakeB_<AnimateVertexColors>d__10::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__10__ctor_mBA04B89258FA2EF09266E1766AB0B815E521897A ();
// 0x000001E1 System.Void TMPro.Examples.VertexShakeB_<AnimateVertexColors>d__10::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_mE001B767DE85B4B3B86A0C080B9FC00381340A1C ();
// 0x000001E2 System.Boolean TMPro.Examples.VertexShakeB_<AnimateVertexColors>d__10::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__10_MoveNext_m611487BEE2BB82A9BFF5EA2157BDCA610F87876D ();
// 0x000001E3 System.Object TMPro.Examples.VertexShakeB_<AnimateVertexColors>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6A54A1BF63433F860822B43ABA9FAC6A4124409C ();
// 0x000001E4 System.Void TMPro.Examples.VertexShakeB_<AnimateVertexColors>d__10::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_m48F09B740CBEEC27459498932572D2869A1A4CBE ();
// 0x000001E5 System.Object TMPro.Examples.VertexShakeB_<AnimateVertexColors>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_mA915AC55E6DEE343235545FC1FE6F6CA5611DF3C ();
// 0x000001E6 System.Void TMPro.Examples.VertexZoom_<>c__DisplayClass10_0::.ctor()
extern void U3CU3Ec__DisplayClass10_0__ctor_m1C2F2204ADD6E4BA14E14CF255F520B7E2464941 ();
// 0x000001E7 System.Int32 TMPro.Examples.VertexZoom_<>c__DisplayClass10_0::<AnimateVertexColors>b__0(System.Int32,System.Int32)
extern void U3CU3Ec__DisplayClass10_0_U3CAnimateVertexColorsU3Eb__0_m673C7031DB1882DEEFB53F179E3C2FB13FB6CA5A ();
// 0x000001E8 System.Void TMPro.Examples.VertexZoom_<AnimateVertexColors>d__10::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__10__ctor_m2F5D29C1CA797C0BCEC16C8B5D96D1CF5B07F6F3 ();
// 0x000001E9 System.Void TMPro.Examples.VertexZoom_<AnimateVertexColors>d__10::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_m1A43A8EA2FB689EE2B39D8A624580594374905B9 ();
// 0x000001EA System.Boolean TMPro.Examples.VertexZoom_<AnimateVertexColors>d__10::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__10_MoveNext_mCA826F12F72BDBB79F9B50DC9CBC6E7F80B2110F ();
// 0x000001EB System.Object TMPro.Examples.VertexZoom_<AnimateVertexColors>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFF0E754E9557C03F892EFA19C0307AECD6BA8C4D ();
// 0x000001EC System.Void TMPro.Examples.VertexZoom_<AnimateVertexColors>d__10::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_m002A7C6C8AE61BE6CD6FD0B2173C75DBF47BCC56 ();
// 0x000001ED System.Object TMPro.Examples.VertexZoom_<AnimateVertexColors>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_mE91B03C99FCCBC8ED4E37649C5364E83D047B053 ();
// 0x000001EE System.Void TMPro.Examples.WarpTextExample_<WarpText>d__8::.ctor(System.Int32)
extern void U3CWarpTextU3Ed__8__ctor_m845C9410F3856EF25585F59C425200EEFCEFB3C0 ();
// 0x000001EF System.Void TMPro.Examples.WarpTextExample_<WarpText>d__8::System.IDisposable.Dispose()
extern void U3CWarpTextU3Ed__8_System_IDisposable_Dispose_m63AC2AE8BC0FCF89812A33CAF150E9D1B56BAE6A ();
// 0x000001F0 System.Boolean TMPro.Examples.WarpTextExample_<WarpText>d__8::MoveNext()
extern void U3CWarpTextU3Ed__8_MoveNext_m98D3E999A69E233C7AD5F357A0D0623D731DCDAA ();
// 0x000001F1 System.Object TMPro.Examples.WarpTextExample_<WarpText>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWarpTextU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3971C0D86C5903812972245A6F872D101ACB5189 ();
// 0x000001F2 System.Void TMPro.Examples.WarpTextExample_<WarpText>d__8::System.Collections.IEnumerator.Reset()
extern void U3CWarpTextU3Ed__8_System_Collections_IEnumerator_Reset_mF17827612AC7C91DE879114D1D6428450B9504D0 ();
// 0x000001F3 System.Object TMPro.Examples.WarpTextExample_<WarpText>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CWarpTextU3Ed__8_System_Collections_IEnumerator_get_Current_m98A13A358D4E71D9612F68731A7AE11A3DD080DE ();
static Il2CppMethodPointer s_methodPointers[499] = 
{
	GameOverMenu_RestartGame_m4A041A23D9426E40157CF5016536DB122A94C840,
	GameOverMenu_BackToMenu_mDD1212E86D9E7AD1D9DE2CBD8EC4EE218F1595B5,
	GameOverMenu__ctor_m398A846133DBF19CBFC9CE4A237CF0026E881AA7,
	ManagePerimeter__ctor_mC7BFA403B5D458A12336C86E8FE870ED1AA82585,
	AimAndAttack_Start_m27AC2D3B1B12B155864C4C61B927E12121791E9F,
	AimAndAttack_Update_mFDA390CCB213C2FB04E654F7CE49E9A61C5AF0D9,
	AimAndAttack_CheckDistance_m3000D5D89B2CE82DDFA1FA524B609426E581C888,
	AimAndAttack_WaitTime_mC71406FE4B30820FF614B275D4E9333D3853417E,
	AimAndAttack_Attack_m8C20C41412C9EB187DFF726E2235491F1DCC6827,
	AimAndAttack_OnCollisionEnter2D_m01E490BB18DCC1DF43319B1DC9097E1E433A9AD0,
	AimAndAttack__ctor_mE7EF81A0431220D7D7340FFEF2FFB65181F9978E,
	EnemyTouch__ctor_mFDD2FE38EDDF2064C6B0A1ED339A8DF55CFA3AE8,
	FireFly_Start_mE0FF4EA07DDE723AADD5BD01CC44F61379F11E52,
	FireFly_Update_mB9C34AB0D9CFE4C874E334BF12C0FA8531438B54,
	FireFly_PrepAttack_m9C164D2AAF3EFD923E0802FB9A083BEACC992F02,
	FireFly_ChargeLightning_m19DECA1BA654F1FD09C27F41D1FD795D212B2363,
	FireFly_ShootLightning_m94EDC436FA191956273D9B2D6C23AC7E11197206,
	FireFly_OnCollisionEnter2D_m4FB81B087059CCFA062775D4BA6554D109250F7D,
	FireFly__ctor_mD9F6583A4D3F7EBC638665616236D4B81395D4D8,
	FlyTrap_Awake_m20F9C5A000CA32769FE6A9494101BF95F476137F,
	FlyTrap_OnCollisionEnter2D_m0541DB938A942248211961306701DD000755F577,
	FlyTrap_Attack_mDF9B00839C94A20DF13CB00722F9244AD558F85A,
	FlyTrap_Update_mC23F71056FF45F1EF6A3B001496658CE94125694,
	FlyTrap__ctor_m04A42C50F7D56A86F2F1AB2C75486F841A6376BA,
	LightningAttack_OnCollisionEnter2D_m827FF2F47AC63368934777A8D45B16C97ADD4A66,
	LightningAttack__ctor_m736BEAF9A46979F105DABC55019977B67C86FBEA,
	Patrol_Start_m3A4024FE7C256346D6AB36F725E4A2245D03F45C,
	Patrol_Update_mBD2B1D83FD0B56D05AC0581CC99A6A7073F4A441,
	Patrol_PatrolPath_m375E0C5404A5F2C63D2D1B714818DC8FA053CB6A,
	Patrol_PausePatrol_m81ADF0499457D837D276E14BBA66F58916E9A9CE,
	Patrol_ResumePatrol_m64CDE69E7C00A09F7434066D96DF3E6108B45CAA,
	Patrol__ctor_mCC3DA6E03C5863BB40538EED0F518F46CE66831D,
	BreakObject_Update_m901A5E63EF08C0799D83534B30655C9F94F66913,
	BreakObject_OnCollisionEnter2D_m2ABF93DE7C1D07A34EFCBB5F3544A88D2F40C547,
	BreakObject__ctor_mC8B303EDC65E897BC9A87305759CEDF4D295B316,
	CollectCoin_OnTriggerEnter2D_m66AA818AED90FD132656B8BC85BF9637FE464AFD,
	CollectCoin__ctor_m2604E41E17F059AB6CEA0663A80FB8F30793C0FD,
	EnemyEnergy_Start_mE28802C8C093DDF7618DC1AD7012A6BA7A5FD8E1,
	EnemyEnergy_Update_m02F5892F620A38840BEAF6877DE91BDAE43BADA9,
	EnemyEnergy__ctor_m8B04CE902205EDEC5A8068E0784E5D618893D117,
	FindGrapplingPoint__ctor_m6BE9F6EDBB2456A31E96942CF561CE2A3052160C,
	Gem_SetValues_m90DDCAB6480665FF5736FBB00FE47F62F931E420,
	Gem_Start_mB0F178AEE04F6B014AE575686729CB3BE73DD59D,
	Gem_Update_mBCFA445505512609B1D0A045F567DDC85CC3F2CB,
	Gem_ThrowGem_mD065029B005F13AECAC985525165F5A981AC746E,
	Gem_OnTriggerEnter2D_m40F7365F96899F34BDD0C0DCFFD523197C135566,
	Gem__ctor_m81577DAC39344D4EE83A17B5A836838ACA996636,
	GrapplingHook_Start_m5C54D663CC9F37744E8D8ED5A7171E428A831EEB,
	GrapplingHook__ctor_mD5EBF32749DDDCB0A4E2631E14FD8BB497DBE610,
	KillWall_OnCollisionEnter2D_m7829F8CA997A3A5676CACF5CD3265EA5A1AF20BD,
	KillWall_Update_m3AD886F9410E3E4EDC3F42A5E463A84F1DD8253F,
	KillWall__ctor_m1D7582E2BC825C188FCDE1973FEB2EC934AAE543,
	Parallax_Start_m9774B4042DAD92E115B83BBCD41372C3769FE9AF,
	Parallax_Update_m94262B99102F6FF508B5AC680447AB5FF6230306,
	Parallax__ctor_m54405085336E416F633A4B952F796BF1333228B9,
	Ruby_Start_m927E3BEE3A4C812DD47F5AE9E2BAEB1BA90C4041,
	Ruby_Update_m113F4A09C3A2DDA5A9A3C2A119C75A6DEEC30A4D,
	Ruby__ctor_mF8F52F96098FD06A02030A502C70C3B179F3F461,
	ScreenShake__ctor_m486EB047157A617284A82D293E1ECCBD5F56EC62,
	GameManager_Awake_mE60F41F3186E80B2BAB293918745366D18508C0F,
	GameManager_Update_m07DC32583BF09EB71183725B7B95FA7B4716988A,
	GameManager_GameOver_m658E745BE197CC36CFAE422F8037F41A1B7FFFD3,
	GameManager_RestartGame_m1503EDBB7CBD522A762D8B37114E7AC96659FA86,
	GameManager_IsCharacterAlive_mD1D62CC44BED4D6F137E3819EC14F4B52F777F09,
	GameManager_KillPlayer_m03AD86B9DBDD88EC3DCB535059ACD39957A55D79,
	GameManager_EnemyKilled_m4F03B343CBB34287ACB587BB9DA9AC03BF7793A2,
	GameManager_SpawnEnemyEnergy_m6ED2C35DE160BD07279EE53FD795869C01CFDA1C,
	GameManager__ctor_mF7F1107D38DE91EB8A57C1C3BB1A932C50CD9693,
	InputHandler_Awake_m5F930CA361CCBB6FF91D8242D0F5F34140B2C170,
	InputHandler_Update_m7835D52BDC816E70F683678D1B5EF5EE906DD4FA,
	InputHandler_TouchDown_m0FB9FF7EA7CEF7ABAB1C14294C50EB91F4031D88,
	InputHandler_TouchUp_mFECC38B0A71344ED7F638109BB0B141F10ABA8A7,
	InputHandler__ctor_mE091B2759D08A7D2A019C9734ADD5B39D4ACD11F,
	Inventory_Awake_m03B255C17B082423F82DF256F14EDB7A4912B21E,
	Inventory_Start_mF9115FBB18A7772CAD4ED60B31A2F128DA55DF83,
	Inventory_AddToInventory_m5F8D81D95C617A278A66AA03A8C2884D3D250675,
	Inventory_RemoveFromInventory_mB25EA0FF2138F1ADA439A3B4EF24A33B8DF65DD8,
	Inventory_GetBackpackWeight_m4B237C7B802B358B432B942FCE6404170F3F3812,
	Inventory_GetTotalCurrency_mBB33F7DE191BF1D091C1BAABADAB130ECC1DF1F6,
	Inventory__ctor_mB946DCD27224D66DDEE96C7EE8355A7E3FE91CC7,
	MainMenu_PlayGame_m4CD3D61E23D84AD1A018C84D561EAE39ED2D76F7,
	MainMenu__ctor_mF17B753D99BD98B88E949A5B9CA53892E19A6CD5,
	ManageScore_Awake_mF2DCFD41E18C8068C3CF8D216F844294142D1C9F,
	ManageScore_Start_m696BA5C87720F42307E1B18C4F443D8A1CF84E9F,
	ManageScore_AddPoints_m9ACFC712DD0D66352887F8052737ABFE57D7EE4F,
	ManageScore_SubtractEnergy_mC7F98068A28D3FFD8878A1B8E0C0AD1658D08C70,
	ManageScore_AddEnergy_mACDB07BF15F1E20CFA126F6EFB6941FF898DCD66,
	ManageScore_GetPoints_m3DA9EA5BD95AF657C8D4C363F13A0AEAEC88977C,
	ManageScore_GetEnergy_mB4ECF116F0FA510882ECC7F8BA447DA44962B3EE,
	ManageScore_SetDistance_m2908A68F86688117DA6EF4FC57C0AA08ACE8D85C,
	ManageScore_GetDistance_m007DC3BBB0AE74387730FC52CF1F4EE01ECFC291,
	ManageScore__ctor_m5FF340600B9BF4B8A0D764C866BFD4A6328DC4DB,
	ManageUI_Awake_m1EE1D672DA2CEF51CA96841E1FED3DF3ACF82501,
	ManageUI_Update_m56C88C58A98CC39657FB218A2ECE1891A8E02B60,
	ManageUI_UpdateText_m486D9F4840B41F3A0DE338DE4913C6CE87CC7A93,
	ManageUI_HideScoreText_m99EDE7F6A8A02EAB62302D6541E7C2A0F895DE04,
	ManageUI_ShowScoreText_mAFFCCD28AABF6FEC6D8DE40D019A588902E068F9,
	ManageUI__ctor_mD417ED31B5D59C8A74AE43FACC287823710CA760,
	ObjectPooler_Awake_mE25E4226F8F8927FFA714DB869B4002C498520E8,
	ObjectPooler_getPooledObject_m4D0F46D453C8F695E32F9F2F60731676A6F1ADC8,
	ObjectPooler_CheckForInactive_m085476FD0B27F64F2862FF39FB8CE3610DC096AE,
	ObjectPooler_PrepareForReuse_mC9773ADBDED1E5BFE852FD1CA5E0700B1EBD998D,
	ObjectPooler__ctor_m649927772575A33D98CA8599915DEB5C0FD18352,
	PlayAgain_RestartGame_mD745D4B0812D2E9B48506F01D888DBBE63D98467,
	PlayAgain_BackToMenu_mC1939C52A5440D56BCD3202A773AB07BE9F860EF,
	PlayAgain__ctor_m866DC30F2DD4E2260CF6E2DB49E87BEEEB74375E,
	SpawnEnemies_Start_m87D7F16DF1338668EF6A52B7A2E9395BD628CAE6,
	SpawnEnemies_Update_m4A15B4C624986D1D57771DBB11364843535E8F91,
	SpawnEnemies__ctor_m13F88F19FC7CEE95779AAD5950265F61444894FA,
	SpawnGrapplingPoints_Start_m05D29FF2CAB366F64D7CCE03DE9CDB93EA97607C,
	SpawnGrapplingPoints_Update_m3F6415C21FB814192F729A47128B00A88EEBC56E,
	SpawnGrapplingPoints_SwapPoints_m0DB3E30E8DC9FE8B8201FC1049CC8B4219C79638,
	SpawnGrapplingPoints__ctor_m9246271CC59C3CDDCB2BFCAAD22B06A2FF9C411A,
	SpawnObjects_Start_m290BD8FF74C84B0278F7F3767DE9209325D583A3,
	SpawnObjects_Update_mC4BFD9B7253705E0AD0A79D7AD2F58AB3ED56075,
	SpawnObjects_SpawnObstacle_mC3B4B5FA49B259C32F9557C187446886D4DFC0A6,
	SpawnObjects_SpawnCoin_m0A3D4EB46699D4A16C18AAC69701130AE4F72BA8,
	SpawnObjects_DestroyObjects_m9F5666BEBF79A87AB49FA6AD0D25C80CCB7A0010,
	SpawnObjects_PrepareStage_m07C8408CA81998E8372E8D9B6A1D3234D906BC2B,
	SpawnObjects_SpawnEnemy_m75AD83486289CA2262DAC56F8B3B51E30547C18A,
	SpawnObjects__ctor_mE6D3F0E539008648986DA4D502C4A00A596D3CB4,
	SpawnWalls_Start_m505B9A978AC3EF486829D5D8A99CFEB41172F4A8,
	SpawnWalls_Update_m76AA6719B5F83DECCCB25C320D12AE4254F91C33,
	SpawnWalls_SpawnWall_m4DD2141ABE093BD78AA18744892E9E2529ECD332,
	SpawnWalls_GetWallWidth_mAF3FE9E0AA4A2C617C8D93C1AB7B4B503B4B2F23,
	SpawnWalls__ctor_m2ACE656DD9575C51F590F5134AAD7C83DDD4C9AE,
	TimeManager_Start_m2A75447102599AF3F3B8EA72DC92DE6D464F7EB9,
	TimeManager_Update_mD4BBFB572CA604D72BF9459EFF493A41E565DBCD,
	TimeManager_SlowmoOn_m89CCE84A14F2E22AC83AB2510D8ADDF67404C00A,
	TimeManager_SlowmoOff_m1B0DDAD2F0D42D1485C8C6380F00B674F1CC452A,
	TimeManager__ctor_m076923E11728FA19B987632C634A7348083042B0,
	AttackEnemy_Start_m421C3198F9197C4EEE47F98FDB177DED5FF15E31,
	AttackEnemy_OnTriggerEnter2D_mF728A5F43B62CB40203067326DC9F71276D2287C,
	AttackEnemy_Update_m342DDBE033FCD71B07208FD7E6A3AB9731176D93,
	AttackEnemy__ctor_m1CBB48D58ABDD3F95784C16FE7AF01F1F2B35569,
	FlingPlayer_Start_mAF399A6DF68D6D74B27573CBCB87231C94B6F0A9,
	FlingPlayer_Update_m78E6246C21FCED4BF4E9F112DB8A66D690B6D78E,
	FlingPlayer_OnMouseDrag_m8459724A9AFEB1A9EF56145B6518CC9D35423AA4,
	FlingPlayer__ctor_m43F52FB38751F0E673D1310B53BBBEB1FD574B81,
	FollowPlayer_Start_m3D4944B71D74CF5E99F5032DC85E078F16A977C9,
	FollowPlayer_Update_m940D5A36DC60B8CF02F9DC5CE44AA288F3BEDB25,
	FollowPlayer_GetPlayerPos_mD5FAA6880228F0670BB9CEB47FC7D1A5D3B39361,
	FollowPlayer__ctor_m7A7391BDBACA4FB475491CC3B13D12271C5AE761,
	Jump_Start_m8C69302F327C33093CC36B4FC35DDA4B3A86BB72,
	Jump_Update_m512CDFCE74FAB60FDE33432DC017B27AEE0C9205,
	Jump_JumpUp_m0FEA16D9A743C640C8221B80F65FB5A1E6DA856D,
	Jump_OnCollisionEnter2D_mCD70688C0CEA4BF04E178F4B9205D94FB35D2814,
	Jump_OnTriggerEnter2D_m6E7D9AD6871E0E6C76698F4C9130DD2410FC4A4A,
	Jump_ManageJump_m8613688A808094B8B44E9C3C6F660911CA23DC9B,
	Jump_CheckSlide_m70E042E8426B5A035E6944C8D8A473B838BA06F0,
	Jump_GetForce_mF3B40B57D33B3F6AE1B70622E9691D3C9C2514A6,
	Jump_Flip_m90B92F4EFB7896CC876C65131663F860CDFF06A1,
	Jump__ctor_m5FF8975046C5683F92A333A924EC0C511D289C77,
	ProjectileLine_Start_m96297B46C10795F33927F76AD63EA4BF5B71BF39,
	ProjectileLine_Update_m77126BF61671D162B7E8D92DF70405D89144E58F,
	ProjectileLine__ctor_mB9204E54F927B899530E9F9B7F56C7FC66495BD4,
	SlowTeleport_Start_m640E7C2655E6EE849FC328E65EAEAF396C725663,
	SlowTeleport_Update_m9325AE8AAB047E3AA5EA673639244DF70A93C30F,
	SlowTeleport_FlingPlayer_mFB6F7CEA86FF82D78C8C46BF43EC8FD53C8380F9,
	SlowTeleport_OnCollisionEnter2D_mADE6D028C0023BCE3C63B911DE64935401F3832D,
	SlowTeleport_ShootProjectile_m816BE24A2348E762C2D5A2010989A53E24D070FE,
	SlowTeleport__ctor_mAC64D4D38B0971E20D05BFCE333936A6D0BC1BBA,
	Throw_Start_m871695977E7220FC694F26515E7FC9CCF447D1BD,
	Throw_Update_m9A454E6D38540707F9151746158C8EB801F38433,
	Throw_GetRayCollisionTransform_m0C7C447E43910734615304E2A556B7EF31E3ABB7,
	Throw_GetIsRayCasting_m05A0D8DD4B39F0B4C568FB74B2E9FAD04ED6580B,
	Throw_GetDirectionVector3_mCFFE3C106C378E7AE924A94808EDB2C70C465E76,
	Throw_SpawnExplosion_m2544543EBD637C9223DC6916DC79B5A0893557DC,
	Throw__ctor_mC3F9DD097E22416C9BF29185E8871A644B61AFB8,
	JumpArc_Awake_m222E9C334472D1B386FC58A3E44FE2E55A2ACA3A,
	JumpArc_RenderArc_m85A0B689C4C3D91AB492DF33C1670C51CEAC230A,
	JumpArc_CalculateArcArray_m7B7695A66BE38ECADAD28E3CC59AE65F3C20CDC7,
	JumpArc_CalculateArcPoint_m1A992A1C234DCFDF1F27A761D5637BDCF195CBF4,
	JumpArc__ctor_mB9E53E3464B30BAAC3DA628C55B37AF5C3279D5F,
	SwipeDetector_Update_m4C8655500371A9BCD7E24FCE10E0AA3C0F3360DE,
	SwipeDetector_checkSwipe_m0608B40F49318A12CC9430FC812E2D3A95806A61,
	SwipeDetector_verticalMove_m7A3D4EAC75D33896F5B95B5F2D7D558977F3FC9C,
	SwipeDetector_horizontalValMove_m7B260E04DA09077B058D2059BEA265E21789F803,
	SwipeDetector_OnSwipeUp_mBD165A4D1F5C2D78ACB2862A75F9258C5B6E1E88,
	SwipeDetector_OnSwipeDown_mB91943243BD3BC492A1D260851097E3AE1E5A3B1,
	SwipeDetector_OnSwipeLeft_mB581B3F20B9CF39C39274743B3AE3009E0C257AE,
	SwipeDetector_OnSwipeRight_m83B6E2F3BAC614FCF8662CC8BC087091351BFE9C,
	SwipeDetector__ctor_m1D2AE83BFB7097D9C4177195E817D2F06B7BDE70,
	ChatController_OnEnable_m168B1E78BFA288F42D4AE0A8F1424B8D68B07993,
	ChatController_OnDisable_m49C4A6501BCC216F924B3C37F243D1B5B54A69FF,
	ChatController_AddToChatOutput_m5E6DF0E37CB2E9FBBEACCB6EEE6452AB14BBE94C,
	ChatController__ctor_m2C7AAB67386BA2DC6742585988B914B3FAB30013,
	EnvMapAnimator_Awake_mDDD10A405C7152BEFA0ECEA0DCBD061B47C5802E,
	EnvMapAnimator_Start_m630E0BFAB4D647BC38B99A70F522EF80D25F3C71,
	EnvMapAnimator__ctor_m2A8770DA2E27EC52F6A6F704831B732638C76E84,
	TMP_DigitValidator_Validate_mEC7653F2228D8AA66F69D6B3539ED342AEE57691,
	TMP_DigitValidator__ctor_m4E1C1BEB96F76F2EE55E6FEC45D05F2AAC5DF325,
	TMP_PhoneNumberValidator_Validate_mBE0169BE01459AA37111A289EC422DDB0D5E3479,
	TMP_PhoneNumberValidator__ctor_mBF81DE006E19E49DAC3AFF685F8AF268A2FD0FFB,
	TMP_TextEventHandler_get_onCharacterSelection_mF70DBE3FF43B3D6E64053D37A2FADF802533E1FF,
	TMP_TextEventHandler_set_onCharacterSelection_mDEC285B6A284CC2EC9729E3DC16E81A182890D21,
	TMP_TextEventHandler_get_onSpriteSelection_m395603314F8CD073897DCAB5513270C6ADD94BF4,
	TMP_TextEventHandler_set_onSpriteSelection_m3D4E17778B0E3CC987A3EF74515E83CE39E3C094,
	TMP_TextEventHandler_get_onWordSelection_m415F4479934B1739658356B47DF4C2E90496AE2E,
	TMP_TextEventHandler_set_onWordSelection_m2EDD56E0024792DCE7F068228B4CA5A897808F4E,
	TMP_TextEventHandler_get_onLineSelection_m8E724700CC5DF1197B103F87156576A52F62AB2B,
	TMP_TextEventHandler_set_onLineSelection_m067512B3F057A225AF6DD251DD7E546FFF64CD93,
	TMP_TextEventHandler_get_onLinkSelection_m221527467F0606DD3561E0FB0D7678AA8329AD5D,
	TMP_TextEventHandler_set_onLinkSelection_mE3CE372F9FECD727FAB3B14D46439E0534EE8AA8,
	TMP_TextEventHandler_Awake_m67A37475531AC3EB75B43A640058AD52A605B8D9,
	TMP_TextEventHandler_LateUpdate_mB0ABBED08D5494DFFF85D9B56D4446D96DDBDDF5,
	TMP_TextEventHandler_OnPointerEnter_mE1CAF8C68C2356069FEB1AA1B53A56E24E5CE333,
	TMP_TextEventHandler_OnPointerExit_mB429546A32DCF6C8C64E703D07F9F1CDC697B009,
	TMP_TextEventHandler_SendOnCharacterSelection_mFBFC60A83107F26AA351246C10AB42CEB3A5A13C,
	TMP_TextEventHandler_SendOnSpriteSelection_mAB964EB5171AB07C48AC64E06C6BEC6A9C323E09,
	TMP_TextEventHandler_SendOnWordSelection_m3B76D7E79C65DB9D8E09EE834252C6E33C86D3AE,
	TMP_TextEventHandler_SendOnLineSelection_m9E9CAD5FA36FCA342A38EBD43E609A469E49F15F,
	TMP_TextEventHandler_SendOnLinkSelection_m1C55C664BB488E25AE746B99438EEDAE5B2B8DE8,
	TMP_TextEventHandler__ctor_m189A5951F5C0FA5FB1D0CFC461FAA1EBD7AED1AE,
	Benchmark01_Start_m20668FA5AD3945F18B5045459057C330E0B4D1F4,
	Benchmark01__ctor_m40EDCD3A3B6E8651A39C2220669A7689902C8B36,
	Benchmark01_UGUI_Start_mE8F5BC98EC6C16ECEBAD0FD78CD63E278B2DF215,
	Benchmark01_UGUI__ctor_m7F24B3D019827130B3D5F2D3E8C3FF23425F98BE,
	Benchmark02_Start_m3F848191079D3EF1E3B785830D74698325CA0BB7,
	Benchmark02__ctor_m3323414B806F63563E680918CC90EAF766A3D1AE,
	Benchmark03_Awake_m261B7F2CD25DC9E7144B2A2D167219A751AD9322,
	Benchmark03_Start_m649EFCC5BF0F199D102083583854DE87AC5EFBDD,
	Benchmark03__ctor_m90649FDE30CC915363C5B61AA19A7DE874FF18ED,
	Benchmark04_Start_mFDF88CB6DD4C5641A418DB08E105F9F62B897777,
	Benchmark04__ctor_mB07A2FD29BE4AFE284B47F2F610BDB7539F5A5DE,
	CameraController_Awake_m5E24687E6D82C0EBC4984D01B90769B8FD8C38B3,
	CameraController_Start_m257B81C6062A725785739AFE4C0DF84B8931EFB2,
	CameraController_LateUpdate_m9660F57BCF4F8C2154D19B6B40208466E414DAEB,
	CameraController_GetPlayerInput_m0B63EA708A63AF6852E099FD40F7C4E18793560A,
	CameraController__ctor_m8379776EEE21556D56845974B8C505AAD366B656,
	ObjectSpin_Awake_m2E5B2D7FA6FE2F3B5516BD829EDC5522187E6359,
	ObjectSpin_Update_mF8175B9157B852D3EC1BAF19D168858A8782BF0D,
	ObjectSpin__ctor_m1F951082C07A983F89779737E5A6071DD7BA67EB,
	ShaderPropAnimator_Awake_m44ACA60771EECABCB189FC78027D4ECD9726D31A,
	ShaderPropAnimator_Start_m57178B42FF0BB90ACA497EC1AA942CC3D4D54C32,
	ShaderPropAnimator_AnimateProperties_mB34C25C714FAEA4792465A981BAE46778C4F2409,
	ShaderPropAnimator__ctor_mDFAE260FD15CD3E704E86A25A57880A33B817BC6,
	SimpleScript_Start_m0238BE0F5DF0A15743D4D4B1B64C0A86505D1B76,
	SimpleScript_Update_mB92D578CAC3E0A0AFB055C7FEF47601C8822A0F8,
	SimpleScript__ctor_m0E919E8F3C12BAFF36B17E5692FCFA5AE602B2AA,
	SkewTextExample_Awake_mC70E117C1F921453D2F448CABA234FAA17A277ED,
	SkewTextExample_Start_mE2308836BF90B959ABE6064CD2DDDFAF224F0F4A,
	SkewTextExample_CopyAnimationCurve_m3CE7B666BEF4CFFE9EB110C8D57D9A5F6385720B,
	SkewTextExample_WarpText_m8B756AF1E1C065EEA486159E6C631A585B0C3461,
	SkewTextExample__ctor_m44F3CBD12A19C44A000D705FB4AB02E20432EC02,
	TMP_ExampleScript_01_Awake_mE2AAB8DF142D7BDB2C041CC7552A48745DBFDCFF,
	TMP_ExampleScript_01_Update_m1593A7650860FD2A478E10EA12A2601E918DD1EC,
	TMP_ExampleScript_01__ctor_m313B4F7ED747AD6979D8909858D0EF182C79BBC3,
	TMP_FrameRateCounter_Awake_m2540DCD733523BCBB1757724D8546AC3F1BEB16C,
	TMP_FrameRateCounter_Start_mEF10D80C419582C6944313FD100E2FD1C5AD1319,
	TMP_FrameRateCounter_Update_mF4798814F4F86850BB9248CA192EF5B65FA3A92B,
	TMP_FrameRateCounter_Set_FrameCounter_Position_m19C3C5E637FB3ED2B0869E7650A1C30A3302AF53,
	TMP_FrameRateCounter__ctor_m55E3726473BA4825AC0B7B7B7EA48D0C5CE8D646,
	TMP_TextEventCheck_OnEnable_mAFF9E7581B7B0C93A4A7D811C978FFCEC87B3784,
	TMP_TextEventCheck_OnDisable_m270DBB9CC93731104E851797D6BF55EACAE9158A,
	TMP_TextEventCheck_OnCharacterSelection_m4394BE3A0CA37D319AA10BE200A26CFD17EEAA8F,
	TMP_TextEventCheck_OnSpriteSelection_mCBF0B6754C607CA140C405FF5B681154AC861992,
	TMP_TextEventCheck_OnWordSelection_m4C290E23BBA708FE259A5F53921B7B98480E5B08,
	TMP_TextEventCheck_OnLineSelection_mF68BE3244AFD53E84E037B39443B5B3B50336FF5,
	TMP_TextEventCheck_OnLinkSelection_m23569DD32B2D3C4599B8D855AE89178C92BA25C7,
	TMP_TextEventCheck__ctor_m4B49D7387750432FA7A15A804ABD6793422E0632,
	TMP_TextInfoDebugTool__ctor_m2A2D1B42F97BD424B7C61813B83FE46C91575EFB,
	TMP_TextSelector_A_Awake_mEE6FCD85F7A6FDA4CC3B51173865E53F010AB0FF,
	TMP_TextSelector_A_LateUpdate_mF02F95A5D14806665404997F9ABAEE288A9879A0,
	TMP_TextSelector_A_OnPointerEnter_m6D15B2FC399C52D9706DD85C796BAE40CA8362D3,
	TMP_TextSelector_A_OnPointerExit_m080D05700B1D3251085331369FCD2A131D45F963,
	TMP_TextSelector_A__ctor_m6AB8BC86973365C192CF9EACA61459F2E0A5C88D,
	TMP_TextSelector_B_Awake_m87D2FCFCEDEE1FA82DEF77A867D2DE56C3AA0973,
	TMP_TextSelector_B_OnEnable_mD1C87684FD94190654176B38EE7DC960795F08E8,
	TMP_TextSelector_B_OnDisable_m429F83E18507E278CA9E9B5A2AE891087ED0D830,
	TMP_TextSelector_B_ON_TEXT_CHANGED_m91D0E180681C5566066C366487B94A05FB376B12,
	TMP_TextSelector_B_LateUpdate_m80F8343FAB19617468E94CD2B35636DBB9AC2064,
	TMP_TextSelector_B_OnPointerEnter_m9A938ED5B0D70633B9099F5C1B213FD50380116D,
	TMP_TextSelector_B_OnPointerExit_mD481099225DF156CA7CA904AA1C81AF26A974D28,
	TMP_TextSelector_B_OnPointerClick_mE4A6507E55DD05BBC99F81212CF26F2F11179FBE,
	TMP_TextSelector_B_OnPointerUp_m5E52652A02A561F2E8AB7F0C00E280C76A090F74,
	TMP_TextSelector_B_RestoreCachedVertexAttributes_m01B9A1E989D57BE8837E99C4359BCB6DD847CB35,
	TMP_TextSelector_B__ctor_mC42D87810C72234A3360C0965CC1B7F45AB4EE26,
	TMP_UiFrameRateCounter_Awake_mFAF9F495C66394DC36E9C6BC96C9E880C4A3B0A9,
	TMP_UiFrameRateCounter_Start_mC4A3331333B1DFA82B184A0701FCE26395B8D301,
	TMP_UiFrameRateCounter_Update_mCA98BB5342C50F9CE247A858E1942410537E0DAF,
	TMP_UiFrameRateCounter_Set_FrameCounter_Position_mDD0EAB08CE58340555A6654BDD5BEE015E6C6ACE,
	TMP_UiFrameRateCounter__ctor_mE3DC8B24D2819C55B66AEAEB9C9B93AFDA9C4573,
	TMPro_InstructionOverlay_Awake_m951573D9BF0200A4C4605E043E92BBD2EB33BA7C,
	TMPro_InstructionOverlay_Set_FrameCounter_Position_m39D0BB71DCCB67271B96F8A9082D7638E4E1A694,
	TMPro_InstructionOverlay__ctor_m103EF0B8818B248077CB97909BA806477DCEB8A5,
	TeleType_Awake_m3501F8FA1B762D22972B9B2BAC1E20561088882B,
	TeleType_Start_m2A3F19E0F9F2C72D48DDF5A4208AF18AE7769E69,
	TeleType__ctor_m8B985E4023A01F963A74E0FE5E8758B979FB3C3A,
	TextConsoleSimulator_Awake_m8B1E7254BFB2D0C7D5A803AEFAFCD1B5327F79AD,
	TextConsoleSimulator_Start_m85E6334AFE22350A5715F9E45843FD865EF60C9D,
	TextConsoleSimulator_OnEnable_mB6F523D582FE4789A5B95C086AA7C168A5DD5AF7,
	TextConsoleSimulator_OnDisable_m1EF25B5345586DD26BB8615624358EFB21B485DB,
	TextConsoleSimulator_ON_TEXT_CHANGED_mD4A85AE6FE4CD3AFF790859DEFB7E4AAF9304AE5,
	TextConsoleSimulator_RevealCharacters_m7BF445A3B7B6A259450593775D10DE0D4BD901AD,
	TextConsoleSimulator_RevealWords_mD7D62A1D326528506154148148166B9196A9B903,
	TextConsoleSimulator__ctor_mA40DB76E1D63318E646CF2AE921084D0FDF4C3CA,
	TextMeshProFloatingText_Awake_mB40A823A322B9EFD776230600A131BAE996580C3,
	TextMeshProFloatingText_Start_mBFC04A0247294E62BD58CB3AC83F85AE61C3FB4F,
	TextMeshProFloatingText_DisplayTextMeshProFloatingText_mB0DEABA5CC4A6B556D76ED30A3CF08E7F0B42AFC,
	TextMeshProFloatingText_DisplayTextMeshFloatingText_m8AB7E0B8313124F67FEDE857012B9E56397147E2,
	TextMeshProFloatingText__ctor_m610430DD6E4FD84EBF6C499FB4415B5000109627,
	TextMeshSpawner_Awake_m31920E8DD53AD295AAD8B259391A28E1A57862ED,
	TextMeshSpawner_Start_m189316ED7CD62EFD10B40A23E4072C2CEB5A516B,
	TextMeshSpawner__ctor_m3995DDE2D7E7CBF8087A3B61242F35E09AC94668,
	VertexColorCycler_Awake_m19D37F0DDC4E1D64EA67101852383862DCAAED1E,
	VertexColorCycler_Start_m2CFBFF7F45A76D16C29B570E3468AFEEC2D1C443,
	VertexColorCycler_AnimateVertexColors_mDB7F380B912148C792F857E42BFB042C6A267260,
	VertexColorCycler__ctor_mBAF42937750A7A22DB5BF09823489FDE25375816,
	VertexJitter_Awake_m32ACAC43EDE2595CD4FFB6802D58DEBC0F65B52C,
	VertexJitter_OnEnable_m63CC97434F60690EE234794C9C2AD3B25EC69486,
	VertexJitter_OnDisable_mE5E221B893D3E53F3A9516082E2C4A9BE174DDF5,
	VertexJitter_Start_mC977D71742279824F9DD719DD1F5CB10269BC531,
	VertexJitter_ON_TEXT_CHANGED_mE5AE5146D67DA15512283617C41F194AEDD6A4AC,
	VertexJitter_AnimateVertexColors_m6B361C3B93A2CC219B98AACFC59288432EE6AC1E,
	VertexJitter__ctor_mD5B5049BB3640662DD69EB1E14789891E8B2E720,
	VertexShakeA_Awake_m6075DA429A021C8CB3F6BE9A8B9C64127288CD19,
	VertexShakeA_OnEnable_m39AA373478F796E7C66763AA163D35811721F5CD,
	VertexShakeA_OnDisable_mA087E96D94CB8213D28D9A601BC25ED784BB8421,
	VertexShakeA_Start_mDED2AEA47D2E2EF346DE85112420F6E95D9A3CFD,
	VertexShakeA_ON_TEXT_CHANGED_m0B59A798E6B193FE68F6A20E7004B223D5A2993E,
	VertexShakeA_AnimateVertexColors_m238AB73BE06E33312281577CC896CEB7BB175245,
	VertexShakeA__ctor_m41CBBA607D90D45E21C98CCDF347AE27FB50392F,
	VertexShakeB_Awake_m7CBA45BF5135680A823536A18325ECA621EF7A1A,
	VertexShakeB_OnEnable_m39EBB983A4FFFF6DD1C7923C8C23FF09CFF2F6E2,
	VertexShakeB_OnDisable_m8E3858FC1C976F311628466C411675E352F134A5,
	VertexShakeB_Start_m666FA35D389B109F01A5FC229D32664D880ADE09,
	VertexShakeB_ON_TEXT_CHANGED_mF4858E4385EAA74F5A3008C50B8AD180FCBC8517,
	VertexShakeB_AnimateVertexColors_m076A6C9D71EE8B5A54CD1CEDCA5AB15160112DD3,
	VertexShakeB__ctor_m5CAAD9DFA7B4D9C561473D53CA9E3D8B78AE5606,
	VertexZoom_Awake_mB18FF89A84E2AA75BDD486A698955A58E47686EE,
	VertexZoom_OnEnable_mCD27B81253963B3D0CD2F6BA7B161F0DFDC08114,
	VertexZoom_OnDisable_mB32AD5B7DFF20E682BA4FC82B30C87707DD3BA10,
	VertexZoom_Start_m6C64C692D81F64FB7F3244C3F0E37799B159A0DE,
	VertexZoom_ON_TEXT_CHANGED_mC08504F9622CC709271C09EDB7A0DF1A35E45768,
	VertexZoom_AnimateVertexColors_mEC9842E0BC31D9D4E66FD30E6467D5A9A19034D6,
	VertexZoom__ctor_mA4381FC291E17D67EA3C2292EAB8D3C959ADEA79,
	WarpTextExample_Awake_mF6785C4DC8316E573F20A8356393946F6ABFC88C,
	WarpTextExample_Start_m8E7AC9FF62E37EAB89F93FD0C1457555F6DCB086,
	WarpTextExample_CopyAnimationCurve_m2C738EA265E2B35868110EE1D8FCBD4F1D61C038,
	WarpTextExample_WarpText_m27664A46276B3D615ECB12315F5E77C4F2AF29EE,
	WarpTextExample__ctor_mF5BA8D140958AD2B5D2C8C5DE937E21A5D283C9F,
	U3CWaitTimeU3Ed__13__ctor_m6EFDE64DF68EFB7586206C7635EB720CB01D2D92,
	U3CWaitTimeU3Ed__13_System_IDisposable_Dispose_m4DBC5FE487C79A6BBF58FB9044A452A98600F021,
	U3CWaitTimeU3Ed__13_MoveNext_m5A2CDBB6E28874A27AC03DDF103532FC60FB2ADB,
	U3CWaitTimeU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m495CC8A67DA245523E36EB3D988A2BF34215BF8B,
	U3CWaitTimeU3Ed__13_System_Collections_IEnumerator_Reset_m065691C9787C63C3F148E63048B889D8B6B35B81,
	U3CWaitTimeU3Ed__13_System_Collections_IEnumerator_get_Current_mD135528D1A68F768E4BAA645C9888F32BC127614,
	U3CAttackU3Ed__14__ctor_m597078FFC38775D2D521D33A26B38DD3476D7C7E,
	U3CAttackU3Ed__14_System_IDisposable_Dispose_m6ADB88D43C55E299190B759F31E727643AB5B2C5,
	U3CAttackU3Ed__14_MoveNext_m684A3923E528084F17BBE7D616F61F6F6849F970,
	U3CAttackU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1ABB63BCCABEE7C5FC89262BC56129B58ECE9F67,
	U3CAttackU3Ed__14_System_Collections_IEnumerator_Reset_mA9C9322BFAC275CE0290D5E1C585025DDDAD133C,
	U3CAttackU3Ed__14_System_Collections_IEnumerator_get_Current_mB1EFCD0847F10DEBF498263AD815E52E608329F0,
	U3CPrepAttackU3Ed__10__ctor_mF8E86FA4F71DB2E36A06AF07B89C94D8D9337EC6,
	U3CPrepAttackU3Ed__10_System_IDisposable_Dispose_m2A1B1E10ECF2ECEEDBF2B56BE9FECAF3A28C4400,
	U3CPrepAttackU3Ed__10_MoveNext_mFB3180F88E515A8E1EBD8B0854973E7B22D8E04E,
	U3CPrepAttackU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD2B2DBD39108F4C02CFCE5D1B31AEB99A20E32D0,
	U3CPrepAttackU3Ed__10_System_Collections_IEnumerator_Reset_mDEF48DE5D11D5F0C90AC61864907CB5B546F1693,
	U3CPrepAttackU3Ed__10_System_Collections_IEnumerator_get_Current_m842ADB5A94304F03AAE5839A02E3EDF3EE409BEF,
	U3CChargeLightningU3Ed__11__ctor_mA7C9E0F92BE1ACF2CC8F2B76F7CC47F373B1CF54,
	U3CChargeLightningU3Ed__11_System_IDisposable_Dispose_m4018E3A40DE9EA8B4CE337084F0A05EB3A235C0B,
	U3CChargeLightningU3Ed__11_MoveNext_m8B2E0F1C9AA7E60882698B0216EF21A1CDF2DCFF,
	U3CChargeLightningU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE17D3C49DDB521ABB4D2F7E6DEE365EF69914BD3,
	U3CChargeLightningU3Ed__11_System_Collections_IEnumerator_Reset_mEAE53D5A53E78319AB96F82E43A713833E4870AC,
	U3CChargeLightningU3Ed__11_System_Collections_IEnumerator_get_Current_m82FE2A1B29C92F22DFF285A215C5F8FE6663B95C,
	U3CShootLightningU3Ed__12__ctor_m1E4B764759A0891FDE7A4ADAEEAB5EE9FB419988,
	U3CShootLightningU3Ed__12_System_IDisposable_Dispose_m9A8746EEE5535B1AC853376C650FA5DBCCAF2299,
	U3CShootLightningU3Ed__12_MoveNext_m581BC350AEB3D2B5309B46517614DA9DA8C0CFD0,
	U3CShootLightningU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8C27075970AD8049785C100219F3C17394558A0D,
	U3CShootLightningU3Ed__12_System_Collections_IEnumerator_Reset_m7A3E578C7E6E76D7B3CAA3A8BA168CB10A54D370,
	U3CShootLightningU3Ed__12_System_Collections_IEnumerator_get_Current_m547FBF0A599B09B55BF7DA55B0568971486DC599,
	U3CAttackU3Ed__9__ctor_m7E34F89D67984F9CC37139A0B26DEFE2D64D2C04,
	U3CAttackU3Ed__9_System_IDisposable_Dispose_mAF10B9C8CA1338A3F47A71DEBD827480EA2BE6E3,
	U3CAttackU3Ed__9_MoveNext_mB2B3EFADFFBE1DE4847D2A9D8EAE7E36D487C3BC,
	U3CAttackU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC873743E8FC808ECB53207C11F410DC17FFAAA16,
	U3CAttackU3Ed__9_System_Collections_IEnumerator_Reset_m1B3A764A454A5E0A4D8D42B725913EC7470ADFA2,
	U3CAttackU3Ed__9_System_Collections_IEnumerator_get_Current_mF71DDCF7CFABFD68D966945737F3B8CA020C0C59,
	U3CSpawnEnemyEnergyU3Ed__22__ctor_m84319FD9750DA290E45477A8EB03283397838120,
	U3CSpawnEnemyEnergyU3Ed__22_System_IDisposable_Dispose_mD127393FDE121B0099B7115870F8DA7410852A38,
	U3CSpawnEnemyEnergyU3Ed__22_MoveNext_m61E560DB388866B235DB1C30064DA8C12CA86FA9,
	U3CSpawnEnemyEnergyU3Ed__22_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9211BB9B5E1DF842A4E7E2317DC718E2E471FC5B,
	U3CSpawnEnemyEnergyU3Ed__22_System_Collections_IEnumerator_Reset_mF38E04919E617D9C9CB9F6081439DC90809E62D7,
	U3CSpawnEnemyEnergyU3Ed__22_System_Collections_IEnumerator_get_Current_mD6F7429F60F200CFD83ABF44DE46387831711119,
	U3CFlingPlayerU3Ed__9__ctor_m1F684303CF23A14485FEF9DAE6C899DE9EAA9334,
	U3CFlingPlayerU3Ed__9_System_IDisposable_Dispose_m1AE5BBBBFF6779325FE2D0AF8D00E1E01B2D8773,
	U3CFlingPlayerU3Ed__9_MoveNext_m73F8B6B2B287F4BE734BDD18623D28EE714ABEF2,
	U3CFlingPlayerU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF2F38480BF51C400ECCB1C92F35A8CC69201F09D,
	U3CFlingPlayerU3Ed__9_System_Collections_IEnumerator_Reset_m648DB59F2B7D18408C40B36E0F4CD2466AF061A0,
	U3CFlingPlayerU3Ed__9_System_Collections_IEnumerator_get_Current_mD4EB76448507C76A4F7460823744D0A6748CFF61,
	U3CSpawnExplosionU3Ed__18__ctor_mD1EAA2235FFF5719B585F9F4BB68343039DEC93D,
	U3CSpawnExplosionU3Ed__18_System_IDisposable_Dispose_m33D8FFA5E462E3F5205EF9CDB3ED53E92E5323C3,
	U3CSpawnExplosionU3Ed__18_MoveNext_mA77EAA1B2B6938EC1FEEF10766AB979DDB7650CB,
	U3CSpawnExplosionU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0865A7FCAE3068F3A3FB6BF53B0878205BAB578E,
	U3CSpawnExplosionU3Ed__18_System_Collections_IEnumerator_Reset_m890302255E5B45BF27F6F60A165CDC72C79FF2EC,
	U3CSpawnExplosionU3Ed__18_System_Collections_IEnumerator_get_Current_m46F528D3EA63FE821E6B07A6C43103471F68AED1,
	U3CStartU3Ed__4__ctor_m8B0264798939C569742263D32E0054DBAB9AE6FF,
	U3CStartU3Ed__4_System_IDisposable_Dispose_m3EFE2ADAD412045F666CFA1C8C9FF53AF92CBD75,
	U3CStartU3Ed__4_MoveNext_m84F94A5CD6012300AC80698CDCA870A0A146E226,
	U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m129CB3E5CAFFA1D19D4988182EEF116F2086A637,
	U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_m345E255900454CC505A8AAE3BF6AEF3C06467EAB,
	U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_m5920F51DCC2DC7C8BC98EE95D6CD4D7784997272,
	CharacterSelectionEvent__ctor_mE2C306B8090F90261252C94D26AB5085580B11D5,
	SpriteSelectionEvent__ctor_m9D9F101CB717ACD5449336DFFF70F86AE32BB6EC,
	WordSelectionEvent__ctor_mFD7F2937426D4AA1A8CBB13F62C3CC1D2061AD1E,
	LineSelectionEvent__ctor_mA23AFEC8E11183CF472044FA72B07AD28ED6E675,
	LinkSelectionEvent__ctor_m02CC491DBE4B2FF05A8FD4285813215ED3D323E5,
	U3CStartU3Ed__10__ctor_m328932E4B6124311CD738F2F84F69BC149209129,
	U3CStartU3Ed__10_System_IDisposable_Dispose_m209F531CE6ED7F07649497DD15817C1D7C1880A1,
	U3CStartU3Ed__10_MoveNext_mD927C85D41034011055A7CA3AFFAF4E10464F65D,
	U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m18BA91C8A20CBD6976D52E335563D9B42C1AE9A8,
	U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_m5A67B5BDE759A157229E6CF24E653B79B2AC0200,
	U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_mA521C1EFA357A9F4F4CAA68A4D0B85468764323C,
	U3CStartU3Ed__10__ctor_m9A8C7C0644996520AD443A4F7CA527BF05C54C3C,
	U3CStartU3Ed__10_System_IDisposable_Dispose_m94D1420C08F57F2901E2499D36778BB8F1C76932,
	U3CStartU3Ed__10_MoveNext_m31AF957FFAEEED4BE0F39A1185C6112C4EB6F7AA,
	U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m39B535B222104759319A54A6D7E2E81482A1F71E,
	U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_mC2B435140D045B6A20FB105E0E2CBD625218CA74,
	U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_m9A98CCB7604AAD93919CAE48955C6A6CB8C38790,
	U3CAnimatePropertiesU3Ed__6__ctor_mB5F6ED6FCDA5BEAD56E22B64283D7A4D7F7EAE71,
	U3CAnimatePropertiesU3Ed__6_System_IDisposable_Dispose_m29AAE9560CA4EEB4A548A68ACA085EC9E4CB8EA5,
	U3CAnimatePropertiesU3Ed__6_MoveNext_m4F2D37B672E95820F49489611196CDE334736157,
	U3CAnimatePropertiesU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m21D2B0A0B0CADF520D05FE4948F1DE94CF119630,
	U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_Reset_m1E942C0FD32005FDBB182CF646FD2312BA273BC7,
	U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_get_Current_mFC3602799F1D07BB002093DFB879FC759384FDD3,
	U3CWarpTextU3Ed__7__ctor_mA03118DB0FD3BF160500E127D1FACDAF45313047,
	U3CWarpTextU3Ed__7_System_IDisposable_Dispose_m9D7F6A90DA911D77EE72A2824FF9690CED05FBC8,
	U3CWarpTextU3Ed__7_MoveNext_m7FE0DD003507BAD92E35CC5DACE5D043ADD766ED,
	U3CWarpTextU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m349F81ECD49FF12E4009E2E56DB81974D68C6DAD,
	U3CWarpTextU3Ed__7_System_Collections_IEnumerator_Reset_m3122CE754238FB7815F7ABE8E7DFAF3AB7B03278,
	U3CWarpTextU3Ed__7_System_Collections_IEnumerator_get_Current_m79BF250C4ADC29ACF11370E2B5BD4FFD78709565,
	U3CStartU3Ed__4__ctor_m8231909D78A27061165C450481E233339F300046,
	U3CStartU3Ed__4_System_IDisposable_Dispose_m6886DB5D83361607B72BBCCB7D484B9C0BFE1981,
	U3CStartU3Ed__4_MoveNext_m1CD1306C9E074D3F941AC906A48D3CA97C148774,
	U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE50AC5FA9F27773C51DD3E4188A748BA0A513F8A,
	U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_m5D6EE5C4B2C20A433129D8BFD13DFC82681346A2,
	U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_m1FA5600514131056D8198F8442F37A4A22A9F065,
	U3CRevealCharactersU3Ed__7__ctor_m48510711FC78DFEA9CF4603E1E75F4DF7C5F1489,
	U3CRevealCharactersU3Ed__7_System_IDisposable_Dispose_m5B88486625A74566DF3FC7BFB4CE327A58C57ED4,
	U3CRevealCharactersU3Ed__7_MoveNext_mE45076151810A7C1F83802B7754DE92E812EABAB,
	U3CRevealCharactersU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9C330834C7113C8468CC1A09417B7C521CAE833B,
	U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_Reset_m25B719FFD0CAB1DFF2853FF47A4EE2032176E287,
	U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_get_Current_m9A540E1B18E93F749F4BFD4C8597AEC9F2C199F7,
	U3CRevealWordsU3Ed__8__ctor_mDF41BA5FE3D53FEC3CB8214FCA7853A1142DE70C,
	U3CRevealWordsU3Ed__8_System_IDisposable_Dispose_m46827499CD3657AF468926B6302D2340ED975965,
	U3CRevealWordsU3Ed__8_MoveNext_mE0CB1189CFAD7F7B3E74438A4528D9BFAABB48DE,
	U3CRevealWordsU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4A3E941DF6C67BC9ACEFEAA09D11167B3F3A38EC,
	U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_Reset_m967B427886233CDB5DFFEA323F02A89CE9330CC8,
	U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_get_Current_m0C1B2941BEC04593993127F6D9DCDBA6FAE7CC20,
	U3CDisplayTextMeshProFloatingTextU3Ed__12__ctor_m13B7271203EDC80E649C1CE40F09A93BDA2633DF,
	U3CDisplayTextMeshProFloatingTextU3Ed__12_System_IDisposable_Dispose_m3D1611AA38746EF0827F5260DADCC361DD56DF0C,
	U3CDisplayTextMeshProFloatingTextU3Ed__12_MoveNext_m03111B7039F928512A7A53F8DA9C04671AA8D7EE,
	U3CDisplayTextMeshProFloatingTextU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m55B08E77035437C2B75332F29214C33214417414,
	U3CDisplayTextMeshProFloatingTextU3Ed__12_System_Collections_IEnumerator_Reset_m6737F2D695AB295240F15C5B0B4F24A59106BFDA,
	U3CDisplayTextMeshProFloatingTextU3Ed__12_System_Collections_IEnumerator_get_Current_m6BD4D2442BDDDFB6859CFE646182580A0A1E130A,
	U3CDisplayTextMeshFloatingTextU3Ed__13__ctor_m7E4C3B87E56A7B23D725D653E52ADE02554EAE3E,
	U3CDisplayTextMeshFloatingTextU3Ed__13_System_IDisposable_Dispose_m485A7C4CF3496858A72CBA647B29BC610F39FE39,
	U3CDisplayTextMeshFloatingTextU3Ed__13_MoveNext_m6A3C88B1149D12B58E6E580BC04622F553ED1424,
	U3CDisplayTextMeshFloatingTextU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2EE54B2AC8AAE44BACF8EE8954A6D824045CFC55,
	U3CDisplayTextMeshFloatingTextU3Ed__13_System_Collections_IEnumerator_Reset_m8524C9700DEF2DE7A28BBFDB938FE159985E86EE,
	U3CDisplayTextMeshFloatingTextU3Ed__13_System_Collections_IEnumerator_get_Current_m2293C6A327D4D1CCC1ACBC90DBE00DC1C6F39EBE,
	U3CAnimateVertexColorsU3Ed__3__ctor_m3D7543ED636AFCD2C59E834668568DB2A4005F6A,
	U3CAnimateVertexColorsU3Ed__3_System_IDisposable_Dispose_m3F7092E831D4CFDACC5B6254958DFEC2D313D0EE,
	U3CAnimateVertexColorsU3Ed__3_MoveNext_mC0D42DAE0A614F2B91AF1F9A2F8C0AF471CA0AE4,
	U3CAnimateVertexColorsU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6597EE639379454CADA3823A1B955FEFBAF894BD,
	U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_Reset_m9943586181F26EEE58A42138CE0489DDF07EA359,
	U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_get_Current_m2D4E4AA5EEB4F07F283E61018685199A4C2D56BD,
	U3CAnimateVertexColorsU3Ed__11__ctor_mC74A801C40038DA74D856FACFBAD12F3BC3E11E7,
	U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_m07C61223FC885322B6066E81CB130879661D5A72,
	U3CAnimateVertexColorsU3Ed__11_MoveNext_m04CD6FB321DE3AD8D5890766C1F2CAAE4112EDF2,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m35F5F559A34D3F9BE1E5DD636FEAF517164A6B07,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_m325AE901312C158848B79B302EBA7BE847C93D49,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_mA688DA41E2C04FF9774F607794C114057FA055C6,
	U3CAnimateVertexColorsU3Ed__11__ctor_m89953000A887F8C0931B0E98B484FBAAC37748C5,
	U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_m9AFBB2A87D38A1358F9EB09D617075D72DEED19B,
	U3CAnimateVertexColorsU3Ed__11_MoveNext_m9F607EF7DBDFFC4FB2307B0EC4C7F33EEE63BBE8,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF47F7A3AB51F9BC1B8F74E10FD82B29C1B223DCD,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_m99B988724C4F53F7F8334C5F633C8B1603185ADD,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_mE9127628FC1726149DA7C8FE95A7D4CFB1EE1655,
	U3CAnimateVertexColorsU3Ed__10__ctor_mBA04B89258FA2EF09266E1766AB0B815E521897A,
	U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_mE001B767DE85B4B3B86A0C080B9FC00381340A1C,
	U3CAnimateVertexColorsU3Ed__10_MoveNext_m611487BEE2BB82A9BFF5EA2157BDCA610F87876D,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6A54A1BF63433F860822B43ABA9FAC6A4124409C,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_m48F09B740CBEEC27459498932572D2869A1A4CBE,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_mA915AC55E6DEE343235545FC1FE6F6CA5611DF3C,
	U3CU3Ec__DisplayClass10_0__ctor_m1C2F2204ADD6E4BA14E14CF255F520B7E2464941,
	U3CU3Ec__DisplayClass10_0_U3CAnimateVertexColorsU3Eb__0_m673C7031DB1882DEEFB53F179E3C2FB13FB6CA5A,
	U3CAnimateVertexColorsU3Ed__10__ctor_m2F5D29C1CA797C0BCEC16C8B5D96D1CF5B07F6F3,
	U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_m1A43A8EA2FB689EE2B39D8A624580594374905B9,
	U3CAnimateVertexColorsU3Ed__10_MoveNext_mCA826F12F72BDBB79F9B50DC9CBC6E7F80B2110F,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFF0E754E9557C03F892EFA19C0307AECD6BA8C4D,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_m002A7C6C8AE61BE6CD6FD0B2173C75DBF47BCC56,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_mE91B03C99FCCBC8ED4E37649C5364E83D047B053,
	U3CWarpTextU3Ed__8__ctor_m845C9410F3856EF25585F59C425200EEFCEFB3C0,
	U3CWarpTextU3Ed__8_System_IDisposable_Dispose_m63AC2AE8BC0FCF89812A33CAF150E9D1B56BAE6A,
	U3CWarpTextU3Ed__8_MoveNext_m98D3E999A69E233C7AD5F357A0D0623D731DCDAA,
	U3CWarpTextU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3971C0D86C5903812972245A6F872D101ACB5189,
	U3CWarpTextU3Ed__8_System_Collections_IEnumerator_Reset_mF17827612AC7C91DE879114D1D6428450B9504D0,
	U3CWarpTextU3Ed__8_System_Collections_IEnumerator_get_Current_m98A13A358D4E71D9612F68731A7AE11A3DD080DE,
};
static const int32_t s_InvokerIndices[499] = 
{
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	14,
	1791,
	26,
	23,
	23,
	23,
	23,
	1747,
	14,
	14,
	26,
	23,
	23,
	26,
	14,
	23,
	23,
	26,
	23,
	23,
	23,
	1121,
	23,
	23,
	23,
	23,
	26,
	23,
	26,
	23,
	23,
	23,
	23,
	23,
	1779,
	23,
	23,
	23,
	26,
	23,
	23,
	23,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	114,
	23,
	1358,
	2370,
	23,
	23,
	23,
	114,
	114,
	23,
	23,
	23,
	26,
	14,
	683,
	683,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	10,
	10,
	295,
	683,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	14,
	23,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	23,
	23,
	23,
	295,
	295,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	683,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	1289,
	23,
	23,
	23,
	23,
	26,
	26,
	295,
	23,
	683,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	14,
	26,
	23,
	23,
	23,
	23,
	1153,
	114,
	1158,
	2371,
	23,
	23,
	295,
	1747,
	1362,
	23,
	23,
	23,
	683,
	683,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	23,
	23,
	14,
	23,
	2081,
	23,
	2081,
	23,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	23,
	23,
	26,
	26,
	451,
	451,
	35,
	35,
	604,
	23,
	14,
	23,
	14,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	14,
	23,
	23,
	23,
	23,
	23,
	23,
	28,
	14,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	32,
	23,
	23,
	23,
	451,
	451,
	35,
	35,
	604,
	23,
	23,
	23,
	23,
	26,
	26,
	23,
	23,
	23,
	23,
	26,
	23,
	26,
	26,
	26,
	26,
	32,
	23,
	23,
	23,
	23,
	32,
	23,
	23,
	32,
	23,
	23,
	14,
	23,
	23,
	23,
	23,
	23,
	26,
	28,
	28,
	23,
	23,
	23,
	14,
	14,
	23,
	23,
	23,
	23,
	23,
	23,
	14,
	23,
	23,
	23,
	23,
	23,
	26,
	14,
	23,
	23,
	23,
	23,
	23,
	26,
	14,
	23,
	23,
	23,
	23,
	23,
	26,
	14,
	23,
	23,
	23,
	23,
	23,
	26,
	14,
	23,
	23,
	23,
	28,
	14,
	23,
	32,
	23,
	114,
	14,
	23,
	14,
	32,
	23,
	114,
	14,
	23,
	14,
	32,
	23,
	114,
	14,
	23,
	14,
	32,
	23,
	114,
	14,
	23,
	14,
	32,
	23,
	114,
	14,
	23,
	14,
	32,
	23,
	114,
	14,
	23,
	14,
	32,
	23,
	114,
	14,
	23,
	14,
	32,
	23,
	114,
	14,
	23,
	14,
	32,
	23,
	114,
	14,
	23,
	14,
	32,
	23,
	114,
	14,
	23,
	14,
	23,
	23,
	23,
	23,
	23,
	32,
	23,
	114,
	14,
	23,
	14,
	32,
	23,
	114,
	14,
	23,
	14,
	32,
	23,
	114,
	14,
	23,
	14,
	32,
	23,
	114,
	14,
	23,
	14,
	32,
	23,
	114,
	14,
	23,
	14,
	32,
	23,
	114,
	14,
	23,
	14,
	32,
	23,
	114,
	14,
	23,
	14,
	32,
	23,
	114,
	14,
	23,
	14,
	32,
	23,
	114,
	14,
	23,
	14,
	32,
	23,
	114,
	14,
	23,
	14,
	32,
	23,
	114,
	14,
	23,
	14,
	32,
	23,
	114,
	14,
	23,
	14,
	32,
	23,
	114,
	14,
	23,
	14,
	23,
	56,
	32,
	23,
	114,
	14,
	23,
	14,
	32,
	23,
	114,
	14,
	23,
	14,
};
extern const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule = 
{
	"Assembly-CSharp.dll",
	499,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
