﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Exception System.Linq.Error::ArgumentNull(System.String)
extern void Error_ArgumentNull_mCA126ED8F4F3B343A70E201C44B3A509690F1EA7 ();
// 0x00000002 System.Exception System.Linq.Error::ArgumentOutOfRange(System.String)
extern void Error_ArgumentOutOfRange_mACFCB068F4E0C4EEF9E6EDDD59E798901C32C6C9 ();
// 0x00000003 System.Exception System.Linq.Error::MoreThanOneMatch()
extern void Error_MoreThanOneMatch_m85C3617F782E9F2333FC1FDF42821BE069F24623 ();
// 0x00000004 System.Exception System.Linq.Error::NoElements()
extern void Error_NoElements_m17188AC2CF25EB359A4E1DDE9518A98598791136 ();
// 0x00000005 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Where(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000006 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Select(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TResult>)
// 0x00000007 System.Func`2<TSource,System.Boolean> System.Linq.Enumerable::CombinePredicates(System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,System.Boolean>)
// 0x00000008 System.Func`2<TSource,TResult> System.Linq.Enumerable::CombineSelectors(System.Func`2<TSource,TMiddle>,System.Func`2<TMiddle,TResult>)
// 0x00000009 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::SelectMany(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Collections.Generic.IEnumerable`1<TResult>>)
// 0x0000000A System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::SelectManyIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Collections.Generic.IEnumerable`1<TResult>>)
// 0x0000000B System.Linq.IOrderedEnumerable`1<TSource> System.Linq.Enumerable::OrderBy(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>)
// 0x0000000C System.Linq.IOrderedEnumerable`1<TSource> System.Linq.Enumerable::ThenBy(System.Linq.IOrderedEnumerable`1<TSource>,System.Func`2<TSource,TKey>)
// 0x0000000D System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Distinct(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000000E System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::DistinctIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEqualityComparer`1<TSource>)
// 0x0000000F TSource[] System.Linq.Enumerable::ToArray(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000010 System.Collections.Generic.List`1<TSource> System.Linq.Enumerable::ToList(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000011 TSource System.Linq.Enumerable::FirstOrDefault(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000012 TSource System.Linq.Enumerable::Last(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000013 TSource System.Linq.Enumerable::SingleOrDefault(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000014 TSource System.Linq.Enumerable::ElementAt(System.Collections.Generic.IEnumerable`1<TSource>,System.Int32)
// 0x00000015 System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000016 System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000017 System.Int32 System.Linq.Enumerable::Count(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000018 System.Int32 System.Linq.Enumerable::Count(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000019 System.Boolean System.Linq.Enumerable::Contains(System.Collections.Generic.IEnumerable`1<TSource>,TSource)
// 0x0000001A System.Boolean System.Linq.Enumerable::Contains(System.Collections.Generic.IEnumerable`1<TSource>,TSource,System.Collections.Generic.IEqualityComparer`1<TSource>)
// 0x0000001B System.Void System.Linq.Enumerable_Iterator`1::.ctor()
// 0x0000001C TSource System.Linq.Enumerable_Iterator`1::get_Current()
// 0x0000001D System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_Iterator`1::Clone()
// 0x0000001E System.Void System.Linq.Enumerable_Iterator`1::Dispose()
// 0x0000001F System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_Iterator`1::GetEnumerator()
// 0x00000020 System.Boolean System.Linq.Enumerable_Iterator`1::MoveNext()
// 0x00000021 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_Iterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000022 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_Iterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000023 System.Object System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerator.get_Current()
// 0x00000024 System.Collections.IEnumerator System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000025 System.Void System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerator.Reset()
// 0x00000026 System.Void System.Linq.Enumerable_WhereEnumerableIterator`1::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000027 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereEnumerableIterator`1::Clone()
// 0x00000028 System.Void System.Linq.Enumerable_WhereEnumerableIterator`1::Dispose()
// 0x00000029 System.Boolean System.Linq.Enumerable_WhereEnumerableIterator`1::MoveNext()
// 0x0000002A System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereEnumerableIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x0000002B System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereEnumerableIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x0000002C System.Void System.Linq.Enumerable_WhereArrayIterator`1::.ctor(TSource[],System.Func`2<TSource,System.Boolean>)
// 0x0000002D System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereArrayIterator`1::Clone()
// 0x0000002E System.Boolean System.Linq.Enumerable_WhereArrayIterator`1::MoveNext()
// 0x0000002F System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereArrayIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000030 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereArrayIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000031 System.Void System.Linq.Enumerable_WhereListIterator`1::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000032 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereListIterator`1::Clone()
// 0x00000033 System.Boolean System.Linq.Enumerable_WhereListIterator`1::MoveNext()
// 0x00000034 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereListIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000035 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereListIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000036 System.Void System.Linq.Enumerable_WhereSelectEnumerableIterator`2::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x00000037 System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Clone()
// 0x00000038 System.Void System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Dispose()
// 0x00000039 System.Boolean System.Linq.Enumerable_WhereSelectEnumerableIterator`2::MoveNext()
// 0x0000003A System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x0000003B System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x0000003C System.Void System.Linq.Enumerable_WhereSelectArrayIterator`2::.ctor(TSource[],System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x0000003D System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectArrayIterator`2::Clone()
// 0x0000003E System.Boolean System.Linq.Enumerable_WhereSelectArrayIterator`2::MoveNext()
// 0x0000003F System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable_WhereSelectArrayIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x00000040 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectArrayIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x00000041 System.Void System.Linq.Enumerable_WhereSelectListIterator`2::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x00000042 System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2::Clone()
// 0x00000043 System.Boolean System.Linq.Enumerable_WhereSelectListIterator`2::MoveNext()
// 0x00000044 System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable_WhereSelectListIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x00000045 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x00000046 System.Void System.Linq.Enumerable_<>c__DisplayClass6_0`1::.ctor()
// 0x00000047 System.Boolean System.Linq.Enumerable_<>c__DisplayClass6_0`1::<CombinePredicates>b__0(TSource)
// 0x00000048 System.Void System.Linq.Enumerable_<>c__DisplayClass7_0`3::.ctor()
// 0x00000049 TResult System.Linq.Enumerable_<>c__DisplayClass7_0`3::<CombineSelectors>b__0(TSource)
// 0x0000004A System.Void System.Linq.Enumerable_<SelectManyIterator>d__17`2::.ctor(System.Int32)
// 0x0000004B System.Void System.Linq.Enumerable_<SelectManyIterator>d__17`2::System.IDisposable.Dispose()
// 0x0000004C System.Boolean System.Linq.Enumerable_<SelectManyIterator>d__17`2::MoveNext()
// 0x0000004D System.Void System.Linq.Enumerable_<SelectManyIterator>d__17`2::<>m__Finally1()
// 0x0000004E System.Void System.Linq.Enumerable_<SelectManyIterator>d__17`2::<>m__Finally2()
// 0x0000004F TResult System.Linq.Enumerable_<SelectManyIterator>d__17`2::System.Collections.Generic.IEnumerator<TResult>.get_Current()
// 0x00000050 System.Void System.Linq.Enumerable_<SelectManyIterator>d__17`2::System.Collections.IEnumerator.Reset()
// 0x00000051 System.Object System.Linq.Enumerable_<SelectManyIterator>d__17`2::System.Collections.IEnumerator.get_Current()
// 0x00000052 System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable_<SelectManyIterator>d__17`2::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
// 0x00000053 System.Collections.IEnumerator System.Linq.Enumerable_<SelectManyIterator>d__17`2::System.Collections.IEnumerable.GetEnumerator()
// 0x00000054 System.Void System.Linq.Enumerable_<DistinctIterator>d__68`1::.ctor(System.Int32)
// 0x00000055 System.Void System.Linq.Enumerable_<DistinctIterator>d__68`1::System.IDisposable.Dispose()
// 0x00000056 System.Boolean System.Linq.Enumerable_<DistinctIterator>d__68`1::MoveNext()
// 0x00000057 System.Void System.Linq.Enumerable_<DistinctIterator>d__68`1::<>m__Finally1()
// 0x00000058 TSource System.Linq.Enumerable_<DistinctIterator>d__68`1::System.Collections.Generic.IEnumerator<TSource>.get_Current()
// 0x00000059 System.Void System.Linq.Enumerable_<DistinctIterator>d__68`1::System.Collections.IEnumerator.Reset()
// 0x0000005A System.Object System.Linq.Enumerable_<DistinctIterator>d__68`1::System.Collections.IEnumerator.get_Current()
// 0x0000005B System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_<DistinctIterator>d__68`1::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
// 0x0000005C System.Collections.IEnumerator System.Linq.Enumerable_<DistinctIterator>d__68`1::System.Collections.IEnumerable.GetEnumerator()
// 0x0000005D System.Linq.IOrderedEnumerable`1<TElement> System.Linq.IOrderedEnumerable`1::CreateOrderedEnumerable(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
// 0x0000005E System.Void System.Linq.Set`1::.ctor(System.Collections.Generic.IEqualityComparer`1<TElement>)
// 0x0000005F System.Boolean System.Linq.Set`1::Add(TElement)
// 0x00000060 System.Boolean System.Linq.Set`1::Find(TElement,System.Boolean)
// 0x00000061 System.Void System.Linq.Set`1::Resize()
// 0x00000062 System.Int32 System.Linq.Set`1::InternalGetHashCode(TElement)
// 0x00000063 System.Collections.Generic.IEnumerator`1<TElement> System.Linq.OrderedEnumerable`1::GetEnumerator()
// 0x00000064 System.Linq.EnumerableSorter`1<TElement> System.Linq.OrderedEnumerable`1::GetEnumerableSorter(System.Linq.EnumerableSorter`1<TElement>)
// 0x00000065 System.Collections.IEnumerator System.Linq.OrderedEnumerable`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000066 System.Linq.IOrderedEnumerable`1<TElement> System.Linq.OrderedEnumerable`1::System.Linq.IOrderedEnumerable<TElement>.CreateOrderedEnumerable(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
// 0x00000067 System.Void System.Linq.OrderedEnumerable`1::.ctor()
// 0x00000068 System.Void System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::.ctor(System.Int32)
// 0x00000069 System.Void System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.IDisposable.Dispose()
// 0x0000006A System.Boolean System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::MoveNext()
// 0x0000006B TElement System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.Collections.Generic.IEnumerator<TElement>.get_Current()
// 0x0000006C System.Void System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.Collections.IEnumerator.Reset()
// 0x0000006D System.Object System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.Collections.IEnumerator.get_Current()
// 0x0000006E System.Void System.Linq.OrderedEnumerable`2::.ctor(System.Collections.Generic.IEnumerable`1<TElement>,System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
// 0x0000006F System.Linq.EnumerableSorter`1<TElement> System.Linq.OrderedEnumerable`2::GetEnumerableSorter(System.Linq.EnumerableSorter`1<TElement>)
// 0x00000070 System.Void System.Linq.EnumerableSorter`1::ComputeKeys(TElement[],System.Int32)
// 0x00000071 System.Int32 System.Linq.EnumerableSorter`1::CompareKeys(System.Int32,System.Int32)
// 0x00000072 System.Int32[] System.Linq.EnumerableSorter`1::Sort(TElement[],System.Int32)
// 0x00000073 System.Void System.Linq.EnumerableSorter`1::QuickSort(System.Int32[],System.Int32,System.Int32)
// 0x00000074 System.Void System.Linq.EnumerableSorter`1::.ctor()
// 0x00000075 System.Void System.Linq.EnumerableSorter`2::.ctor(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean,System.Linq.EnumerableSorter`1<TElement>)
// 0x00000076 System.Void System.Linq.EnumerableSorter`2::ComputeKeys(TElement[],System.Int32)
// 0x00000077 System.Int32 System.Linq.EnumerableSorter`2::CompareKeys(System.Int32,System.Int32)
// 0x00000078 System.Void System.Linq.Buffer`1::.ctor(System.Collections.Generic.IEnumerable`1<TElement>)
// 0x00000079 TElement[] System.Linq.Buffer`1::ToArray()
// 0x0000007A System.Void System.Collections.Generic.HashSet`1::.ctor()
// 0x0000007B System.Void System.Collections.Generic.HashSet`1::.ctor(System.Collections.Generic.IEqualityComparer`1<T>)
// 0x0000007C System.Void System.Collections.Generic.HashSet`1::.ctor(System.Collections.Generic.IEnumerable`1<T>)
// 0x0000007D System.Void System.Collections.Generic.HashSet`1::.ctor(System.Collections.Generic.IEnumerable`1<T>,System.Collections.Generic.IEqualityComparer`1<T>)
// 0x0000007E System.Void System.Collections.Generic.HashSet`1::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x0000007F System.Void System.Collections.Generic.HashSet`1::CopyFrom(System.Collections.Generic.HashSet`1<T>)
// 0x00000080 System.Void System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.Add(T)
// 0x00000081 System.Void System.Collections.Generic.HashSet`1::Clear()
// 0x00000082 System.Boolean System.Collections.Generic.HashSet`1::Contains(T)
// 0x00000083 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32)
// 0x00000084 System.Boolean System.Collections.Generic.HashSet`1::Remove(T)
// 0x00000085 System.Int32 System.Collections.Generic.HashSet`1::get_Count()
// 0x00000086 System.Boolean System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
// 0x00000087 System.Collections.Generic.HashSet`1_Enumerator<T> System.Collections.Generic.HashSet`1::GetEnumerator()
// 0x00000088 System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.HashSet`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x00000089 System.Collections.IEnumerator System.Collections.Generic.HashSet`1::System.Collections.IEnumerable.GetEnumerator()
// 0x0000008A System.Void System.Collections.Generic.HashSet`1::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x0000008B System.Void System.Collections.Generic.HashSet`1::OnDeserialization(System.Object)
// 0x0000008C System.Boolean System.Collections.Generic.HashSet`1::Add(T)
// 0x0000008D System.Void System.Collections.Generic.HashSet`1::UnionWith(System.Collections.Generic.IEnumerable`1<T>)
// 0x0000008E System.Void System.Collections.Generic.HashSet`1::CopyTo(T[])
// 0x0000008F System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32,System.Int32)
// 0x00000090 System.Collections.Generic.IEqualityComparer`1<T> System.Collections.Generic.HashSet`1::get_Comparer()
// 0x00000091 System.Void System.Collections.Generic.HashSet`1::TrimExcess()
// 0x00000092 System.Void System.Collections.Generic.HashSet`1::Initialize(System.Int32)
// 0x00000093 System.Void System.Collections.Generic.HashSet`1::IncreaseCapacity()
// 0x00000094 System.Void System.Collections.Generic.HashSet`1::SetCapacity(System.Int32)
// 0x00000095 System.Boolean System.Collections.Generic.HashSet`1::AddIfNotPresent(T)
// 0x00000096 System.Void System.Collections.Generic.HashSet`1::AddValue(System.Int32,System.Int32,T)
// 0x00000097 System.Boolean System.Collections.Generic.HashSet`1::AreEqualityComparersEqual(System.Collections.Generic.HashSet`1<T>,System.Collections.Generic.HashSet`1<T>)
// 0x00000098 System.Int32 System.Collections.Generic.HashSet`1::InternalGetHashCode(T)
// 0x00000099 System.Void System.Collections.Generic.HashSet`1_Enumerator::.ctor(System.Collections.Generic.HashSet`1<T>)
// 0x0000009A System.Void System.Collections.Generic.HashSet`1_Enumerator::Dispose()
// 0x0000009B System.Boolean System.Collections.Generic.HashSet`1_Enumerator::MoveNext()
// 0x0000009C T System.Collections.Generic.HashSet`1_Enumerator::get_Current()
// 0x0000009D System.Object System.Collections.Generic.HashSet`1_Enumerator::System.Collections.IEnumerator.get_Current()
// 0x0000009E System.Void System.Collections.Generic.HashSet`1_Enumerator::System.Collections.IEnumerator.Reset()
static Il2CppMethodPointer s_methodPointers[158] = 
{
	Error_ArgumentNull_mCA126ED8F4F3B343A70E201C44B3A509690F1EA7,
	Error_ArgumentOutOfRange_mACFCB068F4E0C4EEF9E6EDDD59E798901C32C6C9,
	Error_MoreThanOneMatch_m85C3617F782E9F2333FC1FDF42821BE069F24623,
	Error_NoElements_m17188AC2CF25EB359A4E1DDE9518A98598791136,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
};
static const int32_t s_InvokerIndices[158] = 
{
	0,
	0,
	4,
	4,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
};
static const Il2CppTokenRangePair s_rgctxIndices[49] = 
{
	{ 0x02000004, { 73, 4 } },
	{ 0x02000005, { 77, 9 } },
	{ 0x02000006, { 88, 7 } },
	{ 0x02000007, { 97, 10 } },
	{ 0x02000008, { 109, 11 } },
	{ 0x02000009, { 123, 9 } },
	{ 0x0200000A, { 135, 12 } },
	{ 0x0200000B, { 150, 1 } },
	{ 0x0200000C, { 151, 2 } },
	{ 0x0200000D, { 153, 12 } },
	{ 0x0200000E, { 165, 11 } },
	{ 0x02000010, { 176, 8 } },
	{ 0x02000012, { 184, 3 } },
	{ 0x02000013, { 189, 5 } },
	{ 0x02000014, { 194, 7 } },
	{ 0x02000015, { 201, 3 } },
	{ 0x02000016, { 204, 7 } },
	{ 0x02000017, { 211, 4 } },
	{ 0x02000018, { 215, 34 } },
	{ 0x0200001A, { 249, 2 } },
	{ 0x06000005, { 0, 10 } },
	{ 0x06000006, { 10, 10 } },
	{ 0x06000007, { 20, 5 } },
	{ 0x06000008, { 25, 5 } },
	{ 0x06000009, { 30, 1 } },
	{ 0x0600000A, { 31, 2 } },
	{ 0x0600000B, { 33, 2 } },
	{ 0x0600000C, { 35, 1 } },
	{ 0x0600000D, { 36, 1 } },
	{ 0x0600000E, { 37, 2 } },
	{ 0x0600000F, { 39, 3 } },
	{ 0x06000010, { 42, 2 } },
	{ 0x06000011, { 44, 3 } },
	{ 0x06000012, { 47, 4 } },
	{ 0x06000013, { 51, 3 } },
	{ 0x06000014, { 54, 3 } },
	{ 0x06000015, { 57, 1 } },
	{ 0x06000016, { 58, 3 } },
	{ 0x06000017, { 61, 2 } },
	{ 0x06000018, { 63, 3 } },
	{ 0x06000019, { 66, 2 } },
	{ 0x0600001A, { 68, 5 } },
	{ 0x0600002A, { 86, 2 } },
	{ 0x0600002F, { 95, 2 } },
	{ 0x06000034, { 107, 2 } },
	{ 0x0600003A, { 120, 3 } },
	{ 0x0600003F, { 132, 3 } },
	{ 0x06000044, { 147, 3 } },
	{ 0x06000066, { 187, 2 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[251] = 
{
	{ (Il2CppRGCTXDataType)2, 24434 },
	{ (Il2CppRGCTXDataType)3, 21059 },
	{ (Il2CppRGCTXDataType)2, 24435 },
	{ (Il2CppRGCTXDataType)2, 24436 },
	{ (Il2CppRGCTXDataType)3, 21060 },
	{ (Il2CppRGCTXDataType)2, 24437 },
	{ (Il2CppRGCTXDataType)2, 24438 },
	{ (Il2CppRGCTXDataType)3, 21061 },
	{ (Il2CppRGCTXDataType)2, 24439 },
	{ (Il2CppRGCTXDataType)3, 21062 },
	{ (Il2CppRGCTXDataType)2, 24440 },
	{ (Il2CppRGCTXDataType)3, 21063 },
	{ (Il2CppRGCTXDataType)2, 24441 },
	{ (Il2CppRGCTXDataType)2, 24442 },
	{ (Il2CppRGCTXDataType)3, 21064 },
	{ (Il2CppRGCTXDataType)2, 24443 },
	{ (Il2CppRGCTXDataType)2, 24444 },
	{ (Il2CppRGCTXDataType)3, 21065 },
	{ (Il2CppRGCTXDataType)2, 24445 },
	{ (Il2CppRGCTXDataType)3, 21066 },
	{ (Il2CppRGCTXDataType)2, 24446 },
	{ (Il2CppRGCTXDataType)3, 21067 },
	{ (Il2CppRGCTXDataType)3, 21068 },
	{ (Il2CppRGCTXDataType)2, 17842 },
	{ (Il2CppRGCTXDataType)3, 21069 },
	{ (Il2CppRGCTXDataType)2, 24447 },
	{ (Il2CppRGCTXDataType)3, 21070 },
	{ (Il2CppRGCTXDataType)3, 21071 },
	{ (Il2CppRGCTXDataType)2, 17849 },
	{ (Il2CppRGCTXDataType)3, 21072 },
	{ (Il2CppRGCTXDataType)3, 21073 },
	{ (Il2CppRGCTXDataType)2, 24448 },
	{ (Il2CppRGCTXDataType)3, 21074 },
	{ (Il2CppRGCTXDataType)2, 24449 },
	{ (Il2CppRGCTXDataType)3, 21075 },
	{ (Il2CppRGCTXDataType)3, 21076 },
	{ (Il2CppRGCTXDataType)3, 21077 },
	{ (Il2CppRGCTXDataType)2, 24450 },
	{ (Il2CppRGCTXDataType)3, 21078 },
	{ (Il2CppRGCTXDataType)2, 24451 },
	{ (Il2CppRGCTXDataType)3, 21079 },
	{ (Il2CppRGCTXDataType)3, 21080 },
	{ (Il2CppRGCTXDataType)2, 17879 },
	{ (Il2CppRGCTXDataType)3, 21081 },
	{ (Il2CppRGCTXDataType)2, 17880 },
	{ (Il2CppRGCTXDataType)2, 24452 },
	{ (Il2CppRGCTXDataType)3, 21082 },
	{ (Il2CppRGCTXDataType)2, 24453 },
	{ (Il2CppRGCTXDataType)2, 24454 },
	{ (Il2CppRGCTXDataType)2, 17883 },
	{ (Il2CppRGCTXDataType)2, 24455 },
	{ (Il2CppRGCTXDataType)2, 17885 },
	{ (Il2CppRGCTXDataType)2, 24456 },
	{ (Il2CppRGCTXDataType)3, 21083 },
	{ (Il2CppRGCTXDataType)2, 24457 },
	{ (Il2CppRGCTXDataType)2, 17888 },
	{ (Il2CppRGCTXDataType)2, 24458 },
	{ (Il2CppRGCTXDataType)2, 17890 },
	{ (Il2CppRGCTXDataType)2, 17892 },
	{ (Il2CppRGCTXDataType)2, 24459 },
	{ (Il2CppRGCTXDataType)3, 21084 },
	{ (Il2CppRGCTXDataType)2, 24460 },
	{ (Il2CppRGCTXDataType)2, 17895 },
	{ (Il2CppRGCTXDataType)2, 17897 },
	{ (Il2CppRGCTXDataType)2, 24461 },
	{ (Il2CppRGCTXDataType)3, 21085 },
	{ (Il2CppRGCTXDataType)2, 24462 },
	{ (Il2CppRGCTXDataType)3, 21086 },
	{ (Il2CppRGCTXDataType)3, 21087 },
	{ (Il2CppRGCTXDataType)2, 24463 },
	{ (Il2CppRGCTXDataType)2, 17902 },
	{ (Il2CppRGCTXDataType)2, 24464 },
	{ (Il2CppRGCTXDataType)2, 17904 },
	{ (Il2CppRGCTXDataType)3, 21088 },
	{ (Il2CppRGCTXDataType)3, 21089 },
	{ (Il2CppRGCTXDataType)2, 17907 },
	{ (Il2CppRGCTXDataType)3, 21090 },
	{ (Il2CppRGCTXDataType)3, 21091 },
	{ (Il2CppRGCTXDataType)2, 17919 },
	{ (Il2CppRGCTXDataType)2, 24465 },
	{ (Il2CppRGCTXDataType)3, 21092 },
	{ (Il2CppRGCTXDataType)3, 21093 },
	{ (Il2CppRGCTXDataType)2, 17921 },
	{ (Il2CppRGCTXDataType)2, 24293 },
	{ (Il2CppRGCTXDataType)3, 21094 },
	{ (Il2CppRGCTXDataType)3, 21095 },
	{ (Il2CppRGCTXDataType)2, 24466 },
	{ (Il2CppRGCTXDataType)3, 21096 },
	{ (Il2CppRGCTXDataType)3, 21097 },
	{ (Il2CppRGCTXDataType)2, 17931 },
	{ (Il2CppRGCTXDataType)2, 24467 },
	{ (Il2CppRGCTXDataType)3, 21098 },
	{ (Il2CppRGCTXDataType)3, 21099 },
	{ (Il2CppRGCTXDataType)3, 20387 },
	{ (Il2CppRGCTXDataType)3, 21100 },
	{ (Il2CppRGCTXDataType)2, 24468 },
	{ (Il2CppRGCTXDataType)3, 21101 },
	{ (Il2CppRGCTXDataType)3, 21102 },
	{ (Il2CppRGCTXDataType)2, 17943 },
	{ (Il2CppRGCTXDataType)2, 24469 },
	{ (Il2CppRGCTXDataType)3, 21103 },
	{ (Il2CppRGCTXDataType)3, 21104 },
	{ (Il2CppRGCTXDataType)3, 21105 },
	{ (Il2CppRGCTXDataType)3, 21106 },
	{ (Il2CppRGCTXDataType)3, 21107 },
	{ (Il2CppRGCTXDataType)3, 20393 },
	{ (Il2CppRGCTXDataType)3, 21108 },
	{ (Il2CppRGCTXDataType)2, 24470 },
	{ (Il2CppRGCTXDataType)3, 21109 },
	{ (Il2CppRGCTXDataType)3, 21110 },
	{ (Il2CppRGCTXDataType)2, 17956 },
	{ (Il2CppRGCTXDataType)2, 24471 },
	{ (Il2CppRGCTXDataType)3, 21111 },
	{ (Il2CppRGCTXDataType)3, 21112 },
	{ (Il2CppRGCTXDataType)2, 17958 },
	{ (Il2CppRGCTXDataType)2, 24472 },
	{ (Il2CppRGCTXDataType)3, 21113 },
	{ (Il2CppRGCTXDataType)3, 21114 },
	{ (Il2CppRGCTXDataType)2, 24473 },
	{ (Il2CppRGCTXDataType)3, 21115 },
	{ (Il2CppRGCTXDataType)3, 21116 },
	{ (Il2CppRGCTXDataType)2, 24474 },
	{ (Il2CppRGCTXDataType)3, 21117 },
	{ (Il2CppRGCTXDataType)3, 21118 },
	{ (Il2CppRGCTXDataType)2, 17973 },
	{ (Il2CppRGCTXDataType)2, 24475 },
	{ (Il2CppRGCTXDataType)3, 21119 },
	{ (Il2CppRGCTXDataType)3, 21120 },
	{ (Il2CppRGCTXDataType)3, 21121 },
	{ (Il2CppRGCTXDataType)3, 20404 },
	{ (Il2CppRGCTXDataType)2, 24476 },
	{ (Il2CppRGCTXDataType)3, 21122 },
	{ (Il2CppRGCTXDataType)3, 21123 },
	{ (Il2CppRGCTXDataType)2, 24477 },
	{ (Il2CppRGCTXDataType)3, 21124 },
	{ (Il2CppRGCTXDataType)3, 21125 },
	{ (Il2CppRGCTXDataType)2, 17989 },
	{ (Il2CppRGCTXDataType)2, 24478 },
	{ (Il2CppRGCTXDataType)3, 21126 },
	{ (Il2CppRGCTXDataType)3, 21127 },
	{ (Il2CppRGCTXDataType)3, 21128 },
	{ (Il2CppRGCTXDataType)3, 21129 },
	{ (Il2CppRGCTXDataType)3, 21130 },
	{ (Il2CppRGCTXDataType)3, 21131 },
	{ (Il2CppRGCTXDataType)3, 20410 },
	{ (Il2CppRGCTXDataType)2, 24479 },
	{ (Il2CppRGCTXDataType)3, 21132 },
	{ (Il2CppRGCTXDataType)3, 21133 },
	{ (Il2CppRGCTXDataType)2, 24480 },
	{ (Il2CppRGCTXDataType)3, 21134 },
	{ (Il2CppRGCTXDataType)3, 21135 },
	{ (Il2CppRGCTXDataType)3, 21136 },
	{ (Il2CppRGCTXDataType)3, 21137 },
	{ (Il2CppRGCTXDataType)3, 21138 },
	{ (Il2CppRGCTXDataType)3, 21139 },
	{ (Il2CppRGCTXDataType)2, 24481 },
	{ (Il2CppRGCTXDataType)2, 24482 },
	{ (Il2CppRGCTXDataType)3, 21140 },
	{ (Il2CppRGCTXDataType)2, 18024 },
	{ (Il2CppRGCTXDataType)2, 18018 },
	{ (Il2CppRGCTXDataType)3, 21141 },
	{ (Il2CppRGCTXDataType)2, 18017 },
	{ (Il2CppRGCTXDataType)2, 24483 },
	{ (Il2CppRGCTXDataType)3, 21142 },
	{ (Il2CppRGCTXDataType)3, 21143 },
	{ (Il2CppRGCTXDataType)3, 21144 },
	{ (Il2CppRGCTXDataType)2, 24484 },
	{ (Il2CppRGCTXDataType)3, 21145 },
	{ (Il2CppRGCTXDataType)2, 18040 },
	{ (Il2CppRGCTXDataType)2, 18032 },
	{ (Il2CppRGCTXDataType)3, 21146 },
	{ (Il2CppRGCTXDataType)3, 21147 },
	{ (Il2CppRGCTXDataType)2, 18031 },
	{ (Il2CppRGCTXDataType)2, 24485 },
	{ (Il2CppRGCTXDataType)3, 21148 },
	{ (Il2CppRGCTXDataType)3, 21149 },
	{ (Il2CppRGCTXDataType)3, 21150 },
	{ (Il2CppRGCTXDataType)2, 24486 },
	{ (Il2CppRGCTXDataType)2, 24487 },
	{ (Il2CppRGCTXDataType)3, 21151 },
	{ (Il2CppRGCTXDataType)3, 21152 },
	{ (Il2CppRGCTXDataType)2, 18052 },
	{ (Il2CppRGCTXDataType)3, 21153 },
	{ (Il2CppRGCTXDataType)2, 18053 },
	{ (Il2CppRGCTXDataType)2, 24488 },
	{ (Il2CppRGCTXDataType)3, 21154 },
	{ (Il2CppRGCTXDataType)3, 21155 },
	{ (Il2CppRGCTXDataType)2, 24489 },
	{ (Il2CppRGCTXDataType)3, 21156 },
	{ (Il2CppRGCTXDataType)2, 24490 },
	{ (Il2CppRGCTXDataType)3, 21157 },
	{ (Il2CppRGCTXDataType)3, 21158 },
	{ (Il2CppRGCTXDataType)3, 21159 },
	{ (Il2CppRGCTXDataType)2, 18072 },
	{ (Il2CppRGCTXDataType)3, 21160 },
	{ (Il2CppRGCTXDataType)2, 18080 },
	{ (Il2CppRGCTXDataType)3, 21161 },
	{ (Il2CppRGCTXDataType)2, 24491 },
	{ (Il2CppRGCTXDataType)2, 24492 },
	{ (Il2CppRGCTXDataType)3, 21162 },
	{ (Il2CppRGCTXDataType)3, 21163 },
	{ (Il2CppRGCTXDataType)3, 21164 },
	{ (Il2CppRGCTXDataType)3, 21165 },
	{ (Il2CppRGCTXDataType)3, 21166 },
	{ (Il2CppRGCTXDataType)3, 21167 },
	{ (Il2CppRGCTXDataType)2, 18096 },
	{ (Il2CppRGCTXDataType)2, 24493 },
	{ (Il2CppRGCTXDataType)3, 21168 },
	{ (Il2CppRGCTXDataType)3, 21169 },
	{ (Il2CppRGCTXDataType)2, 18100 },
	{ (Il2CppRGCTXDataType)3, 21170 },
	{ (Il2CppRGCTXDataType)2, 24494 },
	{ (Il2CppRGCTXDataType)2, 18110 },
	{ (Il2CppRGCTXDataType)2, 18108 },
	{ (Il2CppRGCTXDataType)2, 24495 },
	{ (Il2CppRGCTXDataType)3, 21171 },
	{ (Il2CppRGCTXDataType)2, 24496 },
	{ (Il2CppRGCTXDataType)3, 21172 },
	{ (Il2CppRGCTXDataType)3, 21173 },
	{ (Il2CppRGCTXDataType)2, 18117 },
	{ (Il2CppRGCTXDataType)3, 21174 },
	{ (Il2CppRGCTXDataType)2, 18117 },
	{ (Il2CppRGCTXDataType)3, 21175 },
	{ (Il2CppRGCTXDataType)2, 18134 },
	{ (Il2CppRGCTXDataType)3, 21176 },
	{ (Il2CppRGCTXDataType)3, 21177 },
	{ (Il2CppRGCTXDataType)3, 21178 },
	{ (Il2CppRGCTXDataType)2, 24497 },
	{ (Il2CppRGCTXDataType)3, 21179 },
	{ (Il2CppRGCTXDataType)3, 21180 },
	{ (Il2CppRGCTXDataType)3, 21181 },
	{ (Il2CppRGCTXDataType)2, 18114 },
	{ (Il2CppRGCTXDataType)3, 21182 },
	{ (Il2CppRGCTXDataType)3, 21183 },
	{ (Il2CppRGCTXDataType)2, 18119 },
	{ (Il2CppRGCTXDataType)3, 21184 },
	{ (Il2CppRGCTXDataType)1, 24498 },
	{ (Il2CppRGCTXDataType)2, 18118 },
	{ (Il2CppRGCTXDataType)3, 21185 },
	{ (Il2CppRGCTXDataType)1, 18118 },
	{ (Il2CppRGCTXDataType)1, 18114 },
	{ (Il2CppRGCTXDataType)2, 24497 },
	{ (Il2CppRGCTXDataType)2, 18118 },
	{ (Il2CppRGCTXDataType)2, 18116 },
	{ (Il2CppRGCTXDataType)2, 18120 },
	{ (Il2CppRGCTXDataType)3, 21186 },
	{ (Il2CppRGCTXDataType)3, 21187 },
	{ (Il2CppRGCTXDataType)3, 21188 },
	{ (Il2CppRGCTXDataType)2, 18115 },
	{ (Il2CppRGCTXDataType)3, 21189 },
	{ (Il2CppRGCTXDataType)2, 18130 },
};
extern const Il2CppCodeGenModule g_System_CoreCodeGenModule;
const Il2CppCodeGenModule g_System_CoreCodeGenModule = 
{
	"System.Core.dll",
	158,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	49,
	s_rgctxIndices,
	251,
	s_rgctxValues,
	NULL,
};
